import React, { Component } from 'react';
import {
  View, Text, TouchableOpacity, Image, StyleSheet, Picker, Dimensions, ImageBackground, TextInput, Linking, AsyncStorage
} from 'react-native';
import { Ionicons, EvilIcons, MaterialIcons, Entypo, FontAwesome5, AntDesign } from '@expo/vector-icons';
const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
import { URL } from '../constants/API'
console.disableYellowBox=true

export default class SideBar extends Component {
  // const [started, setStarted] = useState(false)
  constructor(props) {
    super(props);
    this.state = {
      started: false,
      auth:""

    }

  }
  authentication = () => {
    this.setState({
      auth: !this.state.auth
    });
  }    
  logout = () => {
    AsyncStorage.clear();
    this.props.navigation.push('login');

  }
  

  render() {
    return (

      <View style={{ flex: 1, alignItems: "center", backgroundColor: "#fff" }}>
        <View style={{ width: "100%", marginTop: 40 }}>
          <View style={{ width: "45%",paddingRight:20 }}>
            <Image style={{ width: "100%", height: 80, resizeMode: "stretch", borderRadius: 90 }} source={require('../assets/images/logo.png')} />
          </View>
          {/* <View style={{ width: "50%", marginHorizontal: 12 }}>
            <Text style={{ fontSize: 22, fontWeight: "bold", color: "#1294D0", fontStyle: "italic" }}>Jump In Shape</Text>
          </View> */}
        </View>
{this.state.auth ? (
  <TouchableOpacity style={{ width: "90%", marginTop: 40, paddingBottom: 10, paddingLeft: 15, borderBottomWidth: 1, borderColor: "#E3E3E3" }} onPress={() => this.props.navigation.navigate('login')}>
          <Text style={{ fontSize: 18, fontWeight: "bold", color: "#1294D0 " }}>Sign In</Text>
        </TouchableOpacity>
):(
  <TouchableOpacity style={{ width: "90%", marginTop: 20, paddingBottom: 10, paddingLeft: 15, borderBottomWidth: 1, borderColor: "#E3E3E3" }} onPress={() => this.logout()} >
          <Text style={{ fontSize: 18, fontWeight: "bold", color: "#1294D0" }}>Sign Out</Text>
        </TouchableOpacity>
)
}
        
        <TouchableOpacity style={{ width: "90%", marginTop: 20, paddingBottom: 10, paddingLeft: 15, borderBottomWidth: 1, borderColor: "#E3E3E3" }} onPress={() => this.props.navigation.navigate('profile')} >
          <Text style={{ fontSize: 18, fontWeight: "bold", color: "#1294D0" }}>Profile In</Text>
        </TouchableOpacity>
        {/* <TouchableOpacity style={{ width: "90%", marginTop: 20, paddingBottom: 10, paddingLeft: 15, borderBottomWidth: 1, borderColor: "#E3E3E3" }} onPress={() => this.props.navigation.navigate('learn')} >
          <Text style={{ fontSize: 18, fontWeight: "bold", color: "#1294D0" }}>Learn</Text>
        </TouchableOpacity> */}
        <TouchableOpacity style={{ width: "90%", marginTop: 20, paddingBottom: 10, paddingLeft: 15, borderBottomWidth: 1, borderColor: "#E3E3E3" }} onPress={() => this.props.navigation.push('settings')} >
          <Text style={{ fontSize: 18, fontWeight: "bold", color: "#1294D0" }}>Settings</Text>
        </TouchableOpacity>
        <View style={{ width: "90%", marginTop: 20, paddingBottom: 10, paddingLeft: 15, borderBottomWidth: 1, borderColor: "#E3E3E3" }} onPress={() => this.props.navigation.navigate()} >
          <View>
            <Text style={{ fontSize: 18, fontWeight: "bold", color: "#1294D0" }}>Follow Us</Text>
          </View>
          <View style={{ width: "100%", flexDirection: "row", marginTop: 12 }}>
            {/* <TouchableOpacity style={{ width: "15%" }} onPress={() => this.props.navigation.navigate('Facebook')}>
              <FontAwesome5 name="facebook" size={24} color="grey" />
            </TouchableOpacity> */}
            <TouchableOpacity style={{ width: "15%" }}  onPress={() => this.props.navigation.navigate('Instagram')}>
              <AntDesign name="instagram" size={24} color="red" />
            </TouchableOpacity>
            <TouchableOpacity style={{ width: "15%" }}  onPress={() => this.props.navigation.navigate('Youtube')} >
              <AntDesign name="youtube" size={24} color="red" />
            </TouchableOpacity>
          </View>
        </View>
        <TouchableOpacity style={{ width: "90%", marginTop: 20, paddingBottom: 10, paddingLeft: 15, borderBottomWidth: 1, borderColor: "#E3E3E3" }} onPress={() => this.props.navigation.navigate('caloriesCalculator')} >
          <Text style={{ fontSize: 18, fontWeight: "bold", color: "#1294D0" }}>Calories Calculator</Text>
        </TouchableOpacity>
        <TouchableOpacity style={{ width: "90%", marginTop: 20, paddingBottom: 10, paddingLeft: 15, borderBottomWidth: 1, borderColor: "#E3E3E3" }}  onPress={() => this.props.navigation.navigate('Term_user')} >
          <Text style={{ fontSize: 18, fontWeight: "bold", color: "#1294D0" }}>Terms of use </Text>
        </TouchableOpacity>
        <TouchableOpacity style={{ width: "90%", marginTop: 20, paddingBottom: 10, paddingLeft: 15, borderBottomWidth: 1, borderColor: "#E3E3E3" }} onPress={() => this.props.navigation.navigate('Term_user')} >
          <Text style={{ fontSize: 18, fontWeight: "bold", color: "#1294D0" }}>Privacy Policy</Text>
        </TouchableOpacity>
        
      </View>


    );
  }

}
const styles = StyleSheet.create({
  pic: {


    height: 250,
    width: "100%",


  },
  login_button: {
    width: "90%",

    marginTop: 10,
    paddingVertical: 13,
    alignItems: "center",
    alignContent: "center",
    backgroundColor: "#808080",
    borderRadius: 10
  },
  facebook: {

    width: "90%",

    marginTop: 30,
    paddingVertical: 13,
    alignItems: "center",
    alignContent: "center",
    backgroundColor: "#005f9A",
    borderRadius: 10
  },
});