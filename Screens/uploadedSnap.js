import React, { Component } from 'react';
import {
  View, Text, TouchableOpacity, Image, StyleSheet, Picker, Dimensions, ImageBackground, TextInput, Alert, AsyncStorage,
} from 'react-native';
import { URL,IMAGE_URL } from '../constants/API'
import { Container, Content, ListItem, CheckBox, Body } from 'native-base';
console.disableYellowBox = true
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { MaterialIcons, MaterialCommunityIcons } from '@expo/vector-icons';
const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
import Header from './Header'
import PDFReader from 'rn-pdf-reader-js-no-loading'
import { WebView } from 'react-native-webview';
import { ScrollView } from 'react-native-gesture-handler'
import Pdf from 'react-native-pdf';
import { Modal } from 'react-native';
import ImageViewer from 'react-native-image-zoom-viewer';
import {ProgressView} from "@react-native-community/progress-view";
import i18n from 'i18n-js'
import translations from './translations'
export default class uploadedSnap extends Component {
  // const [started, setStarted] = useState(false)
  constructor(props) {
    super(props);
    this.data = this.props.route.params;
    console.log(">>>>>>>>>>>>>>this.data>>>>>>>>>>>>>>>>>")
    console.log(this.data)
    this.state = {
      started: false,
      otp: "",
      get_data: "",
      data: this.data,
      user_id:"",
      lang_done:false
    }
  }
  async componentDidMount() {
    AsyncStorage.getItem("userID").then(user_data => {
      const val = JSON.parse(user_data);
      if (val) {
       this.setState({user_id:val.user.uuid,file:val.file})
       console.log(">>>>>>>>>>>>>>>>>>user_id>>>>>>>>>>")
       console.log(this.state.user_id)
     
      }

      // this.delete_Policy()

    });
    AsyncStorage.getItem("lang").then(lang => {
      const language = JSON.parse(lang);
      console.log("--a--")
      console.log(language)
      console.log(language)
      console.log("--a--")
      if (language) {  
        console.log("----")
        console.log(language)
        console.log(language)
        console.log("----")
        i18n.translations = translations;
        i18n.locale = language;
        i18n.fallbacks = true;
      
      }
      this.setState({lang_done : true})
    })
  }

  render() {
    if(this.state.lang_done){
    return (

      <View style={{ width: screenWidth, height: screenHeight }}>
        <Header title={i18n.t('Files')} back={true} backtext={false} navigation={this.props.navigation} />
       <View style={{flexDirection:"row", flexWrap:"wrap"}}>
       {this.data.files_data.length > 0 ? 
        this.data.files_data.map((item, index) => {
          console.log(">>>>>>>>>IMAGE>>>>>>>>>")
          console.log(IMAGE_URL+item.file)

return (
  <View key={index} style={{ width: "30%",marginHorizontal:5, alignItems: "center", marginTop: 10, borderWidth:1, borderColor:"#000",borderRadius:10 }} >
                    <Image style={{ width: "100%", height: 100, resizeMode: "cover" }} source={{uri:IMAGE_URL+item.file}} />
                    <View style={{ width: "90%", alignItems: "center" }}>
                    
                    </View>
                  </View>
       )
        })
       
       :null}
       </View>
     







      </View>
);
}
else{
  return(
    <View>
      <Text>Wait</Text>
    </View>
  )
}
    
  }

}
const styles = StyleSheet.create({
  pic: {


    height: 250,
    width: "100%",
    borderRadius: 50



  },
  login_button: {
    width: "100%",
    marginLeft: 10,
    marginTop: 10,
    paddingVertical: 8,
    alignItems: "center",
    alignContent: "center",
    backgroundColor: "#808080",
    borderRadius: 10
  },
  SignUp_button: {

    width: "90%",


    paddingVertical: 10,
    alignItems: "center",
    alignContent: "center",
    backgroundColor: "#E7BF63",
    borderRadius: 10,
    borderWidth: 1,
    borderColor: "#E7BF63"
  },
  box1: {

    width: "40%",
    height: 25,
    resizeMode: "cover",
    borderRadius: 80,



  },
  SignUp_button: {

    width: "90%",


    paddingVertical: 10,
    alignItems: "center",
    alignContent: "center",
    backgroundColor: "#E7BF63",
    borderRadius: 10,
    borderWidth: 1,
    borderColor: "#E7BF63"
  },
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginTop: 25,
},

});