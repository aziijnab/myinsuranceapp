import React, { Component } from 'react';
import {
  View, Text, TouchableOpacity, Image, StyleSheet, Picker, Dimensions, ImageBackground, TextInput, Alert, AsyncStorage, ScrollView, AntDesign
} from 'react-native';
import { URL, IMAGE_URL } from '../constants/API'
console.disableYellowBox = true
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { MaterialIcons, MaterialCommunityIcons, Octicons, Feather } from '@expo/vector-icons';
const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
import Header from './Header'
import i18n from 'i18n-js'
import translations from './translations'
export default class checkInsurance extends Component {
  // const [started, setStarted] = useState(false)
  constructor(props) {
    super(props);
    this.state = {
      started: false,
      user_id: "",
      get_data: [],
      extension:"",
      company_name:""
    }
    this.data = this.props.route.params;
    console.log(this.data)
  }

  componentDidMount() {
    AsyncStorage.getItem("userID").then(user_data => {
      const val = JSON.parse(user_data);
      if (val) {
        this.setState({ user_id: val.user.uuid })
       
        console.log(">>>>>>>>>>>>>>>>>>val>>>>>>>>>>")
        console.log(val)
      }
      this.check_Insurance()

    });

  }
  check_Insurance = () => {
    fetch(URL + "get-user-insurances", {
      method: "Post",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(
        {
          "user_id": this.state.user_id,

        }
      )
    })
      .then(res => res.json())
      .then(async response => {
        console.log("check_Insurance");
        console.log( response.data.extension);
        if (response.response == "success") {
          this.setState({ get_data: response.data })
          console.log(">>>>>>>>>>>>>Company Name>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
          console.log(this.state.get_data);
     
        }


        else {
          Alert.alert(
            "Sorry",
            response.message,
            [
              {
                text: "Cancel",
                style: "cancel"
              },
              { text: "OK" }
            ],
            { cancelable: false }
          );
        }

        this.setState({ loading: true, refreshing: false })
      })
      .catch(error => {
        alert("Please check internet connection");
      });

  }

  render() {
    return (

      <View style={{ width: screenWidth, height: screenHeight, alignItems: "center", }}>
        <Header title={i18n.t('Check')} back={true} backtext={false} navigation={this.props.navigation} />
        <ScrollView style={{ width: "97%" }} showsVerticalScrollIndicator={false}>
            
          <View style={{width:"100%",alignItems:"center",marginTop:30}}>
       <View style={{width:"50%",height:"50%"}}>
             
           
              {this.state.get_data.length > 0 ?
                this.state.get_data.map((item,index) => {
                  return(
                    item.extension == "jpg" || item.extension == "png" ? (
                  <TouchableOpacity key={index} style={{ width: "100%", alignItems: "center", marginTop: 10 }} onPress={() => this.props.navigation.push("DisplayFiles", { policy_data: item })}>
                    <Image style={{ width: "100%", height: 100, resizeMode: "cover" }} source={require('../assets/images/images.png')} />
                    <View style={{ width: "90%", alignItems: "center" }}>
                      <Text style={{ color: "#007AFF" }}>
                       {item.company.partner}
    </Text>
                    </View>
                  </TouchableOpacity>

                ) :
                  item.extension == "pdf" ? (
                    <TouchableOpacity key={index} style={{ width: "100%", alignItems: "center", marginTop: 10 }} onPress={() => this.props.navigation.push("DisplayFiles", { policy_data: item })}>
                      <Image style={{ width: "100%", height: 100, resizeMode: "cover" }} source={require('../assets/images/unnamed.png')} />
                      <View style={{ width: "90%", alignItems: "center" }}>
                        <Text style={{ color: "#007AFF" }}>
                        {item.company.partner}
    </Text>
                      </View>
                    </TouchableOpacity>
                  ) : item.extension == "docx" ? (
                    <TouchableOpacity key={index} style={{ width: "100%", alignItems: "center", marginTop: 10 }} onPress={() => {
Alert.alert(
            "Downloading",
            "Policy is being downloaded.",
            [
            { text: "OK" }
            ],
            { cancelable: false }
          );

                    }}>
                     <Image style={{ width: "100%", height: 100, resizeMode: "cover" }} source={require('../assets/images/word.png')} />
                      <View style={{ width: "90%", alignItems: "center" }}>
                        <Text style={{ color: "#007AFF" }}>
                        {item.company.partner}
    </Text>
                      </View>
                    </TouchableOpacity>
                  ) : null
                  )
                  
                })
               




                : null}


            </View>
          </View>
        </ScrollView>


      </View>

    );
  }

}
const styles = StyleSheet.create({
  pic: {


    height: 250,
    width: "100%",
    borderRadius: 50



  },
  login_button: {
    width: "100%",
    marginLeft: 10,
    marginTop: 10,
    paddingVertical: 8,
    alignItems: "center",
    alignContent: "center",
    backgroundColor: "#808080",
    borderRadius: 10
  },
  SignUp_button: {

    width: "90%",


    paddingVertical: 10,
    alignItems: "center",
    alignContent: "center",
    backgroundColor: "#E7BF63",
    borderRadius: 10,
    borderWidth: 1,
    borderColor: "#E7BF63"
  },
  search: {

    width: "100%",
    marginTop: 10,
    paddingVertical: 10,

    backgroundColor: "#F5F5F5",
    borderRadius: 50,
  },
  box1: {

    width: "100%",
    height: 89,
    resizeMode: "contain",
  },
  popbox1: {
    flexDirection: "row", backgroundColor: "#fff", marginTop: 10,
    shadowColor: "grey",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0,
    shadowRadius: 0,
    elevation: 0,
  },
});