import React, { Component } from 'react';
import {
  View, Text, TouchableOpacity, Image, StyleSheet, Picker, Dimensions, ImageBackground, TextInput, Alert, AsyncStorage
} from 'react-native';
import { Ionicons, EvilIcons, MaterialIcons } from '@expo/vector-icons';
import { ScrollView } from 'react-native-gesture-handler';
const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
import { URL } from '../constants/API'
console.disableYellowBox = true
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview'
import i18n from 'i18n-js'
import translations from './translations'

export default class Home extends Component {
  // const [started, setStarted] = useState(false)
  constructor(props) {
    super(props);
    this.state = {
      started: false,
      login_email: "",
      login_password: "",
      register_email: "",
      register_password: "",
      lang_done:false
    }

  }
  async componentDidMount() {
    AsyncStorage.getItem("userID").then(user_data => {
      const val = JSON.parse(user_data);
      if (val) {
        this.setState({full_name:val.user.f_name})
     

       console.log(val)
      }

   

    });
    AsyncStorage.getItem("lang").then(lang => {
      const language = JSON.parse(lang);
      console.log("--a--")
      console.log(language)
      console.log(language)
      console.log("--bbbbbbbbb--")
      if (language) {  
        console.log("----")
        console.log(language)
        console.log(language)
        console.log("----")
        i18n.translations = translations;
        i18n.locale = language;
        i18n.fallbacks = true;
      
      }
      this.setState({lang_done : true})
    });
  }
  submit = () => {
    let login_email = this.state.login_email
    let login_password = this.state.login_password

    if (login_email == "" && login_password == "") {
      Alert.alert(
        "Sorry",
        " E-mail and Password is required",
        [
          {
            text: "Cancel",
            style: "cancel"
          },
          { text: "OK" }
        ],
        { cancelable: false }
      );
    }

    if (login_email != "" && login_password == "") {
      Alert.alert(
        "Sorry",
        "Password is required",
        [
          {
            text: "Cancel",
            style: "cancel"
          },
          { text: "OK" }
        ],
        { cancelable: false }
      );
    }
    if (login_email == "" && login_password != "") {
      Alert.alert(
        "Sorry",
        "E-mail is required",
        [
          {
            text: "Cancel",
            style: "cancel"
          },
          { text: "OK" }
        ],
        { cancelable: false }
      );
    }
    if (login_email != "" && login_password != "") {

      console.log(login_email + "---" + login_password)
      fetch(URL + "login", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          "email": login_email,
          "password": login_password
        })
      })
        .then(res => res.json())
        .then(async response => {
          console.log(response);
          if (response.response == "success") {

            AsyncStorage.setItem("userID", JSON.stringify(response));
            AsyncStorage.setItem("setting", JSON.stringify(response.setting));
            Alert.alert(
              "Success",
              response.message,
              [
                {
                  text: "Cancel",
                  style: "cancel"
                },
                { text: "OK" }
              ],
              { cancelable: false }
            );
            this.props.navigation.navigate("workouts");

          }
          else {
            Alert.alert(
              "Sorry",
              response.message,
              [
                {
                  text: "Cancel",
                  style: "cancel"
                },
                { text: "OK" }
              ],
              { cancelable: false }
            );
          }


        })
        .catch(error => {
          console.log(error)
          alert("Please check internet connection");
        });

    }

  }
  register = () => {
    let register_email = this.state.register_email
    let register_password = this.state.register_password

    if (register_email == "" && register_password == "") {
      Alert.alert(
        "Sorry",
        " E-mail and Password is required",
        [
          {
            text: "Cancel",
            style: "cancel"
          },
          { text: "OK" }
        ],
        { cancelable: false }
      );
    }

    if (register_email != "" && register_password == "") {
      Alert.alert(
        "Sorry",
        "Password is required",
        [
          {
            text: "Cancel",
            style: "cancel"
          },
          { text: "OK" }
        ],
        { cancelable: false }
      );
    }
    if (register_email == "" && register_password != "") {
      Alert.alert(
        "Sorry",
        "E-mail is required",
        [
          {
            text: "Cancel",
            style: "cancel"
          },
          { text: "OK" }
        ],
        { cancelable: false }
      );
    }
    if (register_email != "" && register_password != "") {

      console.log(register_email + "---" + register_password)
      fetch(URL + "register", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          "email": register_email,
          "password": register_password
        })
      })
        .then(res => res.json())
        .then(async response => {
          console.log(response);
          if (response.response == "success") {
            AsyncStorage.setItem("userID", JSON.stringify(response));
            Alert.alert(
              "Success",
              response.message,
              [
                {
                  text: "Cancel",
                  style: "cancel"
                },
                { text: "OK" }
              ],
              { cancelable: false }
            );
            this.props.navigation.navigate("login");

          }
          else {
            Alert.alert(
              "Sorry",
              response.message,
              [
                {
                  text: "Cancel",
                  style: "cancel"
                },
                { text: "OK" }
              ],
              { cancelable: false }
            );
          }


        })
        .catch(error => {
          console.log(error)
          alert("Please check internet connection");
        });

    }

  }

  render() {
    if(this.state.lang_done){
    return (

      <View style={{ width: screenWidth, height: screenHeight, alignItems: "center",backgroundColor:"#fff"}}>
        <View style={{ width: "100%", alignContent: "center", alignItems: "center",marginTop:80 }}>
          <Image style={styles.pic} source={require('../assets/images/logo.png')} />
        </View>
        <View style={{ width: "100%", alignContent: "center", alignItems: "center", marginTop: 40 }}>
<View style={{width:"90%", alignContent: "center", alignItems: "center",}}>
<Text style={{ fontSize: 15, fontWeight: "bold",color:"#044184",textAlign:"center" }}>{i18n.t('introduction')}</Text>
</View>
       
        </View>
        <View style={{ width: "100%", marginTop: 10, alignItems: "center" }}>
          <View style={{ width: "92%" }}>
            <Text style={{ textAlign: "center", color: "#838383", fontSize: 16 }}>
              {i18n.t('recommend')}
          </Text>
          </View>
        </View>

        <View style={{width:"100%",alignItems:"center",height:"20%"}} >

        <View style={{ width: "90%", marginTop: 20, justifyContent: "center", alignContent: "center",paddingTop:20 }}>
          <View style={{ width: "100%", flexDirection: "row" }}>

            <View style={{ width: "50%", justifyContent: "center", alignItems: "center", alignContent: "center" }}>
              <TouchableOpacity style={styles.SignUp_button} onPress={()=>this.props.navigation.navigate('login')} >
                <View style={{ flexDirection: "row" }}>
                  {/* <EvilIcons style={{}} name="sc-facebook" size={24} color="#ffffff" /> */}
                  <Text style={{ color: '#E7BF63', alignItems: "center", marginTop: 2, fontSize: 18, fontWeight: "bold" }}>{i18n.t('login')}</Text>
                </View>
              </TouchableOpacity>
            </View>
            <View style={{ width: "50%", justifyContent: "center", alignItems: "center" }}>
              <TouchableOpacity style={styles.SignUp1_button} onPress={()=>this.props.navigation.navigate('registration')} >
                <View style={{ flexDirection: "row" }}>
                  {/* <Image style={{width:20,height:20,resizeMode:"cover"}} source={require('../assets/images/google.png')}/> */}
                  <Text style={{ color: '#E7BF63', alignItems: "center", marginTop: 2, fontSize: 18, paddingLeft: 5, fontWeight: "bold" }}>{i18n.t('register')}</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>

        </View>
             {/* <TouchableOpacity  style={{paddingTop:20}}>
              <Text style={{ fontWeight: "bold", fontSize: 16, color: "#E7BF63", textAlign: "center",textDecorationLine:"underline" }}>
                Enter your access code
    </Text>
            </TouchableOpacity>  */}

         
        </View>







      </View>


);
}
else{
  return(
    <View>
      <Text>Wait</Text>
    </View>
  )
}
    
  }

}
const styles = StyleSheet.create({
  pic: {


    height: 250,
    width: "100%",
    resizeMode:"contain",
    paddingTop:50


  },
  SignUp_button: {

    width: "90%",

    marginTop: 10,
    paddingVertical: 10,
    alignItems: "center",
    alignContent: "center",
    backgroundColor: "#fff",
    borderRadius: 10,
    borderWidth: 1,
    borderColor: "#E7BF63"
  },
  SignUp1_button: {

    width: "90%",
    borderWidth: 1,
    borderColor: "#808080",
    marginTop: 10,
    paddingVertical: 10,
    alignItems: "center",
    alignContent: "center",
    backgroundColor: "#fff",
    borderRadius: 10,
    borderWidth: 1,
    borderColor: "#E7BF63"
  },
});