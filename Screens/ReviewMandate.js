import React, { Component } from 'react';
import {
  View, Text, TouchableOpacity, Image, StyleSheet, Picker, Dimensions, ImageBackground, TextInput, Alert, AsyncStorage,
} from 'react-native';
import { URL } from '../constants/API'
import { Container, Content, ListItem, CheckBox, Body } from 'native-base';
console.disableYellowBox = true
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { MaterialIcons, MaterialCommunityIcons } from '@expo/vector-icons';
const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
import Header from './Header'
import translation from './translation'
import * as Localization from 'expo-localization';
import i18n from 'i18n-js'
import { ScrollView } from 'react-native-gesture-handler';
// i18n.translations = {
//   en: { welcome: 'Hello', name: 'Ali' },
//   ja: { welcome: 'こんにちは' },
// };
i18n.fallbacks = true;
i18n.locale = "th"
export default class ReviewMandate extends Component {
  // const [started, setStarted] = useState(false)
  constructor(props) {
    super(props);
    this.state = {
      user_data: [],
      user_email: "",
      company: "",
      f_name: "",
      street_no: "",
      zip_city: "",
      tel_private: "",
      dob: "",
      email: "",
      tel_business: "",
      place: "",
      client: "",
      is_contract: true,
    }
 
  }
  componentDidMount() {
    AsyncStorage.getItem("userID").then(userID => {
      const val = JSON.parse(userID);
      console.log(val);
      if(val){
        this.setState({
          user_email:val.user.email,
      
      })
      console.log(">>>>>>>>>>>user_email>>>>>>>>>>>>>>>>")
      console.log(val.user.email)   
      }
     
    });
      AsyncStorage.getItem("mandate").then(mandate => {
        const val = JSON.parse(mandate);
        console.log(val);
        if(val){
          this.setState({
            company:val.company,
            f_name:val.f_name,
            street_no:val.street_no,
            zip_city:val.zip_city,
            tel_private:val.tel_private,
            dob:val.dob,
            email:val.email,
            tel_business:val.tel_business,
            place:val.place,
            client:val.client,
        })
              
        }
       
      });
  
}

  save = () => {
    let company = this.state.company;
    let f_name = this.state.f_name;
    let street_no = this.state.street_no;
    let zip_city = this.state.zip_city;
    let tel_private = this.state.tel_private;
    let dob = this.state.dob;
    let email = this.state.email;
    let tel_business = this.state.tel_business;
    let place = this.state.place;
    let client = this.state.client;
    let is_contract = this.state.is_contract;
    

    fetch(URL + "update-contract", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        "company": company,
        "f_name": f_name,
        "street_no": street_no,
        "zip_city": zip_city,
        "tel_private": tel_private,
        "dob": dob,
        "email": this.state.user_email,
        "user_email": email,
        "tel_business": tel_business,
        "place": place,
        "client": client,
        "is_contract":1

        
 
      })
    })
      .then(res => res.json())
      .then(async response => {
        console.log(response);
        if (response.response == "success"){
         
            Alert.alert(
                "Success",
                response.message,
                [
                  {
                    text: "Cancel",
                    style: "cancel"
                  },
                  { text: "OK"}
                ],
                { cancelable: false }
              );
    
          }else{
            
            Alert.alert(
              "Sorry",
              response.message,
              [
                {
                  text: "Cancel",
                  style: "cancel"
                },
                { text: "OK"}
              ],
              { cancelable: false }
            );
          }
         this.setState({loading:false})
         
        })
        .catch(error => {
          alert("Please check internet connection");
        });
}


  render() {
    return (

      <View style={{ width: screenWidth, height: screenHeight, alignItems: "center" }}>
        <Header title={"Review Mandate"} back={true} backtext={false} navigation={this.props.navigation} />
        <ScrollView style={{ width: "100%" }}>
          <View style={{ width: "100%", marginTop: 20, alignItems: "center", alignContent: "center", paddingBottom: 250 }}>
            <View style={{ width: "95%", backgroundColor: "#fff", paddingVertical: 15, borderRadius: 15 }}>
              <View style={{ width: "100%", marginTop: 10, paddingLeft: 15 }} >
                <Text style={{ fontWeight: "bold", fontSize: 17, color: "#007AFF" }}>
                  Power of attorney and mandate
</Text>
                <Text style={{ fontWeight: "bold", fontSize: 15, color: "#000", paddingTop: 5 }}>
                  For managing the insurance portfolio

</Text>
                <Text style={{  fontSize: 15, color: "#000", paddingTop: 10 }}>
                  The undersigned (hereinafter referred to as the client):

</Text>
                        <View style={{ width: "95%", marginTop: 20, flexDirection: "row", borderBottomWidth: 1, borderColor: "#007AFF", paddingBottom: 7 }}>
                  <View style={{ width: "50%", marginHorizontal: 5, justifyContent: "center" }}>
                    <Text style={{ fontWeight: "bold", fontSize: 15 }}>
                      Salutation / Company:
</Text>
                  </View>
                  <View style={{ width: "50%", justifyContent: "center" }}>
                  <TextInput
                  style={{ color: '#044184', fontSize: 16, paddingHorizontal: 15 }}
                  secureTextEntry={false}
                  placeholder={"Salutation / Company"}
                  placeholderTextColor="#044184"
                  onChangeText={(company) => this.setState({ company })}
                  value={this.state.company}
                />
                  </View>

                </View>
                <View style={{ width: "95%", marginTop: 20, flexDirection: "row", borderBottomWidth: 1, borderColor: "#007AFF", paddingBottom: 7 }}>
                  <View style={{ width: "50%", marginHorizontal: 5, justifyContent: "center" }}>
                    <Text style={{ fontWeight: "bold", fontSize: 15 }}>
                      first Name:
</Text>
                  </View>
                  <View style={{ width: "50%", justifyContent: "center" }}>
                     <TextInput
                  style={{ color: '#044184', fontSize: 16, paddingHorizontal: 15 }}
                  secureTextEntry={false}
                  placeholder={"Enter your Name"}
                  placeholderTextColor="#044184"
                  onChangeText={(f_name) => this.setState({ f_name })}
                  value={this.state.f_name}
                />
                  </View>

                </View>
                <View style={{ width: "95%", marginTop: 20, flexDirection: "row", borderBottomWidth: 1, borderColor: "#007AFF", paddingBottom: 7 }}>
                  <View style={{ width: "50%", marginHorizontal: 5, justifyContent: "center" }}>
                    <Text style={{ fontWeight: "bold", fontSize: 15 }}>
                      Street, No:
</Text>
                  </View>
                  <View style={{ width: "50%", justifyContent: "center" }}>
                     <TextInput
                  style={{ color: '#044184', fontSize: 16, paddingHorizontal: 15 }}
                  secureTextEntry={false}
                  placeholder={" Enter your Street No"}
                  placeholderTextColor="#044184"
                  onChangeText={(street_no) => this.setState({ street_no })}
                  value={this.state.street_no}
                />
                  </View>

                </View>
                <View style={{ width: "95%", marginTop: 20, flexDirection: "row", borderBottomWidth: 1, borderColor: "#007AFF", paddingBottom: 7 }}>
                  <View style={{ width: "50%", marginHorizontal: 5, justifyContent: "center" }}>
                    <Text style={{ fontWeight: "bold", fontSize: 15 }}>
                      Postcode / town:

</Text>
                  </View>
                  <View style={{ width: "50%", justifyContent: "center" }}>
                     <TextInput
                  style={{ color: '#044184', fontSize: 16, paddingHorizontal: 15 }}
                  secureTextEntry={false}
                  placeholder={"Enter your Post Code"}
                  placeholderTextColor="#044184"
                  onChangeText={(zip_city) => this.setState({ zip_city })}
                  value={this.state.zip_city}
                />
                  </View>

                </View>
                <View style={{ width: "95%", marginTop: 20, flexDirection: "row", borderBottomWidth: 1, borderColor: "#007AFF", paddingBottom: 7 }}>
                  <View style={{ width: "50%", marginHorizontal: 5, justifyContent: "center" }}>
                    <Text style={{ fontWeight: "bold", fontSize: 15 }}>
                      Tel. Private:

</Text>
                  </View>
                  <View style={{ width: "50%", justifyContent: "center" }}>
                     <TextInput
                  style={{ color: '#044184', fontSize: 16, paddingHorizontal: 15 }}
                  secureTextEntry={false}
                  placeholder={"Enter your Private No"}
                  placeholderTextColor="#044184"
                  onChangeText={(tel_private) => this.setState({ tel_private })}
                  value={this.state.tel_private}
                />
                  </View>

                </View>
                <View style={{ width: "95%", marginTop: 20, flexDirection: "row", borderBottomWidth: 1, borderColor: "#007AFF", paddingBottom: 7 }}>
                  <View style={{ width: "50%", marginHorizontal: 5, justifyContent: "center" }}>
                    <Text style={{ fontWeight: "bold", fontSize: 15 }}>
                      Date of birth:

</Text>
                  </View>
                  <View style={{ width: "50%", justifyContent: "center" }}>
                     <TextInput
                  style={{ color: '#044184', fontSize: 16, paddingHorizontal: 15 }}
                  secureTextEntry={false}
                  placeholder={"Enter Your Birth Date"}
                  placeholderTextColor="#044184"
                  onChangeText={(dob) => this.setState({ dob })}
                  value={this.state.dob}
                />
                  </View>

                </View>
                <View style={{ width: "95%", marginTop: 20, flexDirection: "row", borderBottomWidth: 1, borderColor: "#007AFF", paddingBottom: 7 }}>
                  <View style={{ width: "50%", marginHorizontal: 5, justifyContent: "center" }}>
                    <Text style={{ fontWeight: "bold", fontSize: 15 }}>
                      Tel-Business:

</Text>
                  </View>
                  <View style={{ width: "50%", justifyContent: "center" }}>
                     <TextInput
                  style={{ color: '#044184', fontSize: 16, paddingHorizontal: 15 }}
                  secureTextEntry={false}
                  placeholder={"Enter Your Business No"}
                  placeholderTextColor="#044184"
                  onChangeText={(tel_business) => this.setState({ tel_business })}
                  value={this.state.tel_business}
                />
                  </View>

                </View>
                <View style={{ width: "95%", marginTop: 20, flexDirection: "row", borderBottomWidth: 1, borderColor: "#007AFF", paddingBottom: 7 }}>
                  <View style={{ width: "50%", marginHorizontal: 5, justifyContent: "center" }}>
                    <Text style={{ fontWeight: "bold", fontSize: 15 }}>
                      E-mail:

</Text>
                  </View>
                  <View style={{ width: "50%", justifyContent: "center" }}>
                     <TextInput
                  style={{ color: '#044184', fontSize: 16, paddingHorizontal: 15 }}
                  secureTextEntry={false}
                  placeholder={"Enter Your E-mail"}
                  placeholderTextColor="#044184"
                  onChangeText={(email) => this.setState({ email })}
                  value={this.state.email}
                />
                  </View>

                </View>
                <Text style={{ fontWeight: "bold", fontSize: 15, color: "#000", paddingTop: 10 }}>
                  issued to INP on behalf of Kabu Consultation GmbH

</Text>
                <Text style={{  fontSize: 15, color: "#000", paddingTop: 20 }}>
                  Finanz GmbH, Tonhallestrasse 45, CH-9500 Wil SG (hereinafter referred to as broker)

</Text>
                <Text style={{  fontSize: 15, color: "#000", paddingTop: 20 }}>
                  the power of attorney and the mandate to manage his existing insurance policies.

</Text>
                <Text style={{  fontSize: 15, color: "#000", paddingTop: 20 }}>
                  The agreement to manage the insurance portfolio also includes the following

</Text>
                <Text style={{ fontWeight: "bold", fontSize: 15, color: "#007AFF", paddingTop: 20 }}>
                  Services:

</Text>
                <Text style={{ fontSize: 15, color: "#000", paddingTop: 20 }}>

                  .Supervision of all existing insurance contracts

</Text>
                <Text style={{ fontSize: 15, color: "#000", paddingTop: 20 }}>
                  .Review of insurance needs and the insurance portfolio

</Text>
                <Text style={{ fontSize: 15, color: "#000", paddingTop: 20 }}>
                  . Ongoing review of the product range on the insurance market
</Text>
                <Text style={{ fontSize: 15, color: "#000", paddingTop: 20 }}>
                  .Obtaining offers from various insurance companies

</Text>
                <Text style={{ fontSize: 15, color: "#000", paddingTop: 20 }}>
                  .Assistance with risk and security analyzes

</Text>
                <Text style={{ fontSize: 15, color: "#000", paddingTop: 20 }}>

                  .Policy renewals

</Text>
                <Text style={{ fontSize: 15, color: "#000", paddingTop: 20 }}>


                  .New deals

</Text>
                <Text style={{ fontSize: 15, color: "#000", paddingTop: 20 }}>


                  The client remains the policyholder and premium debtor.

</Text>
                <Text style={{ fontSize: 15, color: "#000", paddingTop: 5 }}>



                  He signs insurance applications himself and accepts claims payments directly. As part of
                  For overall advice, it is imperative to make all changes and new deals through the broker.

</Text>
                <Text style={{ fontSize: 15, color: "#000", paddingTop:10 }}>

                  The broker represents him in all other matters towards the insurance companies.
                  Original policies are sent to the broker for review.

</Text>
                <Text style={{ fontSize: 15, color: "#000", paddingTop: 10 }}>

                  The brokerage agreement is concluded for an indefinite period. However, the mandate agreement will
                  terminated by the client before 18 months have elapsed, the brokerage fees and
                  Commissions billed to compensated expenses. Commissions received, brokerage fees and
                  Compensation for inventory is the property of Kabu Consultation GmbH

</Text>
    <View style={{ width: "95%", marginTop: 20, flexDirection: "row", paddingBottom: 7 }}>
                  <View style={{ width: "50%", marginHorizontal: 5, justifyContent: "center" }}>
                    <Text style={{ fontWeight: "bold", fontSize: 15 }}>
                    Place and Date:
</Text>
                  </View>
                  <View style={{ width: "50%", justifyContent: "center", borderBottomWidth: 1, borderColor: "#007AFF" }}>
                  <TextInput
                  style={{ color: '#044184', fontSize: 16, paddingHorizontal: 15 }}
                  secureTextEntry={false}
                  placeholder={"Enter Your Place"}
                  placeholderTextColor="#044184"
                  onChangeText={(place) => this.setState({ place })}
                  value={this.state.place}
                />
                  </View>

                </View>
                <View style={{ width: "95%", marginTop: 20, flexDirection: "row", paddingBottom: 7 }}>
                  <View style={{ width: "50%", marginHorizontal: 5, justifyContent: "center" }}>
                    <Text style={{ fontWeight: "bold", fontSize: 15 }}>
                    The client
</Text>
                  </View>
                  <View style={{ width: "50%", justifyContent: "center", borderBottomWidth: 1, borderColor: "#007AFF" }}>
                     <TextInput
                  style={{ color: '#044184', fontSize: 16, paddingHorizontal: 15 }}
                  secureTextEntry={false}
                  placeholder={"Enter Client Name"}
                  placeholderTextColor="#044184"
                  onChangeText={(client) => this.setState({ client })}
                  value={this.state.client}
                />
                  </View>

                </View>
         
                 

                </View>
              
              
              
              
              </View>

            </View>
          {/* </View> */}
        </ScrollView>


        <View style={{ width: "100%", position: "absolute", alignItems: "center", bottom: 15 }} >
          <View style={{ width: "90%", justifyContent: "center", alignItems: "center", alignContent: "center" }}>
            <TouchableOpacity style={styles.SignUp_button}  onPress={() => this.save()}>

              {/* <EvilIcons style={{}} name="sc-facebook" size={24} color="#ffffff" /> */}
              <Text style={{ color: '#fff', alignItems: "center", marginTop: 2, fontSize: 18, fontWeight: "bold" }}>Save</Text>

            </TouchableOpacity>
          </View>
        </View>
      </View>

    );
  }

}
const styles = StyleSheet.create({
  pic: {


    height: 250,
    width: "100%",
    borderRadius: 50



  },
  login_button: {
    width: "100%",
    marginLeft: 10,
    marginTop: 10,
    paddingVertical: 8,
    alignItems: "center",
    alignContent: "center",
    backgroundColor: "#808080",
    borderRadius: 10
  },
  SignUp_button: {

    width: "90%",


    paddingVertical: 10,
    alignItems: "center",
    alignContent: "center",
    backgroundColor: "#E7BF63",
    borderRadius: 10,
    borderWidth: 1,
    borderColor: "#E7BF63"
  },
  box1: {

    width: "40%",
    height: 25,
    resizeMode: "cover",
    borderRadius: 80,



  },
});