import React, { Component } from 'react';
import {
  View, Text, TouchableOpacity, Image, StyleSheet, Picker, Dimensions, ImageBackground, TextInput, Alert, AsyncStorage
} from 'react-native';
import { URL } from '../constants/API'
console.disableYellowBox = true
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { MaterialIcons, MaterialCommunityIcons } from '@expo/vector-icons';
const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
import Header from './Header'
import * as Localization from 'expo-localization';
import i18n from 'i18n-js';
import translation from './translation';
i18n.translations = {
  en: { welcome: 'Hello', name: 'Ali' },
  ja: { welcome: 'こんにちは' },
};
i18n.translation = translation;
i18n.locale = Localization.locale;
i18n.fallbacks = true;
i18n.locale="ja"
export default class Switchlanguage extends Component {
  // const [started, setStarted] = useState(false)
  constructor(props) {
    super(props);
    this.state = {
      started: false,
      otp: "",
    }


  }


  render() {
    return (

      <View style={{ width: screenWidth, height: screenHeight, alignItems: "center", backgroundColor: "#fbfbfb" }}>
        <Header title={""} back={true} backtext={false} navigation={this.props.navigation} />


        <View style={{ width: "90%", justifyContent: "center", alignItems: "center", alignContent: "center" }}>
          <TouchableOpacity  >

            <Text>
              {i18n.t('welcome')} {i18n.t('name')}
            </Text>

          </TouchableOpacity>
        </View>


















































        <View style={{ width: "100%", position: "absolute", alignItems: "center", bottom: 15 }} >
          <View style={{ width: "90%", justifyContent: "center", alignItems: "center", alignContent: "center" }}>
            <TouchableOpacity style={styles.SignUp_button} >

              {/* <EvilIcons style={{}} name="sc-facebook" size={24} color="#ffffff" /> */}
              <Text style={{ color: '#fff', alignItems: "center", marginTop: 2, fontSize: 18, fontWeight: "bold" }}>Enter</Text>

            </TouchableOpacity>
          </View>
        </View>
      </View>

    );
  }

}
const styles = StyleSheet.create({
  pic: {


    height: 250,
    width: "100%",
    borderRadius: 50



  },
  login_button: {
    width: "100%",
    marginLeft: 10,
    marginTop: 10,
    paddingVertical: 8,
    alignItems: "center",
    alignContent: "center",
    backgroundColor: "#808080",
    borderRadius: 10
  },
  SignUp_button: {

    width: "90%",


    paddingVertical: 10,
    alignItems: "center",
    alignContent: "center",
    backgroundColor: "#E7BF63",
    borderRadius: 10,
    borderWidth: 1,
    borderColor: "#E7BF63"
  },
});