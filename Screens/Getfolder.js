import React, { Component } from 'react';
import {
  View, Text, TouchableOpacity, Image, StyleSheet, Picker, Dimensions, ImageBackground, TextInput, Alert, AsyncStorage,Modal,RefreshControl,Platform
} from 'react-native';
import { Ionicons, EvilIcons, MaterialIcons,Entypo } from '@expo/vector-icons';
import { ScrollView } from 'react-native-gesture-handler';
const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
import { URL } from '../constants/API'
console.disableYellowBox = true
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview'
import Footer from './Footer';
import Header from './Header'
import i18n from 'i18n-js'
import translations from './translations'
import * as ImagePicker from 'expo-image-picker';
import RNPickerSelect from 'react-native-picker-select';
export default class Getfolder extends Component {
  // const [started, setStarted] = useState(false)
  constructor(props) {
    super(props);
    this.state = {
      started: false,
      lang_done: false,
      user_id: "",
      file: "",
      folder_id:"",
      get_folder:[],
      user_image: "",
      folder_name: "",
      modalVisible: false,
      phase1: true,
      isNew: false,
      loading: true,
      refreshing: false
    }

  }
   componentDidMount() {
    this.getPermissionAsync()
    AsyncStorage.getItem("userID").then(user_data => {
      const val = JSON.parse(user_data);
      if (val) {
        this.setState({ user_id:val.user.uuid })
   

        console.log(val)
      }
      this.Getfolder()


    });

    AsyncStorage.getItem("lang").then(lang => {
      const language = JSON.parse(lang);
      console.log("--a--")
      console.log(language)
      console.log(language)
      console.log("--a--")
      if (language) {
        console.log("----")
        console.log(language)
        console.log(language)
        console.log("----")
        i18n.translations = translations;
        i18n.locale = language;
        i18n.fallbacks = true;

      }
      this.setState({ lang_done: true })
    });
  }
  Getfolder = () => {
    fetch(URL + "get-folders", {
      method: "Post",
      headers: {
        "Content-Type": "application/json",
      },

      body: JSON.stringify(
        {
          "user_id": this.state.user_id,
        }
      )
    })
      .then(res => res.json())
      .then(async response => {
        console.log(">>>>>Getfolder>>>>>");
        console.log(response);
        if (response.response == "success") {
          this.setState({ get_folder: response.data })

        }
        else {
          Alert.alert(
            "Sorry",
            response.message,
            [
              {
                text: "Cancel",
                style: "cancel"
              },
              { text: "OK" }
            ],
            { cancelable: false }
          );
        }
        this.setState({ loading: true, refreshing: false })

      })
      .catch(error => {
        alert("Please check internet connection");
      });
  }
  submit = () => {
    const photo = {
      uri: this.state.file.uri,
      type: "image/jpeg",
      name: "photo"+this.state.user_id+".jpeg",
    };
let fileType = 'jpg';
    console.log("this.state.folder_name")
    console.log(this.state.folder_name)
    console.log("this.state.user_id")
    console.log(this.state.user_id)
    console.log("this.state.folder_id")
    console.log(this.state.folder_id)
    console.log("this.state.fileType")
    console.log(fileType)

  
    const form = new FormData();
if(this.state.folder_id){
  
    form.append("user_id",this.state.user_id);
    form.append("folder_id",this.state.folder_id);
    form.append("fileType", fileType);
    form.append("file",photo);
   
  
}
else{
 
    form.append("user_id",this.state.user_id);
    form.append("folder_name",this.state.folder_name);
    form.append("fileType",fileType);
    form.append("file",photo);
 

}

    fetch(URL + "set-folder", {
      method: "Post",
      body: form,
    })
      .then(res => res.json())
      .then(async response => {
        console.log(">>>>>Setfolder>>>>>");
        console.log(response);
        if (response.response == "success") {
          this.Getfolder()
          // this.props.navigation.reload("Getfolder")
          Alert.alert(
            "Success",
            response.message,
            [
              {
                text: "Cancel",
                style: "cancel"
              },
              { text: "OK" }
            ],
            { cancelable: false }
            );
            this.setState({ phase1:true, isNew:false, folder_id:'', modalVisible:false })
        }
        else {
          Alert.alert(
            "Sorry",
            response.message,
            [
              {
                text: "Cancel",
                style: "cancel"
              },
              { text: "OK" }
            ],
            { cancelable: false }
          );
        }


      })
      .catch(error => {
        alert("Please check internet connection");
      });
  }
  getPermissionAsync = async () => {
    if (Platform.OS !== 'web') {
      
      const { status } = await Camera.requestPermissionsAsync();
      if (status !== 'granted') {
        alert('Sorry, we need camera roll permissions to make this work!');
      }
    }
  };

  pickImage = async () => {

    let result =await ImagePicker.launchCameraAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      base64: true,
      aspect: [4, 3],
      quality: 1,
    });

    console.log(">>>>>>>>>>>>>>RESULT>>>>>>>>>>>>>>>")
    console.log(result)
  
    if (!result.cancelled) {
      this.setModalVisible(true)
      this.setState({ file: result })
      console.log(">>>>>>>>>>>>>>FILE>>>>>>>>>>>>>>>")
      console.log(this.state.file)
    }
  };
  setModalVisible = () => {

    this.setState({ modalVisible: !this.state.modalVisible });

  }
  render() {
    const placeholder_insuranceName = {
      label: "Your existing folder ",
      value: null,
      color: "#C7C7C7",
      key: 0,

    };
let insurance_value = [];
    if(this.state.get_folder.length > 0){
      this.state.get_folder.map((item, index) => {
        let obj = {
          label: item.name,
          value: item.uuid
        }
        insurance_value.push(obj)
     })
    } 
 
    const { modalVisible } = this.state;
    if (this.state.lang_done) {
      return (

        <View style={{ width: screenWidth, height: screenHeight, alignItems: "center" }}>
          <Header title={i18n.t('StoredPolicy')} back={true} backtext={false} navigation={this.props.navigation} />
            <View style={{ width: "100%", paddingTop: 5, paddingHorizontal: 10 }}>
              <View style={{ width: "100%", alignItems: "center" }}>
                <View style={{ width: "90%", alignItems: "center" }}>
                  <Text style={{ fontSize: 40, color: "#E7BF63", fontWeight: "bold" }}>{i18n.t('folder')}</Text>
                </View>
              </View>
              {this.state.loading ? (
            <ScrollView refreshControl={<RefreshControl refreshing={this.state.refreshing} onRefresh={() => this.Getfolder()}
             
             />
             }
             showsVerticalScrollIndicator={false} 
             >

              <View style={{ width: "100%", alignItems: "center",marginTop:20,paddingBottom:180 }}>
              {this.state.get_folder.length > 0 ?
             this.state.get_folder.map((item, index) => {
              return (
            <TouchableOpacity 
            onPress={()=> this.props.navigation.push('uploadedSnap',{files_data : item.files})} 
            key={index} style={styles.popbox}>
              <View style={{ width: "90%", alignItems: "center",flexDirection:"row" }}>
              <View style={{ width: "35%", alignItems: "center" }}>
            <View style={{ width: "100%", paddingLeft: 10 }}>
              <Image style={styles.box} source={require('../assets/images/folder.png')} />
            </View>
          </View>
          <View style={{ width: "65%" }}>
            <Text style={{ fontSize: 22, color: "#000000", fontWeight: "bold" }}>{item.name}</Text>
          </View>
              </View>
              </TouchableOpacity>
              )
            })
            : null
          }
              </View>

          </ScrollView>
          ):(
            <View style={{ width: "100%", height: 200, alignItems: "center", justifyContent: "center", alignContent: "center" }}>
                <Text>No folder Available</Text>
              </View>
          )}

            </View>

          <Modal
          animationType="slide"
          transparent={true}
          swipeDirection="left"
          onSwipe={this.closeModal}
          onBackdropPress={this.closeModal}
          visible={this.state.modalVisible}
        // onRequestClose={() => {
        //   Alert.alert("Modal has been closed.");
        // }}
        >

          <View style={styles.centeredView}>

            <View style={styles.modalView}>
              <View style={{ width: "100%" }}>
                <TouchableOpacity style={{ width: "15%", alignContent: "center", alignItems: "center", paddingLeft: 5 }} onPress={() => {
                  this.setModalVisible()
                  this.setState({phase1:true, isNew:false, folder_id:''})
                  }}
                >
                  <Ionicons name="ios-arrow-back" size={25} color="#FE000C" />
                </TouchableOpacity>
              </View>
              {this.state.phase1 ? (
                <View style={{ width: "100%", alignItems: "center", alignContent: "center", }}>
                <View style={{ width: "80%", flexDirection: "row", justifyContent: "space-between" }}>
                  <TouchableOpacity 
                  onPress={()=> this.setState({phase1:false, isNew:true})}
                  style={{ width:"40%",backgroundColor:"#E7BF63",borderRadius:10,padding:30,marginTop:20 }} >
                    <Text style={{ color: '#000',fontSize:15
                    ,textAlign:"center" }}>New</Text> 
                  
                  </TouchableOpacity>
                  <TouchableOpacity onPress={()=> this.setState({phase1:false, isNew:false})} style={{ width:"40%",backgroundColor:"#E7BF63",borderRadius:10,padding:30,marginTop:20}}
                 
                    >
                    <Text style={{ color: '#000',fontSize:15,textAlign:"center"
                     }}>Existing</Text> 
                  </TouchableOpacity>
                </View>
              </View>
              ) : this.state.isNew ? (
  <View style={{ width: "80%", flexDirection: "row", justifyContent: "space-between" }}>
              <View style={{ width: "100%", marginTop: 25, paddingVertical: 5, alignContent: "center", alignItems: "center" }}>
              <View style={{ width: "45%", borderBottomWidth: 1, borderBottomColor: "#044184", paddingBottom: 5 ,paddingBottom:10}}>
                <TextInput
                  style={{ color: '#044184', fontSize: 16, paddingHorizontal: 15 }}
                  secureTextEntry={false}
                  placeholder={"Enter Folder name"}
                  placeholderTextColor="#044184"
                  onChangeText={(folder_name) => this.setState({ folder_name })}
                  value={this.state.folder_name}
                />
              </View>
           
            <TouchableOpacity  style={{paddingVertical:10,alignItems:"center",marginTop:40}} onPress={() =>{
              this.submit()
               }} >
           <View style={{width:"45%",backgroundColor:"#E7BF63",borderRadius:10,padding:10}} >
               <Text style={{ color: '#000',fontSize:20,fontWeight:"bold" }}>Submit</Text>
                </View>
                </TouchableOpacity>
            </View>
            </View>
) : (
  <View style={{ width: "100%"}}>
            <View style={{ width: "100%", alignItems: "center", alignContent: "center",marginTop:5 }}>
            <View style={{ width: "90%", marginHorizontal: 15 }}>
              <RNPickerSelect
                placeholder={placeholder_insuranceName}
                items={insurance_value}
                onValueChange={value => {
                  this.setState({ folder_id: value })
                }}
                style={{
                  ...smallpickerSelectStyles,
                  iconContainer: {
                    bottom: 10,
                    right: 0,
                    marginHorizontal: 10
                  },
                  width: 15
                }}
                value={this.state.folder_id}
                useNativeAndroidPickerStyle={false}
                textInputProps={{ underlineColor: "#3B5998" }}
                Icon={() => {
                  return (
                    <Entypo name="triangle-down" size={15} color="grey" />
                  );
                }}
              />
            </View>
          </View> 
          <View style={{width:"100%",alignItems:"center",marginTop:50}}>
              <View style={{width:"90%",alignItems:"center"}}>
              <TouchableOpacity onPress={() =>{
              this.submit()
               }} style={{width:"45%",backgroundColor:"#E7BF63",borderRadius:10,padding:10}} >
               <Text style={{ color: '#000',fontSize:25,fontWeight:"bold",textAlign:"center" }}>Submit</Text>
                </TouchableOpacity>
              </View>
            </View>  

            </View>
) }
             



             
        
           
           








              <ScrollView showsVerticalScrollIndicator={false}>

              </ScrollView>


             

            </View>
          </View>
        </Modal>

   
          <View style={{ width: "100%", position: "absolute", bottom: 30 ,zIndex:900}} >


<View style={{ width: "90%"}}>
  <TouchableOpacity style={styles.SignUp_button} onPress={() =>this.pickImage()} >
    <Text style={{ color: '#fff', alignItems: "center", fontSize: 26, fontWeight: "bold" }}>+</Text>
  </TouchableOpacity>
</View>

</View>







        </View>


      );
    }
    else {
      return (
        <View>
          <Text>Wait</Text>
        </View>
      )
    }
  }

}
const smallpickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 16,
    paddingVertical: 7,
    paddingHorizontal: 7,
    borderWidth: 1,
    borderColor: "#EBEBEB",
    color: "#B8B7BB",
    backgroundColor: "#fff",
    width: "100%",
    fontWeight: "bold",
    // to ensure the text is never behind the icon
  },
  inputAndroid: {
    fontSize: 16,
    paddingVertical: 6,
    paddingHorizontal: 7,
    fontWeight: "bold",
    borderWidth: 1,
    borderColor: "#EBEBEB",
    color: "#B8B7BB",
    backgroundColor: "#fff",
    width: "100%" // to ensure the text is never behind the icon
  }
});
const styles = StyleSheet.create({

  popbox: {
    width: '30%', alignContent: "center", alignItems: "center", backgroundColor: "#fff", borderRadius: 10, margin: 5,
    shadowColor: "grey",
    shadowOffset: {
      width: 1,
      height: 1,
    },
    shadowOpacity: 2.60,
    shadowRadius: 2.62,
    elevation: 6,
  },
  popbox2: {
    width: '30%', alignContent: "center", alignItems: "center", borderRadius: 10, margin: 5,

  },
  popbox1: {
    width: '46%', alignContent: "center", alignItems: "center", backgroundColor: "#fff", margin: 8, borderRadius: 10, marginRight: 5,
    shadowColor: "grey",
    shadowOffset: {
      width: 1,
      height: 1,
    },
    shadowOpacity: 2.60,
    shadowRadius: 2.62,
    elevation: 6,
  },
  box: {
    width: "50%", height: 60, resizeMode: "contain"
  },
  popbox: {
    width: '95%', alignContent: "center", alignItems: "center", backgroundColor: "#fff", marginTop: 10,
    shadowColor: "grey",
    shadowOffset: {
      width: 1,
      height: 1,
    },
    shadowOpacity: 2.60,
    shadowRadius: 2.62,
    elevation: 6,
  },
  SignUp_button: {

    width: "15%",
marginLeft:"auto",
height:52,
    paddingVertical: 10,
    alignItems: "center",
    alignContent: "center",
    backgroundColor: "#E7BF63",
    borderRadius: 50,
    borderWidth: 1,
    borderColor: "#E7BF63"
  },
  login_button: {
    width: "50%",
    padding: 10,
    alignItems: "center",
    alignContent: "center",

    backgroundColor: "#E7BF63",
    borderRadius: 10,
  },
  centeredView: {
    width: screenWidth,
    height: screenHeight,
    justifyContent: "flex-end",

  },
  modalView: {
    justifyContent: "center",
    alignItems: "center",
    width: "100%",
    height: "100%",
    backgroundColor: "white",
    borderRadius: 20,
    paddingTop: 40,


    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
});