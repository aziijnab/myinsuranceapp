import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image, StyleSheet, Picker, Dimensions, TextInput, ScrollView, ActivityIndicator, Alert, AsyncStorage, RefreshControl, ImageBackground 
  ,Linking 
} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { MaterialIcons, Ionicons, EvilIcons, MaterialCommunityIcons, Octicons, Feather, Entypo, AntDesign, Fontisto, FontAwesome5, SimpleLineIcons } from '@expo/vector-icons';
import { } from '@expo/vector-icons';
import Constants from 'expo-constants';
import Header from './Header';
import Footer from './Footer';
import RNPickerSelect from 'react-native-picker-select';
import Icon from 'react-native-vector-icons/Ionicons';
import ToggleSwitch from 'toggle-switch-react-native'
import { URL } from '../constants/API'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview'
// import {URL, IMAGE_URL} from '../constants/API'
console.disableYellowBox = true
import { color } from 'react-native-reanimated';
import { isRequired } from 'react-native/Libraries/DeprecatedPropTypes/DeprecatedColorPropType';
import * as ImagePicker from 'expo-image-picker';
import * as Permissions from "expo-permissions";
const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
import * as Localization from 'expo-localization';
import i18n from 'i18n-js'
import translations from './translations'
import { MenuProvider } from 'react-native-popup-menu';
import { Camera } from 'expo-camera';

import {
  Menu,
  MenuOptions,
  MenuOption,
  MenuTrigger,
} from 'react-native-popup-menu';
export default class Profile extends Component {
  // const [started, setStarted] = useState(false)
  constructor(props) {
    super(props);
    this.state = {
      started: false,
      full_name:"",
      phone:"",
      user_image: ""

    }
  
  }
  componentDidMount() {
    AsyncStorage.getItem("userID").then(user_data => {
      const val = JSON.parse(user_data);
      if (val) {
        this.setState({full_name:val.user.f_name,phone:val.user.phone_no})
       
     
       console.log(val)
      }
     

    });
    AsyncStorage.getItem("user_image").then(image => {
      const val_image = JSON.parse(image);

      if (val_image) {
    
        this.setState({
          user_image: val_image
        });
        // this.state.user_avatar = val_image;

      }
    });
    AsyncStorage.getItem("lang").then(lang => {
      const language = JSON.parse(lang);
      if (language) {
        i18n.translations = translations;
        i18n.locale = language;
        i18n.fallbacks = true;
      }

   

    });
    
  }

  getPermissionAsync = async () => {
    if (Platform.OS !== 'web') {
      
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      if (status !== 'granted') {
        alert('Sorry, we need camera roll permissions to make this work!');
      }
    }
  };

  pickImage = async () => {

    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });

    // // //console.log(result);

    if (!result.cancelled) {
      this.setState({ user_image1: result.uri, user_image: "" })
      AsyncStorage.setItem("user_image", JSON.stringify(result.uri));
     
    }
  };
  lang_switch=(language)=>{
    AsyncStorage.getItem("lang").then(lang => {
      const languageStored = JSON.parse(lang);
      i18n.translations = translations;
        i18n.locale = language;
        i18n.fallbacks = true;
      if (languageStored == language) {
        let new_lang = languageStored
        AsyncStorage.setItem("lang", JSON.stringify(new_lang));
      }else{
        AsyncStorage.setItem("lang", JSON.stringify(language));
      }

   

    });
  

    this.props.navigation.replace('profile')
       }
  logout=()=>{
    AsyncStorage.clear();
    this.props.navigation.reset({
      index: 0,
      routes: [{name: 'ModeSelection'}],
    })
    
       }

  render() {

    return (
      <MenuProvider style={{ width: screenWidth, height: screenHeight }}>
        <View style={{ width: screenWidth, height: screenHeight }}>
          <Header title={i18n.t('profile')} back={false} backtext={false} navigation={this.props.navigation} />
         <ScrollView style={{}} showsVerticalScrollIndicator={false}>
         <View style={{ width: "100%", marginVertical: 20, alignContent: "center", alignItems: "center", marginTop: 15,paddingBottom:80 }}>

<View style={styles.box} >

  {/* <Image style={{width:"100%",height:"100%",resizeMode:"cover",  borderRadius: 80}} source={{ uri: "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxITEhUSEhIVFRUWFxUYFRcWFxgWGBcVFhcXFhoXFhcdHSggGB0lHRUVITEhJSkrLi4uFx8zODMsNygtLisBCgoKDg0OGhAQGy0mICUtLS0vLTUtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAKkBKgMBIgACEQEDEQH/xAAbAAADAQEBAQEAAAAAAAAAAAACAwQFAQYAB//EADsQAAEDAgQDBgUCBQQCAwAAAAEAAhEDIQQxQVESYXEFE4GRofAiMrHB0QZCFFJi4fEjcoKikuKywtL/xAAaAQEAAwEBAQAAAAAAAAAAAAADAQIEBQAG/8QAKxEAAwACAQMCBAYDAAAAAAAAAAECAxEhBBIxQVETImHwBRSBkdHxMrHh/9oADAMBAAIRAxEAPwCUIwhCML7xnzLZ0IwFwBGFVhUzoCY0IQEwBG2BTOgJgCEBMAVGwaZ8AjAXwCMBUbBpnwCMBfAIwFRsCqPgEYC+ARgKjYNUcy+3NA6sBAOZ0+v+UnH4gU2l0XybOUna/ienJeYxeLc+m573hoBIgfvLZkbw3wjxXkti4sLyc+huY3tpjCY4dIkiXE7AXK8x2lVqPqS9/D/SM457fVQN7Wh/eFoMSb2k5AWyGVggpVHVB3jmhoJOWvMdEukjq4ek+Fzr9SnDUSROwgSZ4grMA4tkEW3P0SKDHvLTo3M6ALewuDD7HMZOixWe8hGbIpXzCcFRda0g5DbqVrUaZGkeMckTRFg2CI4iCP8AqFRT2yvnmb/RZ22zmZczo4JiZJI2uiptNiGwedpHROdSLWi4HOc/HdTV8QA20y03M2N9Bqq7Myru8FrYM2y8j0S8Q34SAOgmFFU7UD/hBLTofeSRWxpaYLptcqr2TOG9k7cfwjhcDYkggx5jVC/te2cHk3+6y69bn73UrnJO3Z1p6eXy0amGrAuLiSLeKsD7AiRtIWNhXQQcwDdaT6ypZ7JHPAL6wJ0kKarXKT2hXjhjOfRC9QPEaSYouIXweiAXHNOinZoTPqlSD4BJfVIyE80140NknI3UCTogeTKokpr6Qz0Su55/VQ0P3pnoQjAQtCNq+hZkphBGAuBG1UYNMIJgCEBG0KjAphAJjQhaEwBGwKZ1oTAEICY0KjYFM6AjAXAF17w0EnII2BTCScVi+Fp4G8bhADQRdx3OgGZKxsX2nBkmZH+m3InmdgsHtntIsaKbXQ993EGzAdG7E3JOem6t2GjF0dXSQzH15c52IrmBHE1sEgmYYI+FpjOJNr88qvjyRlwMaBABgNBJIbGtQ3JJygk5Q2d5Dg2Mm5TaToOpzPTYCM/E1eMhvES1s3/mJiXHmYEcgPG64O9h6deH/X39/XlbFcZsPDQRoFr4N7nZ5RYaCNgouz8NJGg6XXpey+y+IS3QwAfuiyWW6nLELRZgGtI+ISYFsp5LZoUzAsQ28AZAnnqosNTDYJ1BPgMls4muXNbwZRcDdZn5PneoyN1wfVAGtaJc57o8tuSW6sBmP87Aanmkfxhj4pgWBGfRIq1WEfC4kzmT79FCTAnG/Uqr4w8LtNtTPX7BSYp5cLu+K3ENDt4qatiQAL+nvzUzqgP8208X0up7TTjw65KDwwbgEZDUpGJcTfIxBnXdT1Hg3BMjU31jmhqPfY/N9RymVPaapjnZNUecks1F2sNr/XxUzpV9G2UmONUjIwufxhF/yL+ClqvSS+V5yNONMtZX4nAlUnEt3WXTcuucjckvGmzR7/a/0XBXO/4UgdYKhgAEnPQKjWiO1IoZWBz/AMrpgpH8SeSdTrA/MPFVaI00DwkIeI+wqntjp9P7IVUsqNQJjUATAvoGDTCaEwIGpjVRgUwgExqFoRhGwKYYCYAgamNCozPTCaEwBC0JjQjYFM+LgM1ldv8AaQYzhBu71Ewn9r1C1s6Lw3avaBeYGn2Ua9TT0nTfFpU/BLj8U7vS4zlA/HJS0WGtVEkgakXPQDU6In4ZxHGTA6pVLEcDpaBMQ3kd+qsfQTKU6jzrQ3E1LEARmIGTRHqefVT4XDmVYzDuf8QGdzpfVaGF7PcDeANfibPhe6hspWaYnWzuFpxFp+3VbODYdASSMuc5AaosNhWtjSSAIscsvIE+C2MKA27RG2/iVnqjjdT1HsI7qGy60j5Rd3PpC+r0S0yDpJaDtvv9EGKrimC8kgb69ANTmY/uvP4jF1K1ieCleefj+7opiHXPoBix1fO+B/afbTQQ2eOBcNy4uv4WecZiH/KAwZA5HzN/RC/u2/KP+R926KWri+a0zK9EdPHilLUr9/4GVMI9131T0ufv9k8PLQGh2UZt2WccQ7/P90o1iNfopct+TR8Onwzap1XH9zfFp/P2RNxjsiLbgzHhYn1WCMUd/oibjoVXiIfTs2xiQflMnUH76hJrVFntxTXZ56HUeOi4/EEayN9fFH8MlYdMKpUSuJE+CJlAG3up0h50G0lMmFxh98kqs4bI2SuWUmoqXvkArKo1vJXU3wjpEVOgnAyqqAPgvqBac7K6mWBUbBu/TR1w+C+yzu8d/MVVXxE2A80vvDuVVI9G0uTeCYEARhd1gUG0JjUtoTGo2BQxqY1LATA1GwKYbUxLa3ZNaUbM9MNpRNcdtUIamsEI2BTMrtqqeAWzkG+n5uvAY1l5iF+g9r4cQLZf2XjMbhZvH1UHV/D7lIyC4kAafdLFAkyArqeH5THl5q7D4bSPsPyV51o6dZVPglw9En4b+H0WzgsNwgRlvOXI7LlDCDig5ZdPDXNX18IeB0CXBpyuXMi8f1ajmAqOjnZs+3omwWOJcabz8THA/wC5rhw25SZnotKrixSkkwLGc/D7LznbOHd3bMVRNsnROh15ghS4nFCsBDiW5ls3Gk+seSRYlXJR9NOTVLx4f0aKMXinVncZs0WA0HvU+wvFV5EtMwLWgeCk7+bA5eHgpu8k8k/b7GycOv0CcS659/hVYTsqrVEsaYORi1jGa9D+meyqJirVOfyN+pduvRP7VpNECNRnoqO34SMXUfiLiuzFO2v2PJYT9IPM8ZMqw/pKmNVou/UICkrfqMEmAibyszfH6237Gbif06ybBZ9f9PgZLUf20FNW7X2Ur4hsxX1KMDE9lObkpHMeFuYjH8QWVWqe/wApp7n5OniyW18xM2sR0TRiEl51Sg+FLnZq7UzRpV0GJrWhRd/CRVrSqfD5PTi52OoVbrcwxtl5rDwNKSvR4SihypbKdQ0h1DPJVuZI57fhDTEe9Uyi0k/FM5j/ACszMDr1BFINzEnZd7w7N8gn1GDULvdt5eajZHcaTCi4+R8ktrU1rV22HQTTyKMePovgEYVGBR1juR9Pymh3I+n5QNTWhGwKYTD7smNXGouH39kbM9MYGo45n34JYJ29f7IxxbDxP9kbM9E2LYIJOxzdEc/lsvO4sZgtN8jb6Zleu7s6xGgk57m1ys7GURew5zpznUZdFRsXBm7Xo8q7D3+UxqLW5i6ppMuOEcVo09bqpwE8PhkT4EAZIaLm0wWtmJyDXwOpiVR7N9ZG0VU6FrzOwAn1FvFNZAbZ2dmgRnsTElTCqSMjnkA8f/VcLeJweyOIEEsdaeHQE5GNVCXuZaTfklx5dQcaog0nH/VbHwcRt3gj5djobE6leYxNMUncTBNNxMNnhcy9oOkgg7XXtxiKdam4McJ+QscQHtJPD8QPXNecxvZb6LiGND6ecZxJIyE2zyB8Fqxvg19JmSbmuH49t/8AfYwarST8Lr7OHC7x0PUI6VYEgOaf+Ik+SJ1Mgktu2flIBg8pkJbTJ0HS3h7CZHV8oeca64BMeXmETMSd4VHZ+B4rvmBvP0Q4nCAGGr3cvAHfj7u0SaxKNpVeA7EfUj9oM3P9ILjA1sCuUOzzJ+KABIJBuCAQY6GVDclHlx8pPwRVKSV3BK1f4Am065p1DCRLVHce/MJLyefqUnBTuB2XpMRgjsp3YDkvd40dTOjzzkioFtYnB/j8KKtg8jurqka8eWWZoC+ZTkp7aPxRutDDYS/X391FPQ9ZVI/s3CiJWsxsJWFoERC0e6WG3ycrLl3QkBMp5ohTTWUkTCdIfTAd+Fw4QbFHTYj4TufNU0H3a8DQjagajC7jEoYw+7pjZS2lNajZno65sixg9UdOpv7/ACFwJjVRgUEHjdMaRyQt95pjT7n6I2Z6CFQDX0TGOBtmUoVHf0+fkmOa47AjI7I2BSCZh4cXZzoch0spMdJMNtGZFyOQOnMfdWNDjaw01n/KRipALA2bZXPCMuK19+c+YNlJfzGK9sHhbA5QYA3N5/Pql1qLgRBJ5kMAA5WVX8C3Jry4DM3BvuAPcKfG0uERJ8ZuNiIt1R750bppNpL/AETPv+6fBkc/2qOriuBwdxGBYmBMeAEhXBsgZttZsgnq6J8kivRbwmwI5m534UktDy53po5VwNOrwOa8h8/OyzhDScxncBS4s4pk8Tu8G5kEiMj/AJUtT/TcHMtf5Q4nQ6RZPrYtxaTx2O5nQcloQqik16r6rwSupPcOJ1INO+u3zIsLhrzlGpKOpV+ES6U/CuLmwBbmvbYlVSkbSI4bKzsjsU1pdMAZ7kkWHTInks4OgQvbfp1obh2k2niJ8yPsFSqcrg5vW5qw49z5b0Z+KwgpHv3u4A0/DTYGmXOtAtrf12XjMZi4qEwAJ+USeG9hOsZLX/UmIJql0m4McjoOWq8o9pkTrEeKXFHqzX+H4Pl7rfLX3/Z7vs6hxNFp/uqX9n8LhIzWl2Bhw2izoEXaQ+JqzU+dHGrqG8rleOTMrYQKV+FELTqugLMqy5ROx8VU/Uxu1MKIPDfXyUlPCywzoT+VsVmAAydClPb8L4tc/QKzppHUx5WpSPMY/CcFSmdz9x+Vq4ajcWFiZnxSO1Wy6iOf3atRlOD4kD1P2U3Tco15MjcTv6lVPDnhyAHh6plSlGuXpyhNo3F7nSck6BkW2PSJ8FlbZz3b2RsEnKNpGaoZR6J2HYzTPzPSdEwAjUnko2eeT2Fd3C7wlWCkPf3XO4PuPyoKLIjOamNQApgXcZ0KDHv3CYD79hLCNo5BGwKDDQcwj4RoAULRyTQqMz0fBn9I8x+EQp/0D34LlR4aJP0nkmOtk2ekfdUYFH3czoG9ASV8XMyHASDDpPDHhe+SIE/yH/p+UymSP2H/AKeeaNg0zks1FLkOIfhF3rY4ZptJuINjGd7Xj08U9rz/ACn0/KPvT/KfNv8A+kbM9P72ZVZrS48L2hxsOF1yRob/AFU2Lw3GAJdxASYcSAfei08ZxEEGmeHY8HD5cUu8Vh16rhIDXnYktkDaJ9f8kNPZpw7rwyOlSdJBkCdc3cwdr5r6pwuAEcQAIkfSdUNSsbzYHOSDMb7Bd70QDJBOViPKyXk3NPyZuJwTJBDSLi3W33QfwTXzEz4xtK067iQRJvqdNs7lJ7xxAiwIMiwPnG6WaYqyXryS0MHAufNUUn8IsJ+iY2A6Dk4Wnf39V89mgCvvbKVXd5JHFe27GIdhm8s//KfuF4d1ivYdgHhHAflqDiYeeo97Kl+DJ+JTvGvo9n2I7JDnmZIe0tkZgm4d72WFR/TBDwXFxGgOl4gcl7ceq7CoslI5uPr8uNNJgYenwtA2CkxBl+WVlcUt0NBKMyw+dk+JoAt6LDxkCwVHaHahNgsl9TiV52jq9NhtLdEuJcTbcget/QFd4vgH9Tp8CS76IMQPPIeOZ8BKFzxxRsLe+iu1wdSVwjPxx48RTaNIPqT9gtcftHU+n/ssfs//AFKz6mgsPG1vAeq26LSSY0tvzP2UZONL2Q2bjU+yKaTN/T3ZU0acm2vgQgw9hII8ffqrKjGyDmMpG/PZZaZguudCnyTBuJzy9VQGOiImcnadQEqWzFzuSbeEKsPgDnkBtzKgOm+BbWkEfS5j8qjh5nzSKY/cZgTb7p/CeXkkU7K0YYRhAEbV2GdmhoKMBA1GCqMChjUxqUAETWznbYAx5wjYFIoYiazmfNKbTHP/AMj+UxrBz8z+VRmehzW8z5omjmUtrBz8z+U1jevmSjZnoNvUx4ouEnInx15cuq+aUQfcBGwK2K7hlyeK5mC2Y5D4clLiu7GUHP4SzPp8Nj9fUW42vwtymV57FOMz11Q2tiYMbvls7jMHScLNjIj/AE3C4uJEb+Sz6jDN7FoyizZ1G/3W1hCXN10i6lxlI8TT/u18lWb09GvHkafazMqNIgcRJtJ4RtmhZTIdAOckWGeoy8fNVFuQI3J6beqjxzXlst0uenJNL3waJe+Cg0Z1uI++XXX+yqFMFvvySsA/vGhzY45E3Pw9RsVbULeEusHAHiByJ3ncaHLRXTfgzZKaejzGOsV6n9LVRUocBMOaZadRzC8tiagqG329haPZbXUjy1JyCVzudGjqsffh7fXyewwzasnj4eo18FUFk0O1Gt+EyeZM+R1RVcdIsQgcs4NYLb5RbWxTW2m6ye0MXISXul05qWuVKk14enmXskquJSJjqcvyeQVNWANhuVE14MmZ66xuNANlZHUjlHzifm5QJz/yVK9pd8IzdMkbfuP0A8FRWdADnEwZtAnOPXbmjwFExJFzFthoOgufNQ60tjp9q2d7MwAY0NBm8nmTlI5BaLMNERzm5kmV3DYWIJg+OvLmtCIvcnymemSz3bbMuXM3QunRAz6c76HbVODRe0ixsYj89UTG2vYcxMjKeso6dMXic7g58pEI97MzoS9wJIcDPKJkbqinSO8DMNzNtyvg0NExfcayvmOi5uLZZ5JUj29rgN7m2B8vyhNQ7s8117QfL5eqAFv8vomSJSWjIBRtSmhMaumztUNCNqW1MCowaQYCa0pQRhGzPSHNTAUgAbJjUbApDweSMOS2lGEbM9Ia1NCQ1vXzRtbzPmqNAUg3U5ESlP7MpkyZ6SnMHXzRumLGCipBd1T4YirSa1toACyarXEk5AWAyk/bRPxdOoRxG4BuNugS2VAWni0vbOOvh6LNrRoxz2re9kb6BPF0Aj6z5pNFrssheN8votilT4jIFzeNHDQ8yqK2FaB3k2g8Q3jpkfwrzW+C76jXDPM4zCvw/wDr07fzNyHQXuOWn0KlR/iQA0XgcQJg3zEaiddOWS0KjXd4CSOG3DIyjobOifi+4EUN7LBh1M8DwLRYGP8A4lbZpJc+fcvWfUpvz7/yQs/T3dmWFxdoBYgcoN/PwTaDzB70G0nf0Jkb2Oq0Rj3M+Cu3T5oj+x8CnNfRqCxBsAJFxGkZrzqvUyVmyP8Az5+pj90wDO4MbG28iI8V0tbnxNy0I/KsxeEZEF0DhJmY8A0g8tViV8KAOLit4WnTNT3Sx8T7/UOrimiYI8/xKgfjthxHfID8rooyYuY52+iZ3bGZkA+R8Lk+Sq6lG2YmTPxLajyC6Y6ZdB9/VMp02sOd5sBJJP335eqtrF7h8DY5uET/AMcz4wlMoNaCZDnaHM8xGg6KrvaGV8aBoYfiMvFx8rQRafq73zNdOkN75Rf3OaDD0eI2bJ1OQb0/C1G4RszFx7k7oboLJk0we5IEXsBPXbmqadE55C3MnqfwnUW7Dx+0ao+NxFpDSYJOY0sEO2zFVtnzacjIx7OaZaYJE+njulsbwTm6+f5VHrIHKEkyGxZEgiDsbwOoCW6kRlbc7/hUPE2KAvsnmS0tiHmLZc19xnZDVF80PEN0iQ6XBjhG0pYRhdFnbpDWlGEoFMaVRg0hoTGlJamBGwKQ0I2pbSjaVRgUhzSmNKS0o2lG0Z6Q5pTAUkFG0qjQFIcCuucdEAKTXrxEI6QXZtjMS2dSJ1nXRZtGkWk2MzHJXtfBM5WnqidSMl2YMEDnkszRea7Vol4uAENzDpA2EX8ChL3vIJNpnh0mDBjlmj7u5BNySibTgqZWiXrz6iywzJyAgK7D4YxJMDNIglfPa7hzsMwkTYV7fBTh38XwkAi+xBHRQ9pdmsLSR8O/CQZ6g/QIqR2snVqLqnUDS3mkW09ook4vaekeYq978raptl8wAPSTuhq4Rro46p4gBMiL6xbKVu0uyyDBJJvBtH2K6cC4mGm2RsZbrmTkpdtm9dTK8P7/AGMZuC4Ww7jIzkOdF9wCiwuBAy4YM5WJC16mBeW8HBcWlpABHOSu0ezQ08TtNAYACN+CPzPD5M59Aizb6kbcgljCaxactepP4VGLrAkhthsNeaowtPiA3INpzVGmi3e5nbG4bDCIyGhGXkqBSAgG7ttEdKiWtIETpOiY6nMg5GLdEaltmOr2/Ig/FfMR0Fs+I5yEk1GTcl3XKOQt5qx7NjHKBHkkFo/lB6CEkxotLQ6jEWyXH2SmNIu3LUFNJSpHtcgOG3ghKIlA4pUhZQtwCX3fIJjihlXSHkwgjCWEYW9ncYwFGEsIwqMGhoKMFLajCowaQ0FGClhGFRgUhoKMFKajajYFIa0pgKUEbVRg0jtR1lO59xGwTKyQ3PwRUemVoY2XTHjzVmGJAAPgocOrmfMj0FlXoA+hEmZRM9VQuVV5oDu3wwWNtKW6xkJyU9eRVeQe6GYy22VFOwQUEVX5VJWuXoobuhJBt7lLw2SKpp1VfUPXOjonrtyU2NwvEPmNtNFWuFeJmmntHn6WFBJkxbMrTps4QzhAAIv5bqUaq5n7f9v4VmjXlpvyEHeXvJcjXVE5AV7QSPiUt3JEUBVkhJRw5WX0r5uSAK6QqR8SgJROQFIhZQJKCV0oVZDJH//Z" }}  /> */}
  {this.state.user_image ? (
    <Image style={{ width: "100%", height: "100%", resizeMode: "cover", borderRadius: 80 }} source={{ uri: this.state.user_image }} />
  ) : (
    <Image style={{ width: "100%", height: "100%", resizeMode: "cover", borderRadius: 80 }} source={{ uri: this.state.user_image1 }} />
  )}
  <TouchableOpacity style={{ position: "absolute", bottom: 20, right: 0, borderWidth: 1, borderColor: "#E7BF63", borderRadius: 50, padding: 6, backgroundColor: "#F0F0F0" }} 
  onPress={() => this.pickImage()}>

    <Fontisto name="camera" size={18} color="#E7BF63" />
  </TouchableOpacity>
</View>
<View style={{ alignItems: "center" }}>
  <View style={{ marginTop: 15 }}>
    <Text style={{ fontWeight: "bold", fontSize: 15, color: "#044184" }}>{this.state.full_name}</Text>
  </View>
  <View style={{ marginTop: 5, }}>
    <Text style={{ fontWeight: "bold", fontSize: 15, color: "#044184" }}>{this.state.phone}</Text>
  </View>
</View>

<View style={{ width: "100%", marginTop: 20, alignItems: "center", alignContent: "center" }}>
  <View style={{ width: "90%", backgroundColor: "#fff", paddingVertical: 15, borderRadius: 15 }}>
    {/* <View style={{ width: "100%", flexDirection: "row", padding: 5 }}>
      <View style={{ width: "20%" }}>
        <Entypo name="edit" size={20} color="#044184" />
      </View>
      <View style={{ width: "70%" }}>
        <Text style={{ fontSize: 18, color: "#044184" }}>{i18n.t('review')} {i18n.t('mandate')}</Text>
      </View>
      <TouchableOpacity style={{ width: "10%", }} onPress={() => this.props.navigation.navigate("reviewMandate")}>
        <AntDesign name="right" size={20} color="#E7BF63" />
      </TouchableOpacity>
    </View> */}
    <View style={{ width: "100%", flexDirection: "row", padding: 5, paddingTop: 15 }}  >
      <View style={{ width: "20%" }}>
        <MaterialIcons name="language" size={screenWidth >= 400 ? 40 : 20} color="#044184" />
      </View>
      <View style={{ width: "70%" }}>
        <Text style={{ fontSize: 18, color: "#044184" }}>{i18n.t('language')}</Text>
      </View>

      <TouchableOpacity style={{ width: "10%", }} >
        <Menu >
          <MenuTrigger style={{ marginTop: 10 }} >
            <AntDesign name="right" size={20} color="#E7BF63" />
          </MenuTrigger>
          <MenuOptions>
            {/* <MenuOption onSelect={() => this.exitChat()}  >
      <Text style={{ color: '#003cff', fontSize: 17, fontWeight: "bold" }}>English</Text>
    </MenuOption>  */}
            <MenuOption onSelect={() => this.lang_switch('en')}  >
              <Text style={{ color: '#003cff', fontSize: 17, fontWeight: "bold" }}>English</Text>
            </MenuOption>
            <MenuOption onSelect={() => this.lang_switch('gr')}  >
              <Text style={{ color: '#003cff', fontSize: 17, fontWeight: "bold" }}>Germen</Text>
            </MenuOption>
          </MenuOptions>

        </Menu>
      </TouchableOpacity>
    </View>
    <View style={{ width: "100%", flexDirection: "row", padding: 5, paddingTop: 15 }}  >
      <View style={{ width: "20%" }}>
        <Image style={screenWidth >= 400 ?  styles.box1_tablet : styles.box1} source={(require('../assets/images/BULB.png'))} />
      </View>
      <View style={{ width: "70%" }}>
        <Text style={{ fontSize: 18, color: "#044184" }}>{i18n.t('expert_assistance')}</Text>
      </View>
      <TouchableOpacity style={{ width: "10%", }} onPress={() => this.props.navigation.navigate("chat")}>
        <AntDesign name="right" size={20} color="#E7BF63" />
      </TouchableOpacity>
    </View>

    <View style={{ width: "100%", flexDirection: "row", padding: 5, paddingTop: 15 }}  >
      <View style={{ width: "20%" }}>
        <Image style={screenWidth >= 400 ?  styles.box1_tablet : styles.box1} source={(require('../assets/images/logo.png'))} />
      </View>
      <View style={{ width: "70%" }}>
        <Text style={{ fontSize: 18, color: "#044184" }}>{i18n.t('website')}</Text>
      </View>
      <TouchableOpacity style={{ width: "10%", }} onPress={ ()=>{ Linking.openURL('https://kabu-consultation.ch/en/home/')}}>
        <AntDesign name="right" size={20} color="#E7BF63" />
      </TouchableOpacity>
    </View>

  </View>
</View>
<View style={{ width: "100%", marginTop: 20, alignItems: "center", alignContent: "center" }}>
  <View style={{ width: "90%", backgroundColor: "#fff", paddingVertical: 15, borderRadius: 15 }}>
    <View style={{ width: "100%",alignItems:"center" }}>
   
        <Text style={{ fontSize: 18, color: "#E7BF63" }}>{i18n.t('Contact')} </Text>
  
    
    </View>
    <View style={{ width: "100%", flexDirection: "row", padding: 5, paddingTop: 15,alignItems:"center" }}  onPress={() => this.props.navigation.navigate("")}>
   
      <View style={{ width: "40%",alignItems:"center" }}>
        <Text style={{ fontSize: 15, color: "#044184" }} onPress={ ()=>{ Linking.openURL('tel:0438186415')}}>+41 43 818 64 15</Text>
      </View>
      <View style={{ width: "60%",alignItems:"center" }}>
        <Text style={{ fontSize: 15, color: "#044184" }} onPress={ ()=>{ Linking.openURL('mailto:info@kabu-consultation.ch')}}>info@kabu-consultation.ch</Text>
      </View>

    </View>
    

  </View>
</View>


<View style={{ width: "100%", marginTop: 20, alignItems: "center", alignContent: "center" }} >
<TouchableOpacity style={{ width: "90%", backgroundColor: "#fff", paddingVertical: 5, borderRadius: 15 }} onPress={() => this.logout()}>
  <View style={{ width: "100%", flexDirection: "row", padding: 3, paddingTop: 5 }}>

    <View style={{ width: "90%" }}>
      <Text style={{ fontSize: 18, color: "#044184", paddingLeft: 5 }}>{i18n.t('logout')}</Text>
    </View>
    <View style={{ width: "10%", }} >
      <SimpleLineIcons name="logout" size={20} color="#E7BF63" />
    </View>
  </View>

</TouchableOpacity>



</View>



</View>


         </ScrollView>
         
         
          <Footer title={"profile"} navigation={this.props.navigation} />

        </View>


      </MenuProvider>

    );
  }

}

const smallpickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 16,
    paddingVertical: 7,
    paddingHorizontal: 7,
    borderWidth: 1,
    borderColor: "#044184",
    color: "#B8B7BB",
    backgroundColor: "#fff",
    width: "100%",
    fontWeight: "bold",
    // to ensure the text is never behind the icon
  },
  inputAndroid: {
    fontSize: 16,
    paddingVertical: 6,
    paddingHorizontal: 7,
    fontWeight: "bold",
    borderWidth: 1,
    borderColor: "#044184",
    color: "#B8B7BB",
    backgroundColor: "#fff",
    width: "100%" // to ensure the text is never behind the icon
  }
});
const styles = StyleSheet.create({
  pic: {


    height: 100,
    width: 100,


  },
  Search_button: {
    width: "100%",
    paddingVertical: 15,
    alignItems: "center",
    backgroundColor: "#FF5A60",
    borderRadius: 30,
  },
  save_button: {
    width: "40%",
    paddingVertical: 10,
    alignItems: "center",
    backgroundColor: "#044184",
    borderRadius: 50,
  },
  logout_button: {
    width: "90%",
    marginTop: 10,
    paddingVertical: 15,
    alignItems: "center",
    backgroundColor: "#FF5A60",
    borderRadius: 50,
  },
  icon: {

    width: "20%",
    alignItems: "center",
    justifyContent: "center",
    paddingBottom: 5,
    marginTop: 5
  },
  box: {

    width: 150,
    height: 150,
    borderRadius: 80
    , borderWidth: 1, borderColor: "#E7BF63",
    // position:"absolute"


  },
  box1: {

    width: "40%",
    height: 25,
    resizeMode: "cover",
    borderRadius: 80


  },
  box1_tablet: {

    width: 50,
    height: 50,
    resizeMode: "cover",
    borderRadius: 50


  },

});

