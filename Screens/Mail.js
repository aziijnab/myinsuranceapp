import React, { Component } from 'react';
import {
  View, Text, TouchableOpacity, Image, StyleSheet, Picker, Dimensions, ImageBackground, TextInput, Alert, AsyncStorage, Keyboard, KeyboardAvoidingView
} from 'react-native';
import { URL } from '../constants/API'
import { Textarea, Form } from "native-base";
console.disableYellowBox = true
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { MaterialIcons, MaterialCommunityIcons } from '@expo/vector-icons';
const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
import Header from './Header'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview'
import i18n from 'i18n-js'
import translations from './translations'
export default class Mail extends Component {
  // const [started, setStarted] = useState(false)
  constructor(props) {
    super(props);
    this.data=this.props.route.params
    this.state = {
      started: false,
      ta: "",
      keyboardHeight: 15,
      user_id: "",
      lang_done:false ,
    }

  }
  componentDidMount() {
    AsyncStorage.getItem("userID").then(user_data => {
      const val = JSON.parse(user_data);
      // console.log(val);
      if (val) {
        this.setState({user_id:val.user.uuid})
        console.log(">>>>>>>>>>>>>>>>>>user_id>>>>>>>>>>")
        console.log(user_id)
 
 
        console.log(">>>>>>>>>>>>>>>>>company.data>>>>>>>>>>>>")
        console.log(this.data.companies_data.uuid)
      }

    });
    AsyncStorage.getItem("lang").then(lang => {
      const language = JSON.parse(lang);
      console.log("--a--")
      console.log(language)
      console.log(language)
      console.log("--a--")
      if (language) {  
        console.log("----")
        console.log(language)
        console.log(language)
        console.log("----")
        i18n.translations = translations;
        i18n.locale = language;
        i18n.fallbacks = true;
      
      }
      this.setState({lang_done : true})
    });
    this.keyboardDidShowListener = Keyboard.addListener(
      "keyboardDidShow",
      this._keyboardDidShow
    );
    this.keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      this._keyboardDidHide,
    );
  }
  

_keyboardDidShow = e => {
  var KeyboardHeight = e.endCoordinates.height;
  this.setState({
    keyboardHeight: KeyboardHeight,
  });
};
_keyboardDidHide = e => {
  this.setState({
    keyboardHeight: 0,
  });
};
    send=()=>{
      let ta=this.state.ta;
      if(ta ==""){
          Alert.alert(
              "Sorry",
              "fill the required text area",
              
              [
                {
                  text: "Cancel",
                  style: "cancel"
                },
                { text: "OK"}
              ],
              { cancelable: false }
            );
      }else{
          console.log(ta)
          fetch(URL+"request-quote", {
            method: "POST",
            body: JSON.stringify({
              "company_id":this.data.companies_data.uuid,
              "user_id":this.state.user_id,
              "description": this.state.ta
             
              }),
              headers: {
                "Content-Type": "application/json"
              }
              
          })
            .then(res => res.json())
            .then(async response => {
              console.log(">>>>>>>>>>>>>>>>>>>>submit data>>>>>>>>>>>");
              console.log(response);
              if(response.response == "success"){
                Alert.alert(
                  "Success",
                  response.message,
                  [
                    {
                      text: "Cancel",
                      style: "cancel"
                    },
                    { text: "OK"}
                  ],
                  { cancelable: false }
                );
                this.props.navigation.navigate("dashboard");
      
              }else{
                Alert.alert(
                  "Sorry",
                  response.message,
                  [
                    {
                      text: "Cancel",
                      style: "cancel"
                    },
                    { text: "OK"}
                  ],
                  { cancelable: false }
                );
              }
             
             
            })
            .catch(error => {
              alert("Please check internet connection");
            });
        }
      
      
      }
      render() {
        if(this.state.lang_done){
        return (
    
          
          <View style={{ width:screenWidth,height:screenHeight }}>
          <KeyboardAwareScrollView
    contentContainerStyle={{ flexGrow: 1 }}
    >
              <Header title={i18n.t('ask')} back={true} backtext={false} navigation={this.props.navigation} />
    
              <View style={{ width: "100%", alignContent: "center", alignItems: "center" ,marginTop:20}}>
                <Image style={styles.pic} source={require('../assets/images/logo.png')} />
              </View>
              <View style={{ width: "100%", alignItems: "center" ,marginTop:20}}>
                <View style={{ width:"90%" }}>
    <KeyboardAvoidingView>
    
    
                  <Form >
    
                    <Textarea style={{ borderColor: "#DCDCDC", fontSize: 20, color: "grey", borderRadius: 15 }} 
                    rowSpan={4} 
                    bordered placeholder={i18n.t('Description')}
                    placeholderTextColor="#044184"
                      onChangeText={(ta) => this.setState({ ta })}
                      value={this.state.ta}
    
                    />
                  </Form>
    
    </KeyboardAvoidingView>
    
                </View>
              </View>
          
              <View style={{width:"100%",position:"absolute",alignItems:"center",bottom:Platform.OS === 'ios' ? 0:this.state.keyboardHeight}} >


<View style={{ width: screenWidth <= 400 ? "90%" : "45%", justifyContent: "center", alignItems: "center", alignContent: "center" }}>
  <TouchableOpacity style={styles.SignUp_button} 
  // onPress={() => this.props.navigation.navigate('dashboard')}
  onPress={() => this.send()}
   >

      {/* <EvilIcons style={{}} name="sc-facebook" size={24} color="#ffffff" /> */}
      <Text style={{ color: '#fff', alignItems: "center", marginTop: 2, fontSize: 18, fontWeight: "bold" }}>{i18n.t('Send')}</Text>
  
  </TouchableOpacity>
</View>






</View>
    
    
            </KeyboardAwareScrollView>
          </View>
    
    
    
    
        );
      }
    
      else{
        return(
          <View>
            <Text>Wait</Text>
          </View>
        )
      }
          
        }

}
const styles = StyleSheet.create({

  getstarted_button: {
    width: "60%",
    paddingVertical: 17,
    position: "absolute", zIndex: 100,
    alignItems: "center",
    backgroundColor: "#208061",
    borderRadius: 50,
    bottom: 40

  },
  icon: {

    width: "20%",
    alignItems: "center",
    justifyContent: "center",
    paddingBottom: 5,
    marginTop: 5

  },

  search: {

    width: "100%",
    marginTop: 10,
    paddingVertical: 15,

    backgroundColor: "#F5F5F5",
    borderRadius: 50,
  },
  box: {

    width: 150,
    height: 150,
    shadowColor: "grey",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.13,
    shadowRadius: 2.62,

    elevation: 4,



  },
  pic: {


    height: 250,
    width: "100%",
    resizeMode:"contain"


  },
  login_button: {
    width: "100%",
    paddingVertical: 15,
    alignItems: "center",
    alignContent: "center",

    backgroundColor: "#FF5A60",
    borderRadius: 50,
  },
  SignUp_button: {

    width: "90%",

   
    paddingVertical: 10,
    alignItems: "center",
    alignContent: "center",
    backgroundColor: "#E7BF63",
    borderRadius: 10,
    borderWidth: 1,
    borderColor: "#E7BF63"
  },
});