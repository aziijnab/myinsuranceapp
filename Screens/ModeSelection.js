//import React from 'react';
import React, { Component } from "react";
import {
  StyleSheet,
  StatusBar,
  Text,
  View,
  Dimensions,
  AsyncStorage,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ActivityIndicator,
  ScrollView,
  image
} from "react-native";
console.disableYellowBox = true;
import Footer from './Footer';
import { LinearGradient } from 'expo-linear-gradient';
import Icon from 'react-native-vector-icons/Ionicons';
import Constants from "expo-constants";
import { Container, Picker, Content, Button } from "native-base";

import * as ImagePicker from 'expo-image-picker';
import { MaterialIcons, Ionicons, EvilIcons, MaterialCommunityIcons, Octicons, Feather, Entypo, AntDesign } from '@expo/vector-icons';

import { Notifications } from "expo";
import * as Permissions from "expo-permissions";
import { TextInput } from "react-native-gesture-handler";
const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;
import i18n from 'i18n-js'
import translations from './translations'
export default class ModeSelection extends Component {
  constructor(props) {
    super(props);
    this.state = {
      lang_done:false
    };
  }
  componentDidMount() {
  
   
    // AsyncStorage.getItem("lang").then(lang => {
    //   const language = JSON.parse(lang);
    //   if (language) {
    //     i18n.translations = translations;
    //     i18n.locale = language;
    //     i18n.fallbacks = true;
    //   }

    // });
    // AsyncStorage.getItem("lang").then(lang => {
    //   const language = JSON.parse(lang);
    //   console.log("--a--")
    //   console.log(language)
    //   console.log(language)
    //   console.log("--a--")
    //   if (language) {  
    //     console.log("----")
    //     console.log(language)
    //     console.log(language)
    //     console.log("----")
    //     i18n.translations = translations;
    //     i18n.locale = language;
    //     i18n.fallbacks = true;
      
    //   }
    //   this.setState({lang_done : true})
    // });
    
  }
 
  lang_switch=(language)=>{
    i18n.translations = translations;
    i18n.locale = language;
    i18n.fallbacks = true;
    AsyncStorage.setItem("lang", JSON.stringify(language));
    console.log("langauge")
    console.log(language)
    // AsyncStorage.getItem("lang").then(lang => {
    //   const languageStored = JSON.parse(lang);
     
    //   if (languageStored == language) {
    //     let new_lang = languageStored
 
    //   }else{
    //     AsyncStorage.setItem("lang", JSON.stringify(language));
    //   }

   

    // });
  

    this.props.navigation.push('home')
       }
 
 
 
  render() {

    // if(this.state.lang_done){
    return (


      <LinearGradient style={{ width: screenWidth, height: screenHeight }} colors={['#fff', '#fff']}>
      

          <View style={{ flex: 1 }}>
          <View style={{ width: "100%", alignContent: "center", alignItems: "center",marginTop:80 }}>
          <Image style={styles.pic} source={require('../assets/images/logo.png')} />
        </View>
        <View style={{ width: "100%", marginTop: 40 }}>
        <View style={{width:"90%",alignItems:"center"}}>
                <Text style={{ fontWeight: "bold", fontSize: 20, color: "#044184", paddingLeft: 20,textAlign:"center" }}>
                 Before you start,kindly choose your suitable language 
          </Text>
              </View>
</View>
               <TouchableOpacity  style={{paddingVertical:10,alignItems:"center",marginTop:80}} onPress={() =>{
                 this.lang_switch('en')
               }} >
           <LinearGradient style={styles.login_button  } colors={['#E7BF63', '#E7BF63']}>
               <Text style={{ color: '#fff',fontSize:20 }}>English</Text>
                </LinearGradient>
                </TouchableOpacity>  
                   <TouchableOpacity  style={{paddingVertical:10,alignItems:"center",marginTop:40}} onPress={() =>{
                 this.lang_switch('gr')
               }} >
           <LinearGradient style={styles.login_button  } colors={['#044184', '#044184']}>
               <Text style={{ color: '#fff',fontSize:20 }}>German</Text>
                </LinearGradient>
                </TouchableOpacity>

             




          </View>
        
         





      </LinearGradient>
  );
}
// else{
//   return(
//     <View>
//       <Text>Wait</Text>
//     </View>
//   )
// }
   
  }

const styles = StyleSheet.create({
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: 'white',
  },
  search: {

    width: "100%",
    marginTop: 10,
    paddingVertical: 15,

    backgroundColor: "#F5F5F5",
    borderRadius: 50,
  },
  box: {

    width: "100%",
    height: 60,
    resizeMode: "cover",
    borderRadius: 80



  },
  login_button: {
    width: "50%",
    paddingVertical: 10,
    alignItems: "center",
    alignContent: "center",

    backgroundColor: "#fff",
    borderRadius: 10,
  },
  getstarted_button: {
    width: "90%",
    // top: 80,
    marginHorizontal: 20,
    paddingVertical: 10,
    alignItems: "center",
    backgroundColor: "#fff",
    borderRadius: 10,

    bottom: 10


  },
  pic: {


    height: 250,
    width: "100%",
    resizeMode:"contain",
    paddingTop:50


  },
})





















































{/* {this.state.isLoading ? (
                 <Button
                   bordered
                   style={{
                     width: "100%",
                     alignItems: "center",
                     justifyContent: "center",
                     borderColor: fontColor,
                     borderRadius: 5,
                     alignItems:"center"
                   }}
                 >
                   <ActivityIndicator color={"#fd7e14"} size={"small"} />
                 </Button>
               ) : (
               
               <TouchableOpacity onPress={()=>this.props.navigation.navigate('ChatDashboard')}>
                   <image source={require('../assets/images/enter.png')}/>
                   </TouchableOpacity>
               )} */}
