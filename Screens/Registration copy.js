import React, { Component } from 'react';
import {
  View, Text, TouchableOpacity, Image, StyleSheet, Picker, Dimensions, ImageBackground, TextInput, Alert, AsyncStorage
} from 'react-native';
import { MaterialIcons, Ionicons, EvilIcons, MaterialCommunityIcons, Octicons, Feather, Entypo, AntDesign, FontAwesome5, Zocial } from '@expo/vector-icons';
import { ScrollView } from 'react-native-gesture-handler';
const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
import Header from './Header'
import { URL } from '../constants/API'
console.disableYellowBox = true
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview'

export default class Registration extends Component {
  constructor(props) {
    super(props);
    this.state = {
      started: false,
      f_name: "",
      s_name: "",
      email: "",
      password: "",
      phone: "",
      address: "",
      zip_code: "",
      city: "",
      refer: "",

    }

  }
  sendRequest = () => {
    let f_name = this.state.f_name;
    let s_name = this.state.s_name;
    let email = this.state.email;
    let password = this.state.password;
    let phone = this.state.phone;
    let address = this.state.address;
    let zip_code = this.state.zip_code;
    let city = this.state.city;
    let refer = this.state.refer;

    fetch(URL + "register", {
      method: "POST",
      body: JSON.stringify({
        "f_name": f_name,
        "sur_name": s_name,
        "email": email,
        "password": password,
        "phone_no": phone,
        "address": address,
        "zip_code": zip_code,
        "city": city,
        "access_code": refer,
      })
    })
      .then(res => res.json())
      .then(async response => {
        console.log(response);
        if (response.response == "success") {
          Alert.alert(
            "Success",
            response.message,
            [
              {
                text: "Cancel",
                style: "cancel"
              },
              { text: "OK" }
            ],
            { cancelable: false }
          );
          this.props.navigation.push("login");

        }
        else {
          Alert.alert(
            "Error",
            response.message,
            [
              {
                text: "Cancel",
                style: "cancel"
              },
              { text: "OK" }
            ],
            { cancelable: false }
          );
        }

      }
      )
      .catch(error => {
        alert("Please check internet connection");

      });

  }
  submit = () => {
    let f_name = this.state.f_name;
    let s_name = this.state.s_name;
    let email = this.state.email;
    let password = this.state.password;
    let phone = this.state.phone;
    let address = this.state.address;
    let zip_code = this.state.zip_code;
    let city = this.state.city;
    let refer = this.state.refer;

    //  let email_regex = "^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$";
    var email_regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;


    if (f_name == "" || s_name == "" || address == "" || email == "" || city == "" || zip_code == "" || phone == "" || password == "" || refer == "") {
      Alert.alert(
        "My_Insurance ",
        "Please enter all fields ",
        [
          {
            text: "Cancel",
            style: "cancel"
          },
          { text: "OK" }
        ],
        { cancelable: false }
      );
    }
    else {
      if (email != "") {
        if (!email_regex.test(email)) {
          Alert.alert(
            "Sorry",
            "Email is not correct",
            [
              {
                text: "Cancel",
                style: "cancel"
              },
              { text: "OK" }
            ],
            { cancelable: false }
          );
        }
        else {
          this.sendRequest();
        }




      }

    }
  }



  render() {
    return (

      <View style={{ width: screenWidth, height: screenHeight, alignItems: "center", backgroundColor: "" }}>


      </View>


    );
  }

}
const styles = StyleSheet.create({
  pic: {


    height: 250,
    width: "100%",


  },
  login_button: {
    width: "80%",
    marginLeft: 40,
    marginTop: 10,
    paddingVertical: 8,
    alignItems: "center",
    alignContent: "center",
    backgroundColor: "#808080",
    borderRadius: 10
  },
  getstarted_button: {
    width: "90%",
    // top: 80,
    marginHorizontal: 20,
    paddingVertical: 10,
    alignItems: "center",
    backgroundColor: "#fff",
    borderRadius: 10,

    bottom: 0


  },
  SignUp_button: {

    width: "90%",


    paddingVertical: 10,
    alignItems: "center",
    alignContent: "center",
    backgroundColor: "#E7BF63",
    borderRadius: 10,
    borderWidth: 1,
    borderColor: "#E7BF63"
  },
});