import React, { Component } from 'react';
import { LinearGradient } from 'expo-linear-gradient';
import { View, Text, TouchableOpacity, Image, StyleSheet, Picker, Dimensions, AsyncStorage, ScrollView, Alert } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { MaterialIcons, Ionicons, EvilIcons, MaterialCommunityIcons, Octicons, Feather, Entypo, SimpleLineIcons } from '@expo/vector-icons';
import Constants from 'expo-constants';

import { color } from 'react-native-reanimated';
import { isRequired } from 'react-native/Libraries/DeprecatedPropTypes/DeprecatedColorPropType';
const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
import i18n from 'i18n-js'
import translations from './translations'

export default class Footer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      page: "",
      loggedIn: false,
      lang_done:false
      
    }

  }
  componentDidMount() {

    AsyncStorage.getItem("userID").then(user_data => {
      const val = JSON.parse(user_data);
      if (val) {

        this.setState({
          loggedIn: true,
        });
      }

    });
    AsyncStorage.getItem("lang").then(lang => {
      const language = JSON.parse(lang);
      console.log("--a--")
      console.log(language)
      console.log(language)
      console.log("--a--")
      if (language) {  
        console.log("----")
        console.log(language)
        console.log(language)
        console.log("----")
        i18n.translations = translations;
        i18n.locale = language;
        i18n.fallbacks = true;
      
      }
      this.setState({lang_done : true})
    });
  }

  render() {
    if(this.state.lang_done){
    return (
      <View style={{ width: "100%", position: "absolute", bottom: 10, zIndex: 0, alignItems: "center", alignContent: "center" }}>
        <View style={{ width: "50%", flexDirection: "row", backgroundColor: '#FEFEFE', paddingVertical: 1, paddingHorizontal: 5, borderBottomLeftRadius: 15, borderTopRightRadius: 15, alignItems: "center", alignContent: "center" }}>
          <View style={styles.icon}>
            <TouchableOpacity style={{ width: "100%", alignItems: "center", backgroundColor: this.props.title == "Home" ? "#FAF1EF" : "#fff", borderBottomLeftRadius: 15 }} onPress={() => this.props.navigation.replace('dashboard')}  >

              <View style={{ width: "100%", paddingTop: 10, paddingBottom: 5, alignItems: "center" }}>
                <View>
                  <Octicons name="person" size={24} color={this.props.title == "Home" ? "#E7BF63" : "#044184"} />
                </View>
                <Text style={{ color: this.props.title == "Home" ? "#E7BF63" : "#044184", textAlign: "center", marginTop: 2 }} >{i18n.t('Home')}</Text>

              </View>
            </TouchableOpacity>

          </View>

          <View style={styles.icon}>
            <TouchableOpacity style={{ width: "100%", alignItems: "center", backgroundColor: this.props.title == "profile" ? "#FAF1EF" : "#fff", borderTopRightRadius: 15 }} onPress={() => this.props.navigation.replace('profile')}>

              <View style={{ width: "95%", paddingTop: 10, paddingBottom: 5, alignItems: "center" }}>
                <View >
                  <MaterialIcons name="chat-bubble-outline" size={24} color={this.props.title == "profile" ? "#E7BF63" : "#044184"} />
                </View>
                <Text style={{ color: this.props.title == "profile" ? "#E7BF63" : "#044184", textAlign: "center", marginTop: 1 }} >{i18n.t('profile')} </Text>
              </View>
            </TouchableOpacity>

          </View>


        </View>



      </View>


    );
  }
  else{
    return(
      <View>
        <Text>Wait</Text>
      </View>
    )
  }
      
    }
  
  }
const styles = StyleSheet.create({
  icon: {

    width: "50%",
    alignItems: "center",
    justifyContent: "center",
  },
  searchicon: {
    width: "24%",
    alignItems: "center",
    justifyContent: "center",
  },
})