import React, { Component } from 'react';
import {
  View, Text, TouchableOpacity, Image, StyleSheet, Picker, Dimensions, ImageBackground, TextInput, Alert, AsyncStorage
} from 'react-native';
import { Ionicons, EvilIcons, MaterialIcons } from '@expo/vector-icons';
import { ScrollView } from 'react-native-gesture-handler';
const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
import { URL } from '../constants/API'
console.disableYellowBox = true
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview'
import Footer from './Footer';
import Header from './Header'


export default class Offer extends Component {
  // const [started, setStarted] = useState(false)
  constructor(props) {
    super(props);
    this.state = {
      started: false,
      login_email: "",
      login_password: "",
      register_email: "",
      register_password: "",
    }

  }
  submit = () => {
    let login_email = this.state.login_email
    let login_password = this.state.login_password

    if (login_email == "" && login_password == "") {
      Alert.alert(
        "Sorry",
        " E-mail and Password is required",
        [
          {
            text: "Cancel",
            style: "cancel"
          },
          { text: "OK" }
        ],
        { cancelable: false }
      );
    }

    if (login_email != "" && login_password == "") {
      Alert.alert(
        "Sorry",
        "Password is required",
        [
          {
            text: "Cancel",
            style: "cancel"
          },
          { text: "OK" }
        ],
        { cancelable: false }
      );
    }
    if (login_email == "" && login_password != "") {
      Alert.alert(
        "Sorry",
        "E-mail is required",
        [
          {
            text: "Cancel",
            style: "cancel"
          },
          { text: "OK" }
        ],
        { cancelable: false }
      );
    }
    if (login_email != "" && login_password != "") {

      console.log(login_email + "---" + login_password)
      fetch(URL + "login", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          "email": login_email,
          "password": login_password
        })
      })
        .then(res => res.json())
        .then(async response => {
          console.log(response);
          if (response.response == "success") {

            AsyncStorage.setItem("userID", JSON.stringify(response));
            AsyncStorage.setItem("setting", JSON.stringify(response.setting));
            Alert.alert(
              "Success",
              response.message,
              [
                {
                  text: "Cancel",
                  style: "cancel"
                },
                { text: "OK" }
              ],
              { cancelable: false }
            );
            this.props.navigation.navigate("workouts");

          }
          else {
            Alert.alert(
              "Sorry",
              response.message,
              [
                {
                  text: "Cancel",
                  style: "cancel"
                },
                { text: "OK" }
              ],
              { cancelable: false }
            );
          }


        })
        .catch(error => {
          console.log(error)
          alert("Please check internet connection");
        });

    }

  }
  register = () => {
    let register_email = this.state.register_email
    let register_password = this.state.register_password

    if (register_email == "" && register_password == "") {
      Alert.alert(
        "Sorry",
        " E-mail and Password is required",
        [
          {
            text: "Cancel",
            style: "cancel"
          },
          { text: "OK" }
        ],
        { cancelable: false }
      );
    }

    if (register_email != "" && register_password == "") {
      Alert.alert(
        "Sorry",
        "Password is required",
        [
          {
            text: "Cancel",
            style: "cancel"
          },
          { text: "OK" }
        ],
        { cancelable: false }
      );
    }
    if (register_email == "" && register_password != "") {
      Alert.alert(
        "Sorry",
        "E-mail is required",
        [
          {
            text: "Cancel",
            style: "cancel"
          },
          { text: "OK" }
        ],
        { cancelable: false }
      );
    }
    if (register_email != "" && register_password != "") {

      console.log(register_email + "---" + register_password)
      fetch(URL + "register", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          "email": register_email,
          "password": register_password
        })
      })
        .then(res => res.json())
        .then(async response => {
          console.log(response);
          if (response.response == "success") {
            AsyncStorage.setItem("userID", JSON.stringify(response));
            Alert.alert(
              "Success",
              response.message,
              [
                {
                  text: "Cancel",
                  style: "cancel"
                },
                { text: "OK" }
              ],
              { cancelable: false }
            );
            this.props.navigation.navigate("login");

          }
          else {
            Alert.alert(
              "Sorry",
              response.message,
              [
                {
                  text: "Cancel",
                  style: "cancel"
                },
                { text: "OK" }
              ],
              { cancelable: false }
            );
          }


        })
        .catch(error => {
          console.log(error)
          alert("Please check internet connection");
        });

    }

  }

  render() {
    return (

      <View style={{ width: screenWidth, height: screenHeight, alignItems: "center" }}>
        <Header title={"Offers"} back={true} backtext={false} navigation={this.props.navigation} />

        <View style={{ width: "100%", marginTop: 20 }}>
          <View style={{ width: "90%" }}>
            <Text style={{ fontWeight: "bold", fontSize: 15, color: "#9ca2aa", paddingLeft: 20, textAlign: "center" }}>
              For which insurance would you like to receive an offer
          </Text>
          </View>
        </View>
          <View style={{ width: "100%", paddingTop: 5, paddingHorizontal: 10 }}>
            <View style={{ flexDirection: 'row', flexWrap: 'wrap', justifyContent: "space-between", marginTop: 20 }}>

            <View style={styles.popbox}>
              <TouchableOpacity style={{ width: "100%", padding: 5 }}
              onPress={() => this.props.navigation.navigate("uploadPolicy")}
              >
                <View style={{ width: "100%", alignItems: "center", justifyContent: "center" }}>

                  <Image style={{ width: "100%", height: 100, resizeMode: "stretch", borderRadius: 10 }} source={require('../assets/images/1.png')} />


                </View>
                <View style={{ alignItems: "center", justifyContent: "center" }}>
                  <Text style={{ fontWeight: "bold", fontSize: 12, paddingTop: 6, color: "#044184" }}>
                  health Insurance
                        </Text>
                </View>

              </TouchableOpacity>
            </View> 
             <View style={styles.popbox}>
              <TouchableOpacity style={{ width: "100%", padding: 5 }}
                  onPress={() => this.props.navigation.navigate("uploadPolicy")}
              >
                <View style={{ width: "100%", alignItems: "center", justifyContent: "center" }}>

                  <Image style={{ width: "100%", height: 100, resizeMode: "stretch", borderRadius: 10 }} source={require('../assets/images/2.png')} />


                </View>
                <View style={{ alignItems: "center", justifyContent: "center" }}>
                  <Text style={{ fontWeight: "bold", fontSize: 12, paddingTop: 6, color: "#044184" }}>
                Motor Vehicle
                        </Text>
                </View>

              </TouchableOpacity>
            </View>  
            <View style={styles.popbox}>
              <TouchableOpacity style={{ width: "100%", padding: 5 }}
                  onPress={() => this.props.navigation.navigate("uploadPolicy")}
              >
                <View style={{ width: "100%", alignItems: "center", justifyContent: "center" }}>

                  <Image style={{ width: "100%", height: 100, resizeMode: "stretch", borderRadius: 10 }} source={require('../assets/images/3.png')} />


                </View>
                <View style={{ alignItems: "center", justifyContent: "center" }}>
                  <Text style={{ fontWeight: "bold", fontSize: 12, paddingTop: 6, color: "#044184" }}>
                  Household
                        </Text>
                </View>

              </TouchableOpacity>
            </View>
            <View style={styles.popbox}>
              <TouchableOpacity style={{ width: "100%", padding: 5 }}
                  onPress={() => this.props.navigation.navigate("uploadPolicy")}
              >
                <View style={{ width: "100%", alignItems: "center", justifyContent: "center" }}>

                  <Image style={{ width: "100%", height: 100, resizeMode: "stretch", borderRadius: 10 }} source={require('../assets/images/4.png')} />


                </View>
                <View style={{ alignItems: "center", justifyContent: "center" }}>
                  <Text style={{ fontWeight: "bold", fontSize: 12, paddingTop: 6, color: "#044184" }}>
                  Life
                        </Text>
                </View>

              </TouchableOpacity>
            </View> 
             <View style={styles.popbox}>
              <TouchableOpacity style={{ width: "100%", padding: 5 }}
                  onPress={() => this.props.navigation.navigate("uploadPolicy")}
              >
                <View style={{ width: "100%", alignItems: "center", justifyContent: "center" }}>

                  <Image style={{ width: "100%", height: 100, resizeMode: "stretch", borderRadius: 10 }} source={require('../assets/images/5.png')} />


                </View>
                <View style={{ alignItems: "center", justifyContent: "center" }}>
                  <Text style={{ fontWeight: "bold", fontSize: 12, paddingTop: 6, color: "#044184" }}>
                  Legal Protection
                        </Text>
                </View>

              </TouchableOpacity>
            </View>  
            <View style={styles.popbox}>
              <TouchableOpacity style={{ width: "100%", padding: 5 }}
                  onPress={() => this.props.navigation.navigate("uploadPolicy")}
              >
                <View style={{ width: "100%", alignItems: "center", justifyContent: "center" }}>

                  <Image style={{ width: "100%", height: 100, resizeMode: "stretch", borderRadius: 10 }} source={require('../assets/images/6.png')} />


                </View>
                <View style={{ alignItems: "center", justifyContent: "center" }}>
                  <Text style={{ fontWeight: "bold", fontSize: 12, paddingTop: 6, color: "#044184" }}>
                  Travel
                        </Text>
                </View>

              </TouchableOpacity>
            </View>
            <View style={styles.popbox}>
              <TouchableOpacity style={{ width: "100%", padding: 5 }}
                  onPress={() => this.props.navigation.navigate("uploadPolicy")}
              >
                <View style={{ width: "100%", alignItems: "center", justifyContent: "center" }}>

                  <Image style={{ width: "100%", height: 100, resizeMode: "stretch", borderRadius: 10 }} source={require('../assets/images/8.png')} />


                </View>
                <View style={{ alignItems: "center", justifyContent: "center" }}>
                  <Text style={{ fontWeight: "bold", fontSize: 12, paddingTop: 6, color: "#044184" }}>
                Building
                        </Text>
                </View>

              </TouchableOpacity>
            </View>  
            <View style={styles.popbox}>
              <TouchableOpacity style={{ width: "100%", padding: 5 }}
                  onPress={() => this.props.navigation.navigate("uploadPolicy")}
              >
                <View style={{ width: "100%", alignItems: "center", justifyContent: "center" }}>

                  <Image style={{ width: "100%", height: 100, resizeMode: "stretch", borderRadius: 10 }} source={require('../assets/images/9.png')} />


                </View>
                <View style={{ alignItems: "center", justifyContent: "center" }}>
                  <Text style={{ fontWeight: "bold", fontSize: 12, paddingTop: 6, color: "#044184" }}>
                  Personal Liability
                        </Text>
                </View>

              </TouchableOpacity>
            </View>
            <View style={styles.popbox}>
              <TouchableOpacity style={{ width: "100%", padding: 5 }}
                  onPress={() => this.props.navigation.navigate("uploadPolicy")}
              >
                <View style={{ width: "100%", alignItems: "center", justifyContent: "center" }}>

                  <Image style={{ width: "100%", height: 100, resizeMode: "stretch", borderRadius: 10 }} source={require('../assets/images/10.png')} />


                </View>
                <View style={{ alignItems: "center", justifyContent: "center" }}>
                  <Text style={{ fontWeight: "bold", fontSize: 12, paddingTop: 6, color: "#044184" }}>
                  Private Accident
                        </Text>
                </View>

              </TouchableOpacity>
            </View>



            </View>
          </View>











      </View>


    );
  }

}
const styles = StyleSheet.create({

  popbox: {
    width: '30%', alignContent: "center", alignItems: "center", backgroundColor: "#fff", borderRadius: 10, margin: 5,
    shadowColor: "grey",
    shadowOffset: {
      width: 1,
      height: 1,
    },
    shadowOpacity: 2.60,
    shadowRadius: 2.62,
    elevation: 6,
  },
  popbox1: {
    width: '46%', alignContent: "center", alignItems: "center", backgroundColor: "#fff", margin: 8, borderRadius: 10, marginRight: 5,
    shadowColor: "grey",
    shadowOffset: {
      width: 1,
      height: 1,
    },
    shadowOpacity: 2.60,
    shadowRadius: 2.62,
    elevation: 6,
  }
});