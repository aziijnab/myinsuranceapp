import React, { Component } from 'react';
import {
  View, Text, TouchableOpacity, Image, StyleSheet, Picker, Dimensions, ImageBackground, TextInput, Alert, AsyncStorage
} from 'react-native';
import { URL } from '../constants/API'
console.disableYellowBox = true
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { MaterialIcons, MaterialCommunityIcons } from '@expo/vector-icons';
const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
import Header from './Header'
import i18n from 'i18n-js'
import translations from './translations'
export default class Otp extends Component {
  // const [started, setStarted] = useState(false)
  constructor(props) {
    super(props);
    this.state = {
      started: false,
      otp: "",
      new_pass: "",
      lang_done:false
    }
    this.data = this.props.route.params;
    console.log(this.data)
  }
  async componentDidMount() {
    AsyncStorage.getItem("userID").then(user_data => {
      const val = JSON.parse(user_data);
      if (val) {
        this.setState({full_name:val.user.f_name})
    

       console.log(val)
      }

   

    });
    AsyncStorage.getItem("lang").then(lang => {
      const language = JSON.parse(lang);
      console.log("--a--")
      console.log(language)
      console.log(language)
      console.log("--a--")
      if (language) {  
        console.log("----")
        console.log(language)
        console.log(language)
        console.log("----")
        i18n.translations = translations;
        i18n.locale = language;
        i18n.fallbacks = true;
      
      }
      this.setState({lang_done : true})
    });
  }
  submit = () => {
    let otp = this.state.otp
    if (otp == "") {
      Alert.alert(
        "Sorry",
        "Enter required field",
        [
          {
            text: "Cancel",
            style: "cancel"
          },
          { text: "OK" }
        ],
        { cancelable: false }
      );
    }



    if (otp != "") {

      console.log(otp)
      console.log(this.data.email)

      fetch(URL + "check-passcode", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          "email": this.data.email,
          "passcode": this.state.otp,
          "new_pass": this.state.new_pass,
        })
      })
        .then(res => res.json())
        .then(async response => {
          console.log(response);
          if (response.response == "success") {

            Alert.alert(
              "Success",
              response.message,
              [
                {
                  text: "Cancel",
                  style: "cancel"
                },
                { text: "OK" }
              ],
              { cancelable: false }
            );
            this.props.navigation.navigate("login");

          }
          else {
            Alert.alert(
              "Sorry",
              response.message,
              [
                {
                  text: "Cancel",
                  style: "cancel"
                },
                { text: "OK" }
              ],
              { cancelable: false }
            );
          }


        })
        .catch(error => {
          alert("Please check internet connection");
        });

    }

  }








  render() {
    if(this.state.lang_done){
    return (

      <View style={{ width: screenWidth, height: screenHeight, alignItems: "center", backgroundColor: "#fbfbfb" }}>
        <Header title={"Otp"} back={true} backtext={false} navigation={this.props.navigation} />
        <View style={{ width: "100%", marginTop: 25, paddingVertical: 5, alignContent: "center", alignItems: "center" }}>
          <View style={{ width: "90%", borderBottomWidth: 1, borderColor: "#044184", paddingTop: 8, paddingVertical: 5, flexDirection: "row" }}>
            <View style={{ width: "90%" }}>
              <Text style={{ color: '#044184', fontSize: 16, paddingHorizontal: 9, fontWeight: "bold" }}>{this.data.email}</Text>
            </View>

          </View>
        </View>

        <View style={{ width: "100%", marginTop: 25, paddingVertical: 5, alignContent: "center", alignItems: "center" }}>
          <View style={{ width: "90%", borderBottomWidth: 1, borderBottomColor: "#044184", paddingBottom: 5, flexDirection: "row" }}>
            <View style={{ width: "90%" }}>
              <TextInput
                style={{ color: '#044184', fontSize: 16, paddingLeft: 15 }}
                secureTextEntry={false}
                keyboardType="default"
                placeholder={i18n.t('Passcode')}
                placeholderTextColor="#044184"
                onChangeText={(otp) => this.setState({ otp })}
                value={this.state.otp}
              />
            </View>
            <View style={{ width: "10%" }}>
              <Image style={{ width: 20, height: 20, resizeMode: "cover" }} source={require('../assets/check_passcode.png')} />
            </View>
          </View>
        </View>

        <View style={{ width: "100%", marginTop: 25, paddingVertical: 5, alignContent: "center", alignItems: "center" }}>
          <View style={{ width: "90%", borderBottomWidth: 1, borderBottomColor: "#044184", paddingBottom: 5, flexDirection: "row" }}>
            <View style={{ width: "90%" }}>
              <TextInput
                style={{ color: '#044184', fontSize: 16, paddingLeft: 15 }}
                secureTextEntry={false}
                keyboardType="default"
                placeholder={i18n.t('new_Passcode')}
                placeholderTextColor="#044184"
                onChangeText={(new_pass) => this.setState({ new_pass })}
                value={this.state.new_pass}
              />
            </View>
            <View style={{ width: "10%" }}>
              <Image style={{ width: 20, height: 20, resizeMode: "cover" }} source={require('../assets/check_passcode.png')} />
            </View>
          </View>
        </View>




        <View style={{ width: "100%", position: "absolute", alignItems: "center", bottom: 15 }} >


          <View style={{ width: "90%", justifyContent: "center", alignItems: "center", alignContent: "center" }}>
            <TouchableOpacity style={styles.SignUp_button} onPress={() => this.submit()}>

              <Text style={{ color: '#fff', alignItems: "center", marginTop: 2, fontSize: 18, fontWeight: "bold" }}>{i18n.t('Enter')}</Text>

            </TouchableOpacity>
          </View>






        </View>


      </View>

);
}
else{
  return(
    <View>
      <Text>Wait</Text>
    </View>
  )
}
    
  }

}
const styles = StyleSheet.create({
  pic: {


    height: 250,
    width: "100%",
    borderRadius: 50



  },
  login_button: {
    width: "100%",
    marginLeft: 10,
    marginTop: 10,
    paddingVertical: 8,
    alignItems: "center",
    alignContent: "center",
    backgroundColor: "#808080",
    borderRadius: 10
  },
  SignUp_button: {

    width: "90%",


    paddingVertical: 10,
    alignItems: "center",
    alignContent: "center",
    backgroundColor: "#E7BF63",
    borderRadius: 10,
    borderWidth: 1,
    borderColor: "#E7BF63"
  },
});