import React, { Component } from 'react';
import {
  View, Text, TouchableOpacity, Image, StyleSheet, Dimensions, ImageBackground, TextInput, Alert, AsyncStorage, Button, Modal, ScrollView, Platform, ActivityIndicator, RefreshControl
} from 'react-native';
import * as Permissions from "expo-permissions";
import { URL, IMAGE_URL } from '../constants/API'
import * as ImagePicker from 'expo-image-picker';
import * as DocumentPicker from 'expo-document-picker';
console.disableYellowBox = true
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { MaterialIcons, MaterialCommunityIcons, Ionicons } from '@expo/vector-icons';
const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
import Header from './Header'
import i18n from 'i18n-js'
import translations from './translations'
import { Camera } from 'expo-camera';
export default class UploadPolicy extends Component {
  constructor(props) {
    super(props);
    this.data = this.props.route.params
    this.state = {
      started: false,
      get_data: [],
      user_id: "",
      file: "",
      modalVisible: false,
      user_image1: "",
      doct_name: "",
      extension: "",
      doct_type: "",
      doct_img: [],
      doct_data: "",
      base64: true,
      save_loading: true,



    }

  }
  async componentDidMount() {
    this.getPermissionAsync()
    AsyncStorage.getItem("userID").then(user_data => {
      const val = JSON.parse(user_data);
      if (val) {
        this.setState({ user_id: val.user.uuid, file: val.file })
        console.log(">>>>>>>>>>>>>>>>>>user_id>>>>>>>>>>")
        console.log(user_id)
        console.log(val)
      }

      this.get_policyData()

    });
    AsyncStorage.getItem("user_image").then(image => {
      const val_image = JSON.parse(image);
      if (val_image) {
        //console.log("val========================================================");
        //console.log(val_image);
        //console.log("val========================================================");
        this.setState({
          user_image1: val_image
        });
        // this.state.user_avatar = val_image;

      }

    });


  }
  _hideModal() {
    this.setState({
      visibleModal: false,
    })
  }
  get_policyData = () => {
    console.log(">>>>>>this.data.companies_data.user_id,>>>>>>>>>")
    console.log(this.state.user_id)
    console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
    console.log("this.data.companies_data.uuid")
    console.log(this.data.companies_data.uuid)
    this.setState({ save_loading: true })

    fetch(URL + "get-user-policy", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },

      body: JSON.stringify(
        {
          "user_id": this.state.user_id,
          "company_id": this.data.companies_data.uuid
        }
      )

    })
      .then(res => res.json())
      .then(async response => {
        console.log("get_policyData");
        console.log(response);
        // this.setModalVisible(false)
        if (response.response == "success") {

          this.setState({ get_data: response.data, extension: response.data.extension })
          console.log("get_data")
          console.log(this.state.get_data)

        }

        else {
          Alert.alert(
            "Sorry",
            response.message,
            [
              {
                text: "Cancel",
                style: "cancel"
              },
              { text: "OK" }
            ],
            { cancelable: false }
          );
        }
        this.setState({ save_loading: false })

      })
      .catch(error => {
        alert("Please check internet connection");
      });


  }
  setModalVisible = () => {

    this.setState({ modalVisible: !this.state.modalVisible });

  }
  getPermissionAsync = async () => {
    if (Platform.OS !== 'web') {

      const { status } = await Camera.requestPermissionsAsync();
      if (status !== 'granted') {
        alert('Sorry, we need camera roll permissions to make this work!');
      }
    }
  };

  _pickDocument = async () => {
    let result = await DocumentPicker.getDocumentAsync({
      type: "application/pdf"
    });
    this.setState({ doct_data: result })
    this.setModalVisible(false)
    this.setState({ save_loading: true })
    let file = result
    let file_type = "pdf"
    console.log("resultresult")
    console.log(result)
    const formData = new FormData();
    formData.append('file', {
      uri: result.uri,
      name: result.name,
      type: "application/pdf",
    });
    formData.append('user_id', this.state.user_id);
    formData.append('company_id', this.data.companies_data.uuid);
    // formData.append('fileType', 'application/pdf');

    fetch('http://myinsuranceapp.ch/api/submit-user-policy', {
      method: 'POST',
      body: formData
    })
      .then((response) => response.json())
      .then((responseJson) => {
        console.log("pick pdf Document");
        console.log(responseJson);
        this.setState({ save_loading: true })
        if (responseJson.response == "success") {

          // this.setState({ modal:responseJson.data})
          // console.log("get_data")
          // console.log(this.state.get_data)
          this.get_policyData();

        }

      })
      .catch((error) => {
        console.log(error);
      });



    console.log(result);
  }
  pickDocument_word = async () => {
    let result = await DocumentPicker.getDocumentAsync({
      type: "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
    });
    this.setState({ doct_data: result })
    this.setModalVisible(false)
    this.setState({ save_loading: true })
    let file = result
    let file_type = "doc"
    const formData = new FormData();
    formData.append('file', {
      uri: result.uri,
      name: result.name,
      type: "application/doc",
    });
    formData.append('Content-Type', 'application/doc');
    formData.append('user_id', this.state.user_id);
    formData.append('company_id', this.data.companies_data.uuid);
    // formData.append('fileType', 'application/doc');

    fetch('http://myinsuranceapp.ch/api/submit-user-policy', {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data',
      },
      body: formData
    })
      .then((response) => response.json())
      .then((responseJson) => {
        console.log("pick Word Dpcument");
        console.log(responseJson);
        if (responseJson.response == "success") {

          // this.setState({ modal:responseJson.data})
          // console.log("get_data")
          // console.log(this.state.get_data)
          this.get_policyData();
          // this.setModalVisible(true)

        }
      })

      .catch((error) => {
        console.log(error);
      });



    console.log(result);
  }

  _pickImage = async () => {

    let result = await ImagePicker.launchCameraAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 4],
      quality: 1,
      base64: true
    });

    this.state.doct_img.push(result.base64)
    this.setState({ doct_img: this.state.doct_img })
    console.log("doct_img????????????")
    console.log(this.state.doct_img)
    this.setModalVisible(false)
    this.setState({ save_loading: true })
    let filename = result.uri
    let exp = filename.split(".")
    let extension = exp[exp.length - 1]
    // console.log("extension")
    // console.log(result.base64)
    // console.log(result)


    fetch('http://myinsuranceapp.ch/api/submit-user-policy', {
      method: 'POST',
      headers: {
        "Content-Type": "application/json",
      },

      body: JSON.stringify({
        "user_id": this.state.user_id,
        "company_id": this.data.companies_data.uuid,
        "file": this.state.doct_img,
        "fileType": extension,


      })
    })
      .then((response) => response.json())
      .then((responseJson) => {
        console.log("pick Image");
        console.log(responseJson);
        if (responseJson.response == "success") {

          // this.setModalVisible(false)

          // this.setState({ modal:responseJson.data})
          // console.log("get_data")
          // console.log(this.state.get_data)
          this.setState({ doct_img: [] })
          this.get_policyData();


        }
      })
      .catch((error) => {
        console.log(error);
      });

    // this.props.navigation.replace('uploadPolicy')

    console.log(result.base64);
  }

  render() {
    const { modalVisible } = this.state;
    return (

      <View style={{ width: screenWidth, height: screenHeight, alignItems: "center" }}>
        <Header title={i18n.t('UploadPolicy')} back={true} done={false} navigation={this.props.navigation} />

        <View style={{ width: "100%", alignItems: "center", marginTop: 30 }}>
          {this.state.save_loading ? (<View style={{ width: "50%", height: "50%" }}>
            <ActivityIndicator color={"#E7BF63"} size={"small"} />
          </View>
          ) : (

            <TouchableOpacity style={{ width: "50%", height: "50%" }} >

              {this.state.get_data ?

                this.state.get_data.extension == "jpg" || this.state.get_data.extension == "png" ? this.state.get_data.policy_data.map((item, index) => {
                  return (

                    <TouchableOpacity key={index} style={{ width: "100%", alignItems: "center", marginTop: 10 }} onPress={() => this.props.navigation.push("DisplayFiles", { policy_data: item.file, other_data: this.state.get_data })}>
                      <Image style={{ width: "100%", height: 100, resizeMode: "cover" }} source={require('../assets/images/images.png')} />
                      <View style={{ width: "90%", alignItems: "center" }}>
                        <Text style={{ color: "#007AFF" }}>
                          {i18n.t('ViewPolicy')}
                        </Text>
                      </View>
                    </TouchableOpacity>

                  )
                })
                  :
                  this.state.get_data.extension == "pdf" ?
                    this.state.get_data.policy_data.map((item, index) => {
                      return (
                        <TouchableOpacity key={index} style={{ width: "100%", alignItems: "center", marginTop: 10 }} onPress={() => this.props.navigation.push("DisplayFiles", { policy_data: item.file, other_data: this.state.get_data })}>
                          <Image style={{ width: "100%", height: 100, resizeMode: "cover" }} source={require('../assets/images/unnamed.png')} />
                          <View style={{ width: "90%", alignItems: "center" }}>
                            <Text style={{ color: "#007AFF" }}>
                              {i18n.t('ViewPolicy')}
                            </Text>
                          </View>
                        </TouchableOpacity>
                      )
                    })
                    : this.state.get_data.extension == "docx" ? this.state.get_data.policy_data.map((item, index) => {
                      return (
                        <TouchableOpacity style={{ width: "100%", alignItems: "center", marginTop: 10 }}
                          // onPress={() => {
                          // Alert.alert(
                          //             "Downloading",
                          //             "Policy is being downloaded.",
                          //             [
                          //             { text: "OK" }
                          //             ],
                          //             { cancelable: false }
                          //           );

                          //                     }}
                          onPress={() => this.props.navigation.push("DisplayFiles", { policy_data: item.file, other_data: this.state.get_data })}
                        >
                          <Image style={{ width: "100%", height: 100, resizeMode: "cover" }} source={require('../assets/images/word.png')} />
                          <View style={{ width: "90%", alignItems: "center" }}>
                            <Text style={{ color: "#007AFF" }}>
                              {i18n.t('ViewPolicy')}
                            </Text>
                          </View>
                        </TouchableOpacity>
                      )
                    }) : null




                : null}

            </TouchableOpacity>
          )}

        </View>

        <View style={{ width: "100%", alignItems: 'center', justifyContent: 'center', marginTop: 30 }}>
          <View style={{ width: "90%", alignItems: "center" }}>
            <Text style={{ fontWeight: "bold", fontSize: 15, color: "#9ca2aa", textAlign: "center" }}>
              {i18n.t('UploadDocument')}
            </Text>
          </View>

          <View style={{ width: "90%", justifyContent: "center", alignItems: "center", alignContent: "center", paddingTop: 10 }}>
            <TouchableOpacity style={styles.SignUp_button} onPress={() => this.setModalVisible(true)} >

              <Text style={{ color: '#fff', alignItems: "center", marginTop: 2, fontSize: 18, fontWeight: "bold" }}>{i18n.t('Enter')}</Text>

            </TouchableOpacity>

          </View>

        </View>


        <Modal
          animationType="slide"
          transparent={true}
          swipeDirection="left"
          onSwipe={this.closeModal}
          onBackdropPress={this.closeModal}
          visible={this.state.modalVisible}
        // onRequestClose={() => {
        //   Alert.alert("Modal has been closed.");
        // }}
        >

          <View style={styles.centeredView}>

            <View style={styles.modalView}>
              <View style={{ width: "100%" }}>
                <TouchableOpacity style={{ width: "15%", alignContent: "center", alignItems: "center", paddingLeft: 5 }} onPress={() => this.setModalVisible()}
                >
                  <Ionicons name="ios-arrow-back" size={25} color="#FE000C" />
                </TouchableOpacity>
              </View>
              <View style={{ width: "100%", alignItems: "center", alignContent: "center", }}>
                <View style={{ width: "80%", flexDirection: "row", justifyContent: "space-between" }}>
                  <TouchableOpacity style={{ width: "30%", marginTop: 20, alignItems: "center" }}

                    onPress={

                      () =>

                        this._pickDocument()
                    }
                  >
                    <Image style={{ width: 60, height: 60, resizeMode: "contain" }} source={require('../assets/images/unnamed.png')} />

                  </TouchableOpacity>
                  <TouchableOpacity style={{ width: "30%", marginTop: 20, alignItems: "center" }}
                    onPress={() =>
                      this._pickImage()}
                  >
                    <Image style={{ width: 50, height: 60, resizeMode: "contain" }} source={require('../assets/images/camera.png')} />
                  </TouchableOpacity>
                  <TouchableOpacity style={{ width: "30%", marginTop: 20, alignItems: "center" }}
                    onPress={() =>
                      this.pickDocument_word()}
                  >
                    <Image style={{ width: 50, height: 60, resizeMode: "contain" }} source={require('../assets/images/word.png')} />
                  </TouchableOpacity>
                </View>


              </View>


              <ScrollView showsVerticalScrollIndicator={false}>

              </ScrollView>


              {/* <View style={{ width: "100%", alignItems: "center", alignContent: "center",marginBottom:10 }} onPress={() => this.props.navigation.navigate('itemCart')}>
                <View style={{ width: "90%" }}>
                  <TouchableOpacity style={styles.filter}  >
                    <Text
                      style={{
                        color: "white",
                        alignItems: "center",
                        marginTop: 2,
                        fontSize: 16,
                        fontWeight: "bold",
                      }}
                    >
                      Apply Filters
            </Text>
                  </TouchableOpacity>
                </View>

              </View> */}

            </View>
          </View>
        </Modal>

      </View>

    );
  }

}
const styles = StyleSheet.create({
  pic: {
    height: 250,
    width: "100%",
    borderRadius: 50
  },
  SignUp_button: {
    width: 100,
    paddingVertical: 10,
    alignItems: "center",
    alignContent: "center",
    backgroundColor: "#E7BF63",
    borderRadius: 10,
    borderWidth: 1,
    borderColor: "#E7BF63"
  },
  SignUp_button1: {

    width: 50,
    backgroundColor: "#E7BF63",
    borderRadius: 10,
    borderWidth: 1,
    borderColor: "#E7BF63",

    paddingVertical: 10,
    alignItems: "center",
    alignContent: "center",

  },
  container: {
    flex: 1,
  },
  camera: {
    flex: 1,
  },
  buttonContainer: {
    flex: 1,
    backgroundColor: 'transparent',
    flexDirection: 'row',
    margin: 20,
  },
  button: {
    flex: 0.1,
    alignSelf: 'flex-end',
    alignItems: 'center',
  },
  text: {
    fontSize: 18,
    color: 'white',
  },
  box: {

    width: 150,
    height: 150,
    borderRadius: 80
  },
  profile: {
    width: "50%",
    height: 35,
    resizeMode: "cover",
    borderRadius: 100
  },
  centeredView: {
    width: screenWidth,
    height: screenHeight,
    justifyContent: "flex-end",
  },
  modalView: {
    width: "100%",
    height: "30%",
    backgroundColor: "white",
    borderRadius: 20,
    paddingTop: 40,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
});













    // <View style={{ width: "100%", marginVertical: 20, alignContent: "center", alignItems: "center", marginTop: 15 }}>

    // <View style={styles.box} >

      //  <Image style={{width:"100%",height:"100%",resizeMode:"cover",  borderRadius: 80}} source={{ uri: "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxITEhUSEhIVFRUWFxUYFRcWFxgWGBcVFhcXFhoXFhcdHSggGB0lHRUVITEhJSkrLi4uFx8zODMsNygtLisBCgoKDg0OGhAQGy0mICUtLS0vLTUtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAKkBKgMBIgACEQEDEQH/xAAbAAADAQEBAQEAAAAAAAAAAAACAwQFAQYAB//EADsQAAEDAgQDBgUCBQQCAwAAAAEAAhEDIQQxQVESYXEFE4GRofAiMrHB0QZCFFJi4fEjcoKikuKywtL/xAAaAQEAAwEBAQAAAAAAAAAAAAADAQIEBQAG/8QAKxEAAwACAQMCBAYDAAAAAAAAAAECAxEhBBIxQVETImHwBRSBkdHxMrHh/9oADAMBAAIRAxEAPwCUIwhCML7xnzLZ0IwFwBGFVhUzoCY0IQEwBG2BTOgJgCEBMAVGwaZ8AjAXwCMBUbBpnwCMBfAIwFRsCqPgEYC+ARgKjYNUcy+3NA6sBAOZ0+v+UnH4gU2l0XybOUna/ienJeYxeLc+m573hoBIgfvLZkbw3wjxXkti4sLyc+huY3tpjCY4dIkiXE7AXK8x2lVqPqS9/D/SM457fVQN7Wh/eFoMSb2k5AWyGVggpVHVB3jmhoJOWvMdEukjq4ek+Fzr9SnDUSROwgSZ4grMA4tkEW3P0SKDHvLTo3M6ALewuDD7HMZOixWe8hGbIpXzCcFRda0g5DbqVrUaZGkeMckTRFg2CI4iCP8AqFRT2yvnmb/RZ22zmZczo4JiZJI2uiptNiGwedpHROdSLWi4HOc/HdTV8QA20y03M2N9Bqq7Myru8FrYM2y8j0S8Q34SAOgmFFU7UD/hBLTofeSRWxpaYLptcqr2TOG9k7cfwjhcDYkggx5jVC/te2cHk3+6y69bn73UrnJO3Z1p6eXy0amGrAuLiSLeKsD7AiRtIWNhXQQcwDdaT6ypZ7JHPAL6wJ0kKarXKT2hXjhjOfRC9QPEaSYouIXweiAXHNOinZoTPqlSD4BJfVIyE80140NknI3UCTogeTKokpr6Qz0Su55/VQ0P3pnoQjAQtCNq+hZkphBGAuBG1UYNMIJgCEBG0KjAphAJjQhaEwBGwKZ1oTAEICY0KjYFM6AjAXAF17w0EnII2BTCScVi+Fp4G8bhADQRdx3OgGZKxsX2nBkmZH+m3InmdgsHtntIsaKbXQ993EGzAdG7E3JOem6t2GjF0dXSQzH15c52IrmBHE1sEgmYYI+FpjOJNr88qvjyRlwMaBABgNBJIbGtQ3JJygk5Q2d5Dg2Mm5TaToOpzPTYCM/E1eMhvES1s3/mJiXHmYEcgPG64O9h6deH/X39/XlbFcZsPDQRoFr4N7nZ5RYaCNgouz8NJGg6XXpey+y+IS3QwAfuiyWW6nLELRZgGtI+ISYFsp5LZoUzAsQ28AZAnnqosNTDYJ1BPgMls4muXNbwZRcDdZn5PneoyN1wfVAGtaJc57o8tuSW6sBmP87Aanmkfxhj4pgWBGfRIq1WEfC4kzmT79FCTAnG/Uqr4w8LtNtTPX7BSYp5cLu+K3ENDt4qatiQAL+nvzUzqgP8208X0up7TTjw65KDwwbgEZDUpGJcTfIxBnXdT1Hg3BMjU31jmhqPfY/N9RymVPaapjnZNUecks1F2sNr/XxUzpV9G2UmONUjIwufxhF/yL+ClqvSS+V5yNONMtZX4nAlUnEt3WXTcuucjckvGmzR7/a/0XBXO/4UgdYKhgAEnPQKjWiO1IoZWBz/AMrpgpH8SeSdTrA/MPFVaI00DwkIeI+wqntjp9P7IVUsqNQJjUATAvoGDTCaEwIGpjVRgUwgExqFoRhGwKYYCYAgamNCozPTCaEwBC0JjQjYFM+LgM1ldv8AaQYzhBu71Ewn9r1C1s6Lw3avaBeYGn2Ua9TT0nTfFpU/BLj8U7vS4zlA/HJS0WGtVEkgakXPQDU6In4ZxHGTA6pVLEcDpaBMQ3kd+qsfQTKU6jzrQ3E1LEARmIGTRHqefVT4XDmVYzDuf8QGdzpfVaGF7PcDeANfibPhe6hspWaYnWzuFpxFp+3VbODYdASSMuc5AaosNhWtjSSAIscsvIE+C2MKA27RG2/iVnqjjdT1HsI7qGy60j5Rd3PpC+r0S0yDpJaDtvv9EGKrimC8kgb69ANTmY/uvP4jF1K1ieCleefj+7opiHXPoBix1fO+B/afbTQQ2eOBcNy4uv4WecZiH/KAwZA5HzN/RC/u2/KP+R926KWri+a0zK9EdPHilLUr9/4GVMI9131T0ufv9k8PLQGh2UZt2WccQ7/P90o1iNfopct+TR8Onwzap1XH9zfFp/P2RNxjsiLbgzHhYn1WCMUd/oibjoVXiIfTs2xiQflMnUH76hJrVFntxTXZ56HUeOi4/EEayN9fFH8MlYdMKpUSuJE+CJlAG3up0h50G0lMmFxh98kqs4bI2SuWUmoqXvkArKo1vJXU3wjpEVOgnAyqqAPgvqBac7K6mWBUbBu/TR1w+C+yzu8d/MVVXxE2A80vvDuVVI9G0uTeCYEARhd1gUG0JjUtoTGo2BQxqY1LATA1GwKYbUxLa3ZNaUbM9MNpRNcdtUIamsEI2BTMrtqqeAWzkG+n5uvAY1l5iF+g9r4cQLZf2XjMbhZvH1UHV/D7lIyC4kAafdLFAkyArqeH5THl5q7D4bSPsPyV51o6dZVPglw9En4b+H0WzgsNwgRlvOXI7LlDCDig5ZdPDXNX18IeB0CXBpyuXMi8f1ajmAqOjnZs+3omwWOJcabz8THA/wC5rhw25SZnotKrixSkkwLGc/D7LznbOHd3bMVRNsnROh15ghS4nFCsBDiW5ls3Gk+seSRYlXJR9NOTVLx4f0aKMXinVncZs0WA0HvU+wvFV5EtMwLWgeCk7+bA5eHgpu8k8k/b7GycOv0CcS659/hVYTsqrVEsaYORi1jGa9D+meyqJirVOfyN+pduvRP7VpNECNRnoqO34SMXUfiLiuzFO2v2PJYT9IPM8ZMqw/pKmNVou/UICkrfqMEmAibyszfH6237Gbif06ybBZ9f9PgZLUf20FNW7X2Ur4hsxX1KMDE9lObkpHMeFuYjH8QWVWqe/wApp7n5OniyW18xM2sR0TRiEl51Sg+FLnZq7UzRpV0GJrWhRd/CRVrSqfD5PTi52OoVbrcwxtl5rDwNKSvR4SihypbKdQ0h1DPJVuZI57fhDTEe9Uyi0k/FM5j/ACszMDr1BFINzEnZd7w7N8gn1GDULvdt5eajZHcaTCi4+R8ktrU1rV22HQTTyKMePovgEYVGBR1juR9Pymh3I+n5QNTWhGwKYTD7smNXGouH39kbM9MYGo45n34JYJ29f7IxxbDxP9kbM9E2LYIJOxzdEc/lsvO4sZgtN8jb6Zleu7s6xGgk57m1ys7GURew5zpznUZdFRsXBm7Xo8q7D3+UxqLW5i6ppMuOEcVo09bqpwE8PhkT4EAZIaLm0wWtmJyDXwOpiVR7N9ZG0VU6FrzOwAn1FvFNZAbZ2dmgRnsTElTCqSMjnkA8f/VcLeJweyOIEEsdaeHQE5GNVCXuZaTfklx5dQcaog0nH/VbHwcRt3gj5djobE6leYxNMUncTBNNxMNnhcy9oOkgg7XXtxiKdam4McJ+QscQHtJPD8QPXNecxvZb6LiGND6ecZxJIyE2zyB8Fqxvg19JmSbmuH49t/8AfYwarST8Lr7OHC7x0PUI6VYEgOaf+Ik+SJ1Mgktu2flIBg8pkJbTJ0HS3h7CZHV8oeca64BMeXmETMSd4VHZ+B4rvmBvP0Q4nCAGGr3cvAHfj7u0SaxKNpVeA7EfUj9oM3P9ILjA1sCuUOzzJ+KABIJBuCAQY6GVDclHlx8pPwRVKSV3BK1f4Am065p1DCRLVHce/MJLyefqUnBTuB2XpMRgjsp3YDkvd40dTOjzzkioFtYnB/j8KKtg8jurqka8eWWZoC+ZTkp7aPxRutDDYS/X391FPQ9ZVI/s3CiJWsxsJWFoERC0e6WG3ycrLl3QkBMp5ohTTWUkTCdIfTAd+Fw4QbFHTYj4TufNU0H3a8DQjagajC7jEoYw+7pjZS2lNajZno65sixg9UdOpv7/ACFwJjVRgUEHjdMaRyQt95pjT7n6I2Z6CFQDX0TGOBtmUoVHf0+fkmOa47AjI7I2BSCZh4cXZzoch0spMdJMNtGZFyOQOnMfdWNDjaw01n/KRipALA2bZXPCMuK19+c+YNlJfzGK9sHhbA5QYA3N5/Pql1qLgRBJ5kMAA5WVX8C3Jry4DM3BvuAPcKfG0uERJ8ZuNiIt1R750bppNpL/AETPv+6fBkc/2qOriuBwdxGBYmBMeAEhXBsgZttZsgnq6J8kivRbwmwI5m534UktDy53po5VwNOrwOa8h8/OyzhDScxncBS4s4pk8Tu8G5kEiMj/AJUtT/TcHMtf5Q4nQ6RZPrYtxaTx2O5nQcloQqik16r6rwSupPcOJ1INO+u3zIsLhrzlGpKOpV+ES6U/CuLmwBbmvbYlVSkbSI4bKzsjsU1pdMAZ7kkWHTInks4OgQvbfp1obh2k2niJ8yPsFSqcrg5vW5qw49z5b0Z+KwgpHv3u4A0/DTYGmXOtAtrf12XjMZi4qEwAJ+USeG9hOsZLX/UmIJql0m4McjoOWq8o9pkTrEeKXFHqzX+H4Pl7rfLX3/Z7vs6hxNFp/uqX9n8LhIzWl2Bhw2izoEXaQ+JqzU+dHGrqG8rleOTMrYQKV+FELTqugLMqy5ROx8VU/Uxu1MKIPDfXyUlPCywzoT+VsVmAAydClPb8L4tc/QKzppHUx5WpSPMY/CcFSmdz9x+Vq4ajcWFiZnxSO1Wy6iOf3atRlOD4kD1P2U3Tco15MjcTv6lVPDnhyAHh6plSlGuXpyhNo3F7nSck6BkW2PSJ8FlbZz3b2RsEnKNpGaoZR6J2HYzTPzPSdEwAjUnko2eeT2Fd3C7wlWCkPf3XO4PuPyoKLIjOamNQApgXcZ0KDHv3CYD79hLCNo5BGwKDDQcwj4RoAULRyTQqMz0fBn9I8x+EQp/0D34LlR4aJP0nkmOtk2ekfdUYFH3czoG9ASV8XMyHASDDpPDHhe+SIE/yH/p+UymSP2H/AKeeaNg0zks1FLkOIfhF3rY4ZptJuINjGd7Xj08U9rz/ACn0/KPvT/KfNv8A+kbM9P72ZVZrS48L2hxsOF1yRob/AFU2Lw3GAJdxASYcSAfei08ZxEEGmeHY8HD5cUu8Vh16rhIDXnYktkDaJ9f8kNPZpw7rwyOlSdJBkCdc3cwdr5r6pwuAEcQAIkfSdUNSsbzYHOSDMb7Bd70QDJBOViPKyXk3NPyZuJwTJBDSLi3W33QfwTXzEz4xtK067iQRJvqdNs7lJ7xxAiwIMiwPnG6WaYqyXryS0MHAufNUUn8IsJ+iY2A6Dk4Wnf39V89mgCvvbKVXd5JHFe27GIdhm8s//KfuF4d1ivYdgHhHAflqDiYeeo97Kl+DJ+JTvGvo9n2I7JDnmZIe0tkZgm4d72WFR/TBDwXFxGgOl4gcl7ceq7CoslI5uPr8uNNJgYenwtA2CkxBl+WVlcUt0NBKMyw+dk+JoAt6LDxkCwVHaHahNgsl9TiV52jq9NhtLdEuJcTbcget/QFd4vgH9Tp8CS76IMQPPIeOZ8BKFzxxRsLe+iu1wdSVwjPxx48RTaNIPqT9gtcftHU+n/ssfs//AFKz6mgsPG1vAeq26LSSY0tvzP2UZONL2Q2bjU+yKaTN/T3ZU0acm2vgQgw9hII8ffqrKjGyDmMpG/PZZaZguudCnyTBuJzy9VQGOiImcnadQEqWzFzuSbeEKsPgDnkBtzKgOm+BbWkEfS5j8qjh5nzSKY/cZgTb7p/CeXkkU7K0YYRhAEbV2GdmhoKMBA1GCqMChjUxqUAETWznbYAx5wjYFIoYiazmfNKbTHP/AMj+UxrBz8z+VRmehzW8z5omjmUtrBz8z+U1jevmSjZnoNvUx4ouEnInx15cuq+aUQfcBGwK2K7hlyeK5mC2Y5D4clLiu7GUHP4SzPp8Nj9fUW42vwtymV57FOMz11Q2tiYMbvls7jMHScLNjIj/AE3C4uJEb+Sz6jDN7FoyizZ1G/3W1hCXN10i6lxlI8TT/u18lWb09GvHkafazMqNIgcRJtJ4RtmhZTIdAOckWGeoy8fNVFuQI3J6beqjxzXlst0uenJNL3waJe+Cg0Z1uI++XXX+yqFMFvvySsA/vGhzY45E3Pw9RsVbULeEusHAHiByJ3ncaHLRXTfgzZKaejzGOsV6n9LVRUocBMOaZadRzC8tiagqG329haPZbXUjy1JyCVzudGjqsffh7fXyewwzasnj4eo18FUFk0O1Gt+EyeZM+R1RVcdIsQgcs4NYLb5RbWxTW2m6ye0MXISXul05qWuVKk14enmXskquJSJjqcvyeQVNWANhuVE14MmZ66xuNANlZHUjlHzifm5QJz/yVK9pd8IzdMkbfuP0A8FRWdADnEwZtAnOPXbmjwFExJFzFthoOgufNQ60tjp9q2d7MwAY0NBm8nmTlI5BaLMNERzm5kmV3DYWIJg+OvLmtCIvcnymemSz3bbMuXM3QunRAz6c76HbVODRe0ixsYj89UTG2vYcxMjKeso6dMXic7g58pEI97MzoS9wJIcDPKJkbqinSO8DMNzNtyvg0NExfcayvmOi5uLZZ5JUj29rgN7m2B8vyhNQ7s8117QfL5eqAFv8vomSJSWjIBRtSmhMaumztUNCNqW1MCowaQYCa0pQRhGzPSHNTAUgAbJjUbApDweSMOS2lGEbM9Ia1NCQ1vXzRtbzPmqNAUg3U5ESlP7MpkyZ6SnMHXzRumLGCipBd1T4YirSa1toACyarXEk5AWAyk/bRPxdOoRxG4BuNugS2VAWni0vbOOvh6LNrRoxz2re9kb6BPF0Aj6z5pNFrssheN8votilT4jIFzeNHDQ8yqK2FaB3k2g8Q3jpkfwrzW+C76jXDPM4zCvw/wDr07fzNyHQXuOWn0KlR/iQA0XgcQJg3zEaiddOWS0KjXd4CSOG3DIyjobOifi+4EUN7LBh1M8DwLRYGP8A4lbZpJc+fcvWfUpvz7/yQs/T3dmWFxdoBYgcoN/PwTaDzB70G0nf0Jkb2Oq0Rj3M+Cu3T5oj+x8CnNfRqCxBsAJFxGkZrzqvUyVmyP8Az5+pj90wDO4MbG28iI8V0tbnxNy0I/KsxeEZEF0DhJmY8A0g8tViV8KAOLit4WnTNT3Sx8T7/UOrimiYI8/xKgfjthxHfID8rooyYuY52+iZ3bGZkA+R8Lk+Sq6lG2YmTPxLajyC6Y6ZdB9/VMp02sOd5sBJJP335eqtrF7h8DY5uET/AMcz4wlMoNaCZDnaHM8xGg6KrvaGV8aBoYfiMvFx8rQRafq73zNdOkN75Rf3OaDD0eI2bJ1OQb0/C1G4RszFx7k7oboLJk0we5IEXsBPXbmqadE55C3MnqfwnUW7Dx+0ao+NxFpDSYJOY0sEO2zFVtnzacjIx7OaZaYJE+njulsbwTm6+f5VHrIHKEkyGxZEgiDsbwOoCW6kRlbc7/hUPE2KAvsnmS0tiHmLZc19xnZDVF80PEN0iQ6XBjhG0pYRhdFnbpDWlGEoFMaVRg0hoTGlJamBGwKQ0I2pbSjaVRgUhzSmNKS0o2lG0Z6Q5pTAUkFG0qjQFIcCuucdEAKTXrxEI6QXZtjMS2dSJ1nXRZtGkWk2MzHJXtfBM5WnqidSMl2YMEDnkszRea7Vol4uAENzDpA2EX8ChL3vIJNpnh0mDBjlmj7u5BNySibTgqZWiXrz6iywzJyAgK7D4YxJMDNIglfPa7hzsMwkTYV7fBTh38XwkAi+xBHRQ9pdmsLSR8O/CQZ6g/QIqR2snVqLqnUDS3mkW09ook4vaekeYq978raptl8wAPSTuhq4Rro46p4gBMiL6xbKVu0uyyDBJJvBtH2K6cC4mGm2RsZbrmTkpdtm9dTK8P7/AGMZuC4Ww7jIzkOdF9wCiwuBAy4YM5WJC16mBeW8HBcWlpABHOSu0ezQ08TtNAYACN+CPzPD5M59Aizb6kbcgljCaxactepP4VGLrAkhthsNeaowtPiA3INpzVGmi3e5nbG4bDCIyGhGXkqBSAgG7ttEdKiWtIETpOiY6nMg5GLdEaltmOr2/Ig/FfMR0Fs+I5yEk1GTcl3XKOQt5qx7NjHKBHkkFo/lB6CEkxotLQ6jEWyXH2SmNIu3LUFNJSpHtcgOG3ghKIlA4pUhZQtwCX3fIJjihlXSHkwgjCWEYW9ncYwFGEsIwqMGhoKMFLajCowaQ0FGClhGFRgUhoKMFKajajYFIa0pgKUEbVRg0jtR1lO59xGwTKyQ3PwRUemVoY2XTHjzVmGJAAPgocOrmfMj0FlXoA+hEmZRM9VQuVV5oDu3wwWNtKW6xkJyU9eRVeQe6GYy22VFOwQUEVX5VJWuXoobuhJBt7lLw2SKpp1VfUPXOjonrtyU2NwvEPmNtNFWuFeJmmntHn6WFBJkxbMrTps4QzhAAIv5bqUaq5n7f9v4VmjXlpvyEHeXvJcjXVE5AV7QSPiUt3JEUBVkhJRw5WX0r5uSAK6QqR8SgJROQFIhZQJKCV0oVZDJH//Z" }}  /> 
    //   {this.state.user_image ? (
    //     <Image style={{ width: "100%", height: "100%", resizeMode: "contain", borderRadius: 10 }} source={{ uri:  this.state.user_image }} />
    //   ) : (
    //     <Image style={{ width: "100%", height: "100%", resizeMode: "contain", borderRadius: 10 }} source={{ uri: this.state.user_image1 }} />
    //   )}
    //   </View>
    //   </View>