import React, { Component } from 'react';
import {
  View, Text, TouchableOpacity, Image, StyleSheet, Picker, Dimensions, ImageBackground, TextInput, Alert, AsyncStorage
} from 'react-native';
import { Container, Content, ListItem, CheckBox, Body } from 'native-base';
import { MaterialIcons, Ionicons, EvilIcons, MaterialCommunityIcons, Octicons, Feather, Entypo, AntDesign, FontAwesome5, Zocial } from '@expo/vector-icons';
import { ScrollView } from 'react-native-gesture-handler';
const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
import Header from './Header'
import { URL } from '../constants/API'
console.disableYellowBox = true
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview'
import ToggleSwitch from 'toggle-switch-react-native'


export default class Approval extends Component {
  // const [started, setStarted] = useState(false)
  constructor(props) {
    super(props);
    this.state = {
      started: false,
      name: "",
      s_name: "",
      email: "",
      name: "",
      password: "",
      is_check: ""
    }

  }
  submit = () => {
    let login_email = this.state.login_email
    let login_password = this.state.login_password

    if (login_email == "" && login_password == "") {
      Alert.alert(
        "Sorry",
        " E-mail and Password is required",
        [
          {
            text: "Cancel",
            style: "cancel"
          },
          { text: "OK" }
        ],
        { cancelable: false }
      );
    }

    if (login_email != "" && login_password == "") {
      Alert.alert(
        "Sorry",
        "Password is required",
        [
          {
            text: "Cancel",
            style: "cancel"
          },
          { text: "OK" }
        ],
        { cancelable: false }
      );
    }
    if (login_email == "" && login_password != "") {
      Alert.alert(
        "Sorry",
        "E-mail is required",
        [
          {
            text: "Cancel",
            style: "cancel"
          },
          { text: "OK" }
        ],
        { cancelable: false }
      );
    }
    if (login_email != "" && login_password != "") {

      console.log(login_email + "---" + login_password)
      fetch(URL + "login", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          "email": login_email,
          "password": login_password
        })
      })
        .then(res => res.json())
        .then(async response => {
          console.log(response);
          if (response.response == "success") {

            AsyncStorage.setItem("userID", JSON.stringify(response));
            AsyncStorage.setItem("setting", JSON.stringify(response.setting));
            Alert.alert(
              "Success",
              response.message,
              [
                {
                  text: "Cancel",
                  style: "cancel"
                },
                { text: "OK" }
              ],
              { cancelable: false }
            );
            this.props.navigation.navigate("workouts");

          }
          else {
            Alert.alert(
              "Sorry",
              response.message,
              [
                {
                  text: "Cancel",
                  style: "cancel"
                },
                { text: "OK" }
              ],
              { cancelable: false }
            );
          }


        })
        .catch(error => {
          console.log(error)
          alert("Please check internet connection");
        });

    }

  }
  register = () => {
    let register_email = this.state.register_email
    let register_password = this.state.register_password

    if (register_email == "" && register_password == "") {
      Alert.alert(
        "Sorry",
        " E-mail and Password is required",
        [
          {
            text: "Cancel",
            style: "cancel"
          },
          { text: "OK" }
        ],
        { cancelable: false }
      );
    }

    if (register_email != "" && register_password == "") {
      Alert.alert(
        "Sorry",
        "Password is required",
        [
          {
            text: "Cancel",
            style: "cancel"
          },
          { text: "OK" }
        ],
        { cancelable: false }
      );
    }
    if (register_email == "" && register_password != "") {
      Alert.alert(
        "Sorry",
        "E-mail is required",
        [
          {
            text: "Cancel",
            style: "cancel"
          },
          { text: "OK" }
        ],
        { cancelable: false }
      );
    }
    if (register_email != "" && register_password != "") {

      console.log(register_email + "---" + register_password)
      fetch(URL + "register", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          "email": register_email,
          "password": register_password
        })
      })
        .then(res => res.json())
        .then(async response => {
          console.log(response);
          if (response.response == "success") {
            AsyncStorage.setItem("userID", JSON.stringify(response));
            Alert.alert(
              "Success",
              response.message,
              [
                {
                  text: "Cancel",
                  style: "cancel"
                },
                { text: "OK" }
              ],
              { cancelable: false }
            );
            this.props.navigation.navigate("login");

          }
          else {
            Alert.alert(
              "Sorry",
              response.message,
              [
                {
                  text: "Cancel",
                  style: "cancel"
                },
                { text: "OK" }
              ],
              { cancelable: false }
            );
          }


        })
        .catch(error => {
          console.log(error)
          alert("Please check internet connection");
        });

    }

  }
  updateFavourite = () => {
    this.setState({
      is_check: !this.state.is_check
    });



  };

  render() {
    return (

      <View style={{ flex:1, alignItems: "center", backgroundColor: "#fbfbfb" }}>

        <Header title={""} back={true} backtext={false} navigation={this.props.navigation} />

        <View style={{ width: "100%", marginTop: 20 }}>
          <Text style={{ fontWeight: "bold", fontSize: 15, color: "#044184", paddingLeft: 20, color: "#E7BF63" }}>
            One last step
          </Text>
        </View>
        <View style={{ width: "100%", marginTop: 10 }}>
          <Text style={{ fontWeight: "bold", fontSize: 20, color: "#044184", paddingLeft: 20 }}>
            Hello Mia,
          </Text>
        </View>
        <View style={{ width: "100%", alignItems: "center", marginTop: 15 }}>
          <View style={{ width: "90%" }}>
            <Text style={{ fontWeight: "bold", fontSize: 15, color: "#696e78" }}>
              You can start now! You can find our legal documents here. From now on you can
              manage everything related to your insurance in your profile.
           </Text>
          </View>
        </View>

        <View style={{ width: "100%", position: "absolute", bottom: 50 }}>
          <View style={{ width: "90%", flexDirection: "row", paddingLeft: 10,paddingBottom:10 }}>

            <CheckBox  onPress={() => this.updateFavourite()} checked={this.state.is_check} />

            <View>
              <Text style={{ paddingLeft: 15, marginTop: 3,color:"#044184" }}>I agree to receive updates and further information</Text>
            </View>

          </View>
          <View style={{ width: "100%", alignItems: "center", }} >
            <View style={{ width: "90%", justifyContent: "center", alignItems: "center", alignContent: "center" }}>
              <TouchableOpacity style={styles.SignUp_button} 
              onPress={() => this.props.navigation.navigate("login")}
              >

                <Text style={{ color: '#fff', alignItems: "center", marginTop: 2, fontSize: 18, fontWeight: "bold" }}>Register</Text>

              </TouchableOpacity>
            </View>
          </View>

        </View>










      </View>


    );
  }

}
const styles = StyleSheet.create({
  pic: {


    height: 250,
    width: "100%",


  },
  login_button: {
    width: "80%",
    marginLeft: 40,
    marginTop: 10,
    paddingVertical: 8,
    alignItems: "center",
    alignContent: "center",
    backgroundColor: "#808080",
    borderRadius: 10
  },
  getstarted_button: {
    width: "90%",
    // top: 80,
    marginHorizontal: 20,
    paddingVertical: 10,
    alignItems: "center",
    backgroundColor: "#fff",
    borderRadius: 10,

    bottom: 0


  },
  SignUp_button: {

    width: "90%",


    paddingVertical: 10,
    alignItems: "center",
    alignContent: "center",
    backgroundColor: "#E7BF63",
    borderRadius: 10,
    borderWidth: 1,
    borderColor: "#E7BF63"
  },
  box1: {

    width: "40%",
    height: 25,
    resizeMode: "cover",
    borderRadius: 80,



  },
});