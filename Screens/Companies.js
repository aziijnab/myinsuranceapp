import React, { Component } from 'react';
import {
  View, Text, TouchableOpacity, Image, StyleSheet, Picker, Dimensions, ImageBackground, TextInput, Alert, AsyncStorage
} from 'react-native';
import { Ionicons, EvilIcons, MaterialIcons } from '@expo/vector-icons';
import { ScrollView } from 'react-native-gesture-handler';
const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
import { URL } from '../constants/API'
console.disableYellowBox = true
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview'
import Footer from './Footer';
import Header from './Header'


export default class Companies extends Component {
  // const [started, setStarted] = useState(false)
  constructor(props) {
    super(props);
    this.state = {
      started: false,
       get_data:[],
       user_id:""

  }
}
  async componentDidMount() {
    AsyncStorage.getItem("userID").then(user_data => {
      const val = JSON.parse(user_data);
      if (val) {
        
       user_id=val.user.uuid
       console.log(">>>>>>>>>>>>>>>>>>user_id>>>>>>>>>>")
       console.log(user_id) 
        console.log(">>>>>>>>>>>>>>>>>>val>>>>>>>>>>")
       console.log(val)
      }

      this.get_companiesData()

    });

  }
  get_companiesData = () => {
    fetch(URL + "get-companies", {
      method: "Get",
      headers: {
        "Content-Type": "application/json",
      },

      // body: JSON.stringify(
      //   {
      //     "user_id": this.state.user_id,
      //   }
      // )
    })
      .then(res => res.json())
      .then(async response => {
        console.log("  get_companiesData");
        console.log(response);
        if (response.response == "success") {
          this.setState({ get_data: response.data })

        }
        else {
          Alert.alert(
            "Sorry",
            response.message,
            [
              {
                text: "Cancel",
                style: "cancel"
              },
              { text: "OK" }
            ],
            { cancelable: false }
          );
        }


      })
      .catch(error => {
        alert("Please check internet connection");
      });


  }

  render() {
    return (

      <View style={{ width: screenWidth, height: screenHeight, alignItems: "center" }}>
        <Header title={"Insurance Companies"} back={true} backtext={false} navigation={this.props.navigation} />
        <ScrollView showsVerticalScrollIndicator={false} style={{ width: "97%" }}>
              <View style={{ width: "100%", paddingTop: 5, paddingHorizontal: 10 }}>
          <View style={{ flexDirection: 'row', flexWrap: 'wrap', justifyContent: "space-between",marginTop:20 }}>
          {this.state.get_data.length > 0 ?
            this.state.get_data.map((item, index) => {
              return (
            <View key={index} style={styles.popbox}>
              <TouchableOpacity style={{ width: "100%", padding: 5 }}
              onPress={() => this.props.navigation.push("mail",{companiesData:item})}
              >
                <View style={{ width: "100%", alignItems: "center", justifyContent: "center" }}>

                  <Image style={{ width: "100%", height: 100, resizeMode: "stretch", borderRadius: 10 }} source={require('../assets/images/one.png')} />


                </View>
                <View style={{ alignItems: "center", justifyContent: "center" }}>
                  <Text style={{ fontWeight: "bold", fontSize: 12, paddingTop: 6, color: "#044184" }}>
                 {item.partner}
                        </Text>
                </View>

              </TouchableOpacity>
            </View> 
        
          
            )
            })
            : null
          }
          
          
          </View>
          </View>
          </ScrollView>










      </View>


    );
  }

}
const styles = StyleSheet.create({

  popbox: {
    width: '30%', alignContent: "center", alignItems: "center", backgroundColor: "#fff", borderRadius: 10,margin:5,
    shadowColor: "grey",
    shadowOffset: {
      width: 1,
      height: 1,
    },
    shadowOpacity: 2.60,
    shadowRadius: 2.62,
    elevation: 6,
  },
  popbox1: {
    width: '46%', alignContent: "center", alignItems: "center", backgroundColor: "#fff",margin:8, borderRadius: 10, marginRight: 5,
    shadowColor: "grey",
    shadowOffset: {
      width: 1,
      height: 1,
    },
    shadowOpacity: 2.60,
    shadowRadius: 2.62,
    elevation: 6,
  }
});