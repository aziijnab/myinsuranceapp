import React, { Component } from 'react';
import {
  View, Text, TouchableOpacity, Image, StyleSheet, Picker, Dimensions, ImageBackground, TextInput, Alert, AsyncStorage
} from 'react-native';
import { MaterialIcons, Ionicons, EvilIcons, MaterialCommunityIcons, Octicons, Feather, Entypo, AntDesign, FontAwesome5, Zocial } from '@expo/vector-icons';
import { ScrollView } from 'react-native-gesture-handler';
const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
import Header from './Header'
import { URL } from '../constants/API'
import i18n from 'i18n-js'
import translations from './translations'
console.disableYellowBox = true
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview'
import ToggleSwitch from 'toggle-switch-react-native'


export default class MeetExpert extends Component {
  // const [started, setStarted] = useState(false)
  constructor(props) {
    super(props);
    this.state = {
      started: false,
      full_name: "",
      phone: "",
      lang_done:false ,
    }

  }
  componentDidMount() {
   
    AsyncStorage.getItem("userID").then(user_data => {
      const val = JSON.parse(user_data);
      if (val) {
        this.setState({full_name:val.user.f_name,phone:val.user.phone_no})
     
      
       console.log(val)
      }
    });
    AsyncStorage.getItem("lang").then(lang => {
      const language = JSON.parse(lang);
      console.log("--a--")
      console.log(language)
      console.log(language)
      console.log("--a--")
      if (language) {  
        console.log("----")
        console.log(language)
        console.log(language)
      
        console.log("----")
        i18n.translations = translations;
        i18n.locale = language;
        i18n.fallbacks = true;
      
      }
      this.setState({lang_done : true})
    });
  }
  render() {
    if(this.state.lang_done){
    return (

      <View style={{ width: screenWidth, height: screenHeight, alignItems: "center"}}>

        <Header title={i18n.t('Appointment')} back={true} backtext={false} navigation={this.props.navigation} />

        <View style={{ width: "100%", marginTop: 20, alignItems: "center", alignContent: "center" }}>
          <View style={{ width: "90%", backgroundColor: "#fff", paddingVertical: 15, borderRadius: 15 }}>
            <View style={{ width: "100%", flexDirection: "row", padding: 5, paddingTop: 5,alignItems:"center" }}>
              <View style={{ width: "20%", paddingLeft: 5 }}>
                <Image style={screenWidth >= 400 ?  styles.box1_tablet : styles.box1} source={(require('../assets/images/Profilbild.png'))} />
              </View>
              <View style={{ width: "80%" }}>
                <Text style={{ fontSize: 18, color: "#044184", fontWeight: "bold" }}>{i18n.t('Consultation')}</Text>
              </View>

            </View>

            <View style={{ width: "100%", alignItems: "center", marginTop: 15 }}>
              <View style={{ width: "90%" }}>
                <Text style={{ fontWeight: "bold", fontSize: 15, color: "#696e78" }}>
                {i18n.t('Hello')} {this.state.full_name},{i18n.t('statement')} 
                  {/* I am your personal insurance expert. I will help you to optimize your insurance coverage. We can discuss all the details in a consultation */}
           </Text>
              </View>
            </View>
            <View style={{ width: "100%", alignItems: "center",marginTop:15}} >


<View style={{ width: "90%", justifyContent: "center", alignItems: "center", alignContent: "center" }}>
  <TouchableOpacity style={screenWidth >= 400 ?  styles.SignUp_button_tablet : styles.SignUp_button} onPress={() => this.props.navigation.push("Appointment_Mail")} >

    {/* <EvilIcons style={{}} name="sc-facebook" size={24} color="#ffffff" /> */}
    <Text style={{ color: '#fff', alignItems: "center", marginTop: 2, fontSize: 18, fontWeight: "bold" }}>{i18n.t('Mail')}</Text>

  </TouchableOpacity>
</View>
</View>


          </View>
        </View>










     



      </View>


    );
  }

  else{
    return(
      <View>
        <Text>Wait</Text>
      </View>
    )
  }
      
    }
  
  }
const styles = StyleSheet.create({
  pic: {


    height: 250,
    width: "100%",


  },
  login_button: {
    width: "80%",
    marginLeft: 40,
    marginTop: 10,
    paddingVertical: 8,
    alignItems: "center",
    alignContent: "center",
    backgroundColor: "#808080",
    borderRadius: 10
  },
  getstarted_button: {
    width: "90%",
    // top: 80,
    marginHorizontal: 20,
    paddingVertical: 10,
    alignItems: "center",
    backgroundColor: "#fff",
    borderRadius: 10,

    bottom: 0


  },
  SignUp_button: {

    width: "60%",


    paddingVertical: 5,
    alignItems: "center",
    alignContent: "center",
    backgroundColor: "#E7BF63",
    borderRadius: 10,
    borderWidth: 1,
    borderColor: "#E7BF63"
  },
  SignUp_button_tablet: {

    width: "30%",


    paddingVertical: 5,
    alignItems: "center",
    alignContent: "center",
    backgroundColor: "#E7BF63",
    borderRadius: 10,
    borderWidth: 1,
    borderColor: "#E7BF63"
  },
  box1: {

    width: "40%",
    height: 25,
    resizeMode: "cover",
    borderRadius: 80,



  },
  box1_tablet: {

    width: 50,
    height: 50,
    resizeMode: "cover",
    borderRadius: 50,



  },
});