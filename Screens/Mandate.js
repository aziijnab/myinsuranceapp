import React, { Component } from 'react';
import {
  View, Text, TouchableOpacity, Image, StyleSheet, Picker, Dimensions, ImageBackground, TextInput, Alert, AsyncStorage, Modal,ActivityIndicator
} from 'react-native';
import { URL } from '../constants/API'
import { Container, Content, ListItem, CheckBox, Body } from 'native-base';
console.disableYellowBox = true
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { MaterialIcons, MaterialCommunityIcons, Ionicons } from '@expo/vector-icons';
const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
import Header from './Header'
import i18n from 'i18n-js'
import translations from './translations'
import { ScrollView } from 'react-native-gesture-handler';
import SignaturePad from 'react-native-signature-pad';

export default class Mandate extends Component {
  // const [started, setStarted] = useState(false)
  constructor(props) {
    super(props);
    this.data = this.props.route.params
    console.log("this.data")
    console.log(this.data)
    console.log(">>>>>>>>>>>>>>>>this.data.companies_data>>>>>>>>>")
    console.log(this.data.companies_data)
    this.state = {
      started: false,
      f_name: "",
      s_name: "",
      email: "",
      password: "",
      phone: "",
      address: "",
      zip_code: "",
      city: "",
      company: "",
      f_name: "",
      street_no: "",
      zip_city: "",
      tel_private: "",
      dob: "",
      email: "",
      tel_business: "",
      place: "",
      client: "",
      modalVisible: false,
      signature: "",
      is_contract: false,
      lang_done: false,
      save_loading: false,
    }

  }
  async componentDidMount() {
    AsyncStorage.getItem("lang").then(lang => {
      const language = JSON.parse(lang);
      console.log("--a--")
      console.log(language)
      console.log(language)
      console.log("--a--")
      if (language) {
        console.log("----")
        console.log(language)
        console.log(language)
        console.log("----")
        i18n.translations = translations;
        i18n.locale = language;
        i18n.fallbacks = true;

      }
      this.setState({ lang_done: true })
    });

  }
  login = (email, password) => {
    fetch(URL + "login", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        "email": email,
        "password": password
      })
    })
      .then(res => res.json())
      .then(async response => {
        console.log(response);
        if (response.response == "success") {

          AsyncStorage.setItem("userID", JSON.stringify(response));
          Alert.alert(
            "Success",
            response.message,
            [
              {
                text: "Cancel",
                style: "cancel"
              },
              { text: "OK" }
            ],
            { cancelable: false }
          );
          // AsyncStorage.setItem("lang", JSON.stringify("en"));
          this.props.navigation.push("dashboard");

        }
        else {
          Alert.alert(
            "Sorry",
            response.message,
            [
              {
                text: "Cancel",
                style: "cancel"
              },
              { text: "OK" }
            ],
            { cancelable: false }
          );
        }


      })
      .catch(error => {
        console.log(error)
        alert("Please check internet connection");
      });
  }
  sendRequest = () => {
    let check_box = this.state.is_contract
    if (this.state.is_contract) {
      check_box = 1
    }
    else {
      check_box = 0
    }
    console.log("(((((((((((((((((((((((((((this.data)))))))))))))))))))))))))))")
    console.log(this.data)
    console.log(check_box)
    console.log("(((((((((((((((((((((((((((register)))))))))))))))))))))))))))")
    this.setState({ save_loading: true })
     fetch(URL + "register", {
      method: "POST",
      body: JSON.stringify({

        "f_name": this.data.register_data.register_data.f_name,
        "sur_name": this.data.register_data.register_data.s_name,
        "email": this.data.register_data.register_data.email,
        "password": this.data.register_data.register_data.password,
        "phone_no": this.data.register_data.register_data.phone,
        "address": this.data.register_data.register_data.address,
        "zip_code": this.data.register_data.register_data.zip_code,
        "city": this.data.register_data.register_data.city,
        "dob":this.data.register_data.register_data.dob,
        "salutation":this.data.register_data.register_data.favColor,
        "companies": this.data.companies_data,
        "signature": this.state.signature,
        "is_contract": check_box

      }),
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then(res => res.json())
      .then(async response => {
        console.log(response);
        if (response.response == "success") {

          // AsyncStorage.setItem("mandate", JSON.stringify({
          //   "f_name":this.data.register_data.register_data.f_name,
          //   "sur_name": this.data.register_data.register_data.s_name,
          //   "email": this.data.register_data.register_data.email,
          //   "password": this.data.register_data.register_data.password,
          //   "phone_no":this.data.register_data.register_data. phone,
          //   "address": this.data.register_data.register_data.address,
          //   "zip_code":this.data.register_data.register_data. zip_code,
          //   "city": this.data.register_data.register_data.city,
          //   "access_code":this.data.register_data.register_data. dob,
          //   "companies": this.data.companies_data,
          //   "signature":this.state.signature,
          //   "is_contract":check_box
          // }));
          this.login(this.data.register_data.register_data.email, this.data.register_data.register_data.password)
          // this.props.navigation.push("dashboard");
        }
        else {
          Alert.alert(
            "Sorry",
            response.errors,
            [
              {
                text: "Cancel",
                style: "cancel"
              },
              { text: "OK" }
            ],
            { cancelable: false }
          );
        }
        this.setState({ save_loading: false })
      }
      )
      .catch(error => {
        alert("Please check internet connection");

      });

  }

  _signaturePadError = (error) => {
    console.error(error);
  };

  _signaturePadChange = ({ base64DataUrl }) => {
    // console.log("Got new signature: " + base64DataUrl);
    this.setState({ signature: base64DataUrl })
    console.log(">>>>>>signature>>>>>>>")
    console.log(this.state.signature)
  };
  setModalVisible = () => {

    this.setState({ modalVisible: !this.state.modalVisible });

  }
  updateFavourite = () => {
    this.setState({
      is_contract: !this.state.is_contract
    });
  }
  render() {
    const { modalVisible } = this.state;
    if (this.state.lang_done) {
      return (

        <View style={{ width: screenWidth, height: screenHeight, alignItems: "center" }}>
          <Header title={i18n.t('mandate')} back={true} backtext={false} navigation={this.props.navigation} />
          <ScrollView style={{ width: "100%", height: "90%" }}
          showsVerticalScrollIndicator={false}
          >
            <View style={{ width: "100%", marginTop: 20, alignItems: "center", alignContent: "center", paddingBottom: 10 }}>
              <View style={{ width: "95%", backgroundColor: "#fff", paddingVertical: 15, borderRadius: 15 }}>
                <View style={{ width: "100%", marginTop: 10, paddingLeft: 15 }} >
                  <Text style={{ fontWeight: "bold", fontSize: 17, color: "#007AFF" }}>
                    {i18n.t('power')}
                  </Text>
                  <Text style={{ fontWeight: "bold", fontSize: 15, color: "#000", paddingTop: 5 }}>
                    {i18n.t('portfolio')}


                  </Text>
                  {/* <Text style={{ fontSize: 15, color: "#000", paddingTop: 10 }}>
                    {i18n.t('undesigned')}

                  </Text> */}
                  <View style={{ width: "100%", alignItems: "center", alignContent: "center" }}>

                    <View style={{ width: "100%", marginTop: 10 }}>
                      <View style={{ width: "97%", marginTop: 20, flexDirection: "row", borderBottomWidth: 1, borderColor: "#007AFF", paddingBottom: 7 }}>
                        <View style={{ width: "50%", marginHorizontal: 5, justifyContent: "center" }}>
                          <Text style={{ fontWeight: "bold", fontSize: 15 }}>
                            {i18n.t('gender')}
                          </Text>
                        </View>
                        <View style={{ width: "50%", justifyContent: "center" }}>
                          <Text style={{ textAlign: "center" }}>{this.data.register_data.register_data.favColor}</Text>
                        </View>

                      </View>
                      <View style={{ width: "97%", marginTop: 20, flexDirection: "row", borderBottomWidth: 1, borderColor: "#007AFF", paddingBottom: 7 }}>
                        <View style={{ width: "50%", marginHorizontal: 5, justifyContent: "center" }}>
                          <Text style={{ fontWeight: "bold", fontSize: 15 }}>
                            {i18n.t('FirstName')}
                          </Text>
                        </View>
                        <View style={{ width: "50%", justifyContent: "center" }}>
                          <Text style={{ textAlign: "center" }}>{this.data.register_data.register_data.f_name}</Text>
                        </View>

                      </View>
                      <View style={{ width: "97%", marginTop: 20, flexDirection: "row", borderBottomWidth: 1, borderColor: "#007AFF", paddingBottom: 7 }}>
                        <View style={{ width: "50%", marginHorizontal: 5, justifyContent: "center" }}>
                          <Text style={{ fontWeight: "bold", fontSize: 15 }}>
                            {i18n.t('Surname')}
                          </Text>
                        </View>
                        <View style={{ width: "50%", justifyContent: "center" }}>
                          <Text style={{ textAlign: "center" }}>{this.data.register_data.register_data.s_name}</Text>
                        </View>

                      </View>
                      <View style={{ width: "97%", marginTop: 20, flexDirection: "row", borderBottomWidth: 1, borderColor: "#007AFF", paddingBottom: 7 }}>
                        <View style={{ width: "50%", marginHorizontal: 5, justifyContent: "center" }}>
                          <Text style={{ fontWeight: "bold", fontSize: 15 }}>
                            {i18n.t('email')}
                          </Text>
                        </View>
                        <View style={{ width: "50%", justifyContent: "center" }}>
                          <Text style={{ textAlign: "center" }}>{this.data.register_data.register_data.email}</Text>
                        </View>

                      </View>
                      <View style={{ width: "97%", marginTop: 20, flexDirection: "row", borderBottomWidth: 1, borderColor: "#007AFF", paddingBottom: 7 }}>
                        <View style={{ width: "50%", marginHorizontal: 5, justifyContent: "center" }}>
                          <Text style={{ fontWeight: "bold", fontSize: 15 }}>
                            {i18n.t('phone')}

                          </Text>
                        </View>
                        <View style={{ width: "50%", justifyContent: "center" }}>
                          <Text style={{ textAlign: "center" }}>{this.data.register_data.register_data.phone}</Text>
                        </View>

                      </View>
                      <View style={{ width: "97%", marginTop: 20, flexDirection: "row", borderBottomWidth: 1, borderColor: "#007AFF", paddingBottom: 7 }}>
                        <View style={{ width: "50%", marginHorizontal: 5, justifyContent: "center" }}>
                          <Text style={{ fontWeight: "bold", fontSize: 15 }}>
                            {i18n.t('address')}

                          </Text>
                        </View>
                        <View style={{ width: "50%", justifyContent: "center" }}>
                          <Text style={{ textAlign: "center" }}>{this.data.register_data.register_data.address}</Text>
                        </View>

                      </View>
                      <View style={{ width: "97%", marginTop: 20, flexDirection: "row", borderBottomWidth: 1, borderColor: "#007AFF", paddingBottom: 7 }}>
                        <View style={{ width: "50%", marginHorizontal: 5, justifyContent: "center" }}>
                          <Text style={{ fontWeight: "bold", fontSize: 15 }}>
                            {i18n.t('ZipCode')}

                          </Text>
                        </View>
                        <View style={{ width: "50%", justifyContent: "center" }}>
                          <Text style={{ textAlign: "center" }}>{this.data.register_data.register_data.zip_code}</Text>
                        </View>

                      </View>
                      <View style={{ width: "97%", marginTop: 20, flexDirection: "row", borderBottomWidth: 1, borderColor: "#007AFF", paddingBottom: 7 }}>
                        <View style={{ width: "50%", marginHorizontal: 5, justifyContent: "center" }}>
                          <Text style={{ fontWeight: "bold", fontSize: 15 }}>
                            {i18n.t('City')}

                          </Text>
                        </View>
                        <View style={{ width: "50%", justifyContent: "center" }}>
                          <Text style={{ textAlign: "center" }}>{this.data.register_data.register_data.city}</Text>
                        </View>

                      </View>
                      <View style={{ width: "97%", marginTop: 20, flexDirection: "row", borderBottomWidth: 1, borderColor: "#007AFF", paddingBottom: 7 }}>
                  <View style={{ width: "50%", marginHorizontal: 5, justifyContent: "center" }}>
                    <Text style={{ fontWeight: "bold", fontSize: 15 }}>
                    {i18n.t('dob')}

</Text>
                  </View>
                  <View style={{ width: "50%", justifyContent: "center" }}>
                     <Text style={{textAlign:"center"}}>{this.data.register_data.register_data.dob}</Text>
                  </View>

                </View>



                    </View>

                  </View>



                  {/* <Text style={{ fontWeight: "bold", fontSize: 15, color: "#000", paddingTop: 10 }}>
                    {i18n.t('inp')}

                  </Text>
                  <Text style={{ fontSize: 15, color: "#000", paddingTop: 20 }}>
                    {i18n.t('broker')}

                  </Text>
                  <Text style={{ fontSize: 15, color: "#000", paddingTop: 20 }}>
                    {i18n.t('policy')}

                  </Text>
                  <Text style={{ fontSize: 15, color: "#000", paddingTop: 20 }}>
                    {i18n.t('agreement')}

                  </Text>
                  <Text style={{ fontWeight: "bold", fontSize: 15, color: "#007AFF", paddingTop: 20 }}>
                    {i18n.t('Services')}

                  </Text>
                  <Text style={{ fontSize: 15, color: "#000", paddingTop: 20 }}>
                    {i18n.t('Supervision')}



                  </Text>
                  <Text style={{ fontSize: 15, color: "#000", paddingTop: 20 }}>
                    {i18n.t('Review')}

                  </Text>
                  <Text style={{ fontSize: 15, color: "#000", paddingTop: 20 }}>
                    {i18n.t('market')}
                  </Text>
                  <Text style={{ fontSize: 15, color: "#000", paddingTop: 20 }}>
                    {i18n.t('offers')}
                  </Text>
                  <Text style={{ fontSize: 15, color: "#000", paddingTop: 20 }}>
                    {i18n.t('Assistance')}

                  </Text>
                  <Text style={{ fontSize: 15, color: "#000", paddingTop: 20 }}>
                    {i18n.t('renewals')}


                  </Text>
                  <Text style={{ fontSize: 15, color: "#000", paddingTop: 20 }}>
                    {i18n.t('deals')}



                  </Text>
                  <Text style={{ fontSize: 15, color: "#000", paddingTop: 20 }}>
                    {i18n.t('policyholder')}



                  </Text>
                  <Text style={{ fontSize: 15, color: "#000", paddingTop: 5 }}>
                    {i18n.t('applications')}

                  </Text>
                  <Text style={{ fontSize: 15, color: "#000", paddingTop: 10 }}>
                    {i18n.t('Original')}


                  </Text>
                  <Text style={{ fontSize: 15, color: "#000", paddingTop: 10 }}>
                    {i18n.t('brokerage')}


                  </Text> */}


                  <View style={{ width: "90%", flexDirection: "row", paddingTop: 50 }}>
                    <View style={{ width: "50%", marginHorizontal: 5, justifyContent: "center" }}>
                      <Text style={{ fontWeight: "bold", fontSize: 15 }}>
                        {i18n.t('Signature')}
                      </Text>
                    </View>
                    {this.state.signature ? (
                      <TouchableOpacity style={{ width: "40%", alignItems: "center", paddingLeft: 20 }} onPress={() => this.setModalVisible(true)}>
                        <Image
                          resizeMode={"contain"}
                          style={{ width: 335, height: 114 }}
                          source={{ uri: this.state.signature }}
                        />
                      </TouchableOpacity>
                    ) : (
                      <View style={{ width: "40%", alignItems: "center", paddingLeft: 20 }}>

                        <TouchableOpacity style={styles.signature} onPress={() => this.setModalVisible(true)} >

                          <Text style={{ color: '#fff', alignItems: "center", marginTop: 2, fontSize: 12, fontWeight: "bold" }}>{i18n.t('sign')}</Text>

                        </TouchableOpacity>
                      </View>

                    )}




                  </View>
                  <View style={{ width: "90%", flexDirection: "row", paddingBottom: 10, paddingTop: 35 }}>
                    <View style={{ width: "10%", alignItems: "center" }}>

                      <CheckBox onPress={() => this.updateFavourite()} checked={this.state.is_contract} />
                    </View>

                    <TouchableOpacity 
                    onPress={() => this.props.navigation.navigate("terms_condition")}>
                      <Text style={{ paddingLeft: 15, marginTop: 3, color: "#044184", textDecorationLine: "underline" }}> {i18n.t('updates')}</Text>
                    </TouchableOpacity>

                  </View>


                </View>

              </View>
            </View>
            <View style={{ width: "100%", alignItems: "center", paddingBottom: 20}} >
              <View style={{ width: "90%", justifyContent: "center", alignItems: "center", alignContent: "center" }}>
              {this.state.save_loading ? (
                  <View style={screenWidth >= 400 ? styles.box1_tablet : styles.box1}>
                                        <ActivityIndicator color={"#fff"} size={"large"} />
                                    </View>
                                    ) : (
                <TouchableOpacity style={screenWidth >= 400 ? styles.box1_tablet : styles.box1} onPress={() => this.sendRequest()} >

                  {/* <EvilIcons style={{}} name="sc-facebook" size={24} color="#ffffff" /> */}
                  <Text style={{ color: '#fff', alignItems: "center", marginTop: 2, fontSize: 24, fontWeight: "bold" }}>{i18n.t('Done')}</Text>

                </TouchableOpacity>
                                    )}
              </View>
            </View>

          </ScrollView>
          <Modal
            animationType="slide"
            transparent={true}
            swipeDirection="left"
            onSwipe={this.closeModal}
            onBackdropPress={this.closeModal}
            visible={this.state.modalVisible}

          >

            <View style={styles.centeredView}>

              <View style={styles.modalView}>
                <View style={{ width: "100%" }}>
                  {/* <TouchableOpacity style={{ width: "15%", alignContent: "center", alignItems: "center", paddingLeft: 5 }} onPress={() => this.setModalVisible()}
                  >
                    <Ionicons name="ios-arrow-back" size={25} color="#FE000C" />
                  </TouchableOpacity> */}
                </View>
                <View style={{ width: "100%", alignItems: "center", alignContent: "center", }}>
                  <View style={{ width: "90%" }}>
                    <View style={{ width: "100%", height: 200, justifyContent: "center", borderWidth: 1, borderColor: "#007AFF", marginLeft: 5 }}>

                      <SignaturePad
                        onError={this._signaturePadError}
                        onChange={this._signaturePadChange}

                        style={{ flex: 1, backgroundColor: 'white' }} />

                    </View>
                    <View style={{ width: "100%", alignItems: "center" ,marginTop:20}} >
                      <View style={{ width: "90%", justifyContent: "center", alignItems: "center", alignContent: "center" }}>
                        <TouchableOpacity style={styles.Done} onPress={() => this.setModalVisible(false)} >

                          <Text style={{ color: '#fff', alignItems: "center", marginTop: 2, fontSize: 12, fontWeight: "bold" }}>{i18n.t('done')}</Text>

                        </TouchableOpacity>
                      </View>
                    </View>
                  </View>


                </View>


                <ScrollView showsVerticalScrollIndicator={false}>

                </ScrollView>


                {/* <View style={{ width: "100%", alignItems: "center", alignContent: "center",marginBottom:10 }} onPress={() => this.props.navigation.navigate('itemCart')}>
                <View style={{ width: "90%" }}>
                  <TouchableOpacity style={styles.filter}  >
                    <Text
                      style={{
                        color: "white",
                        alignItems: "center",
                        marginTop: 2,
                        fontSize: 16,
                        fontWeight: "bold",
                      }}
                    >
                      Apply Filters
            </Text>
                  </TouchableOpacity>
                </View>

              </View> */}

              </View>
            </View>
          </Modal>



        </View>

      );
    }
    else {
      return (
        <View>
          <Text>Wait</Text>
        </View>
      )
    }

  }

}
const styles = StyleSheet.create({
  pic: {


    height: 250,
    width: "100%",
    borderRadius: 50



  },
  login_button: {
    width: "100%",
    marginLeft: 10,
    marginTop: 10,
    paddingVertical: 8,
    alignItems: "center",
    alignContent: "center",
    backgroundColor: "#808080",
    borderRadius: 10
  },
  SignUp_button: {

    width: "90%",


    paddingVertical: 5,
    alignItems: "center",
    alignContent: "center",
    backgroundColor: "#E7BF63",
    borderRadius: 10,
    borderWidth: 1,
    borderColor: "#E7BF63"
  },
  SignUp_button_tablet: {

    width: "45%",


    paddingVertical: 5,
    alignItems: "center",
    alignContent: "center",
    backgroundColor: "#E7BF63",
    borderRadius: 10,
    borderWidth: 1,
    borderColor: "#E7BF63"
  },
  signature: {

    width: "90%",


    paddingVertical: 10,
    alignItems: "center",
    alignContent: "center",
    backgroundColor: "#E7BF63",
    borderRadius: 10,
    borderWidth: 1,
    borderColor: "#E7BF63"
  },
  Done: {

    width: "60%",


    paddingVertical: 5,
    alignItems: "center",
    alignContent: "center",
    backgroundColor: "#E7BF63",
    borderRadius: 10,
    borderWidth: 1,
    borderColor: "#E7BF63"
  },
  box1: {

    width: "60%",


    paddingVertical: 8 ,
    alignItems: "center",
    alignContent: "center",
    backgroundColor: "#E7BF63",
    borderRadius: 10,
    borderWidth: 1,
    borderColor: "#E7BF63"



  },
  centeredView: {
    width: screenWidth,
    height: screenHeight,
    justifyContent: "flex-end",

  },
  box1_tablet: {

    width: 50,
    height: 70,
    resizeMode: "cover",
    borderRadius: 50


  },
  modalView: {
    // justifyContent: "center",
    // alignItems: "center",
    width: "100%",
    height: "60%",
    backgroundColor: "white",
    borderRadius: 20,
    paddingTop: 40,


    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
});
// {this.data.companies_data_arr.map((item, index) => {
//   return (
//     <View key={index} style={{width:"100%", borderRadius:15, borderWidth:1, borderColor:"#808080", marginTop:10}}>
// <View style={{ width: "100%", marginTop: 20, flexDirection: "row", borderBottomWidth: 1, borderColor: "#007AFF", paddingBottom: 7 }}>
//       <View style={{ width: "50%", marginHorizontal: 5, justifyContent: "center" }}>
//         <Text style={{ fontWeight: "bold", fontSize: 15 }}>
//           Contact Person:
// </Text>
//       </View>
//       <View style={{ width: "50%", justifyContent: "center" }}>
//          <Text style={{textAlign:"center"}}>{item.contact_person_name}</Text>
//       </View>

//     </View>
//     <View style={{ width: "100%", marginTop: 20, flexDirection: "row", borderBottomWidth: 1, borderColor: "#007AFF", paddingBottom: 7 }}>
//       <View style={{ width: "50%", marginHorizontal: 5, justifyContent: "center" }}>
//         <Text style={{ fontWeight: "bold", fontSize: 15 }}>
//           Partner Name:
// </Text>
//       </View>
//       <View style={{ width: "45%" }}>
//          <Text style={{textAlign:"center"}}>{item.partner}</Text>
//       </View>

//     </View>
//     <View style={{ width: "100%", marginTop: 20, flexDirection: "row", borderBottomWidth: 1, borderColor: "#007AFF", paddingBottom: 7 }}>
//       <View style={{ width: "50%", marginHorizontal: 5, justifyContent: "center" }}>
//         <Text style={{ fontWeight: "bold", fontSize: 15 }}>
//           Street, No:
// </Text>
//       </View>
//       <View style={{ width: "50%", justifyContent: "center" }}>
//          <Text style={{textAlign:"center"}}>{item.address}</Text>
//       </View>

//     </View>
//     <View style={{ width: "100%", marginTop: 20, flexDirection: "row", borderBottomWidth: 1, borderColor: "#007AFF", paddingBottom: 7 }}>
//       <View style={{ width: "50%", marginHorizontal: 5, justifyContent: "center" }}>
//         <Text style={{ fontWeight: "bold", fontSize: 15 }}>
//           Postcode / town:

// </Text>
//       </View>
//       <View style={{ width: "50%", justifyContent: "center" }}>
//          <Text style={{textAlign:"center"}}>{item.town}</Text>
//       </View>

//     </View>
//     <View style={{ width: "100%", marginTop: 20, flexDirection: "row", borderBottomWidth: 1, borderColor: "#007AFF", paddingBottom: 7 }}>
//       <View style={{ width: "50%", marginHorizontal: 5, justifyContent: "center" }}>
//         <Text style={{ fontWeight: "bold", fontSize: 15 }}>
//           Tel. Private:

// </Text>
//       </View>
//       <View style={{ width: "50%", justifyContent: "center" }}>
//          <Text style={{textAlign:"center"}}>{item.direct_dialing}</Text>
//       </View>

//     </View>
//     <View style={{ width: "100%", marginTop: 20, flexDirection: "row", borderBottomWidth: 1, borderColor: "#007AFF", paddingBottom: 7 }}>
//       <View style={{ width: "50%", marginHorizontal: 5, justifyContent: "center" }}>
//         <Text style={{ fontWeight: "bold", fontSize: 15 }}>
//           Broker Number:

// </Text>
//       </View>
//       <View style={{ width: "50%", justifyContent: "center" }}>
//          <Text style={{textAlign:"center"}}>{item.broker_number}</Text>
//       </View>

//     </View>
//     <View style={{ width: "100%", marginTop: 20, flexDirection: "row", borderBottomWidth: 1, borderColor: "#007AFF", paddingBottom: 7 }}>
//       <View style={{ width: "50%", marginHorizontal: 5, justifyContent: "center" }}>
//         <Text style={{ fontWeight: "bold", fontSize: 15 }}>
//           Tel-Business:

// </Text>
//       </View>
//       <View style={{ width: "50%", justifyContent: "center" }}>
//          <Text style={{textAlign:"center"}}>{item.main_number}</Text>
//       </View>

//     </View>
//     <View style={{ width: "95%", marginTop: 20, flexDirection: "row", paddingBottom: 7 }}>
//       <View style={{ width: "50%", marginHorizontal: 5, justifyContent: "center" }}>
//         <Text style={{ fontWeight: "bold", fontSize: 15 }}>
//           E-mail:

// </Text>
//       </View>
//       <View style={{ width: "50%", justifyContent: "center" }}>
//          <Text style={{textAlign:"center"}}>{item.email}</Text>
//       </View>

//     </View>

// </View>

//   )
// })}