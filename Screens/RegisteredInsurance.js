import React, { Component } from 'react';
import {
  View, Text, TouchableOpacity, Image, StyleSheet, Picker, Dimensions, ImageBackground, TextInput, Alert, AsyncStorage
} from 'react-native';
import { Ionicons, EvilIcons, MaterialIcons,FontAwesome } from '@expo/vector-icons';
import { ScrollView } from 'react-native-gesture-handler';
const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
import { URL,IMAGE_URL } from '../constants/API'
console.disableYellowBox = true
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview'
import Footer from './Footer';
import Header from './Header'
import i18n from 'i18n-js'
import translations from './translations'

export default class RegisteredInsurance extends Component {
  // const [started, setStarted] = useState(false)
  constructor(props) {
    super(props);
    this.data =this.props.route.params
    console.log(">>>>>>>>>>>>>>>THIS DATA>>>>>>>>>>")
    console.log(this.data)
    this.state = {
      started: false,
       get_data:[],
       companies_selected:[],
       companies_selected_data:[],
       user_id:"",
      lang_done:false


  }
}
  async componentDidMount() {
    AsyncStorage.getItem("userID").then(user_data => {
      const val = JSON.parse(user_data);
      if (val) {
        user_id=val.user.uuid
       console.log(">>>>>>>>>>>>>>>>>>this.data>>>>>>>>>>")
       console.log(this.data)
      }

      this.get_companiesData()

    });
    AsyncStorage.getItem("lang").then(lang => {
      const language = JSON.parse(lang);
      console.log("--a--")
      console.log(language)
      console.log(language)
      console.log("--a--")
      if (language) {  
        console.log("----")
        console.log(language)
        console.log(language)
        console.log("----")
        i18n.translations = translations;
        i18n.locale = language;
        i18n.fallbacks = true;
      
      }
      this.setState({lang_done : true})
    });

  }
  SelectCompany = (index) => {
    let current_item_obj = this.state.get_data.find(
      item => item.uuid == index
    );

   //console.log("-------OBJ----------")
   //console.log(current_item_obj)
   //console.log("-------OBJ----------")
if(current_item_obj.selected){
 let indexOfId = this.state.companies_selected.indexOf(index)

  this.state.companies_selected.splice(indexOfId, 1);

  let current_item_obj_selected = this.state.companies_selected_data.findIndex(
    item => item.id == current_item_obj.id
  );

this.state.companies_selected_data.splice(current_item_obj_selected, 1);
  current_item_obj.selected = false;

}else{
this.state.companies_selected.push(index)
this.state.companies_selected_data.push(current_item_obj)
  current_item_obj.selected = true;
}
console.log("companies_selected_data")
console.log(this.state.companies_selected_data)
    this.setState({get_data:this.state.get_data})
  }
  get_companiesData = () => {
    fetch(URL + "get-companies", {
      method: "Get",
      headers: {
        "Content-Type": "application/json",
      },

      // body: JSON.stringify(
      //   {
      //     "user_id": this.state.user_id,
      //   }
      // )
    })
      .then(res => res.json())
      .then(async response => {
        console.log("  get_companiesData");
        console.log(response);
        if (response.response == "success") {
          this.setState({ get_data: response.data })

        }
        else {
          Alert.alert(
            "Sorry",
            response.message,
            [
              {
                text: "Cancel",
                style: "cancel"
              },
              { text: "OK" }
            ],
            { cancelable: false }
          );
        }


      })
      .catch(error => {
        alert("Please check internet connection");
      });


  }

  render() {
    if(this.state.lang_done){
    return (

      <View style={{ width: screenWidth, height: screenHeight, alignItems: "center" }}>
        <Header title={i18n.t('selectCompanies')} back={true} backtext={false} navigation={this.props.navigation} />
        <ScrollView showsVerticalScrollIndicator={false} style={{ width: "97%" }}>
              <View style={{ width: "100%", paddingTop: 5, paddingHorizontal: 10 }}>
          <View style={{ flexDirection: 'row', flexWrap: 'wrap', justifyContent: "space-between",marginTop:20,marginBottom:100 }}>
          {this.state.get_data.length > 0 ?
            this.state.get_data.map((item, index) => {
              if(item.partner == "Other"){
                return (
                <View key={index} style={styles.popbox}>
              <TouchableOpacity style={{ width: "100%", padding: 5 }}
              onPress={() => this.SelectCompany(item.uuid)}
              // onPress={() => this.props.navigation.push("uploadPolicy",{companies_data:item})}
              >
                <View style={{ width: "100%", alignItems: "center", justifyContent: "center" }}>

                  <Image style={{ width: "100%", height: 100, resizeMode: "stretch", borderRadius: 10 }}  source={{uri:IMAGE_URL+item.logo}} />


                </View>
                <View style={{ alignItems: "center", justifyContent: "center" }}>
                  <Text style={{ fontWeight: "bold", fontSize: 12, paddingTop: 6, color: "#044184" }}>
                  {i18n.t('other')}
                        </Text>
                </View>
                {item.selected ? (
                  <View style={{width:"100%",height:20,alignItems:"center"}}>
                  <FontAwesome name="check" size={24} color="black" />
</View>
                ) : null}

              </TouchableOpacity>
            </View>
             
                 ) }
              else{
                return (
            <View key={index} style={styles.popbox}>
              <TouchableOpacity style={{ width: "100%", padding: 5 }}
              onPress={() => this.SelectCompany(item.uuid)}
              // onPress={() => this.props.navigation.push("uploadPolicy",{companies_data:item})}
              >
                <View style={{ width: "100%", alignItems: "center", justifyContent: "center" }}>

                  <Image style={{ width: "100%", height: 100, resizeMode: "stretch", borderRadius: 10 }}  source={{uri:IMAGE_URL+item.logo}} />


                </View>
                <View style={{ alignItems: "center", justifyContent: "center" }}>
                  <Text style={{ fontWeight: "bold", fontSize: 12, paddingTop: 6, color: "#044184" }}>
                 {item.partner}
                        </Text>
                </View>
                {item.selected ? (
                  <View style={{width:"100%",height:20,alignItems:"center"}}>
                  <FontAwesome name="check" size={24} color="black" />
</View>
                ) : null}

              </TouchableOpacity>
            </View> 
        
          
            )
              }
            
            })
            : null
          }
          <View style={styles.popbox2}>
               
              </View>
          
          </View>
          </View>
          </ScrollView>



 
          <View style={{ width: "100%", position: "absolute", alignItems: "center", bottom: 10 }} >


<View style={{ width: "90%", justifyContent: "center", alignItems: "center", alignContent: "center" }}>
  <TouchableOpacity style={styles.SignUp_button} onPress={() => this.props.navigation.push("mandate",{register_data:this.data,companies_data:this.state.companies_selected,companies_data_arr:this.state.companies_selected_data})} >
    <Text style={{ color: '#fff', alignItems: "center", marginTop: 2, fontSize: 18, fontWeight: "bold" }}>{i18n.t('continue')}</Text>
  </TouchableOpacity>
</View>

</View>







      </View>


);
}
else{
  return(
    <View>
      <Text>Wait</Text>
    </View>
  )
}
    
  }

}
const styles = StyleSheet.create({

  popbox: {
    width: '30%', alignContent: "center", alignItems: "center", backgroundColor: "#fff", borderRadius: 10,margin:5,
    shadowColor: "grey",
    shadowOffset: {
      width: 1,
      height: 1,
    },
    shadowOpacity: 2.60,
    shadowRadius: 2.62,
    elevation: 6,
  },
  popbox1: {
    width: '46%', alignContent: "center", alignItems: "center", backgroundColor: "#fff",margin:8, borderRadius: 10, marginRight: 5,
    shadowColor: "grey",
    shadowOffset: {
      width: 1,
      height: 1,
    },
    shadowOpacity: 2.60,
    shadowRadius: 2.62,
    elevation: 6,
  },
  SignUp_button: {

    width: "90%",


    paddingVertical: 10,
    alignItems: "center",
    alignContent: "center",
    backgroundColor: "#E7BF63",
    borderRadius: 10,
    borderWidth: 1,
    borderColor: "#E7BF63"
  },
  popbox2: {
    width: '30%', alignContent: "center", alignItems: "center", borderRadius: 10,margin:5,
    
  },
});