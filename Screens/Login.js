import React, { Component } from 'react';
import {
  View, Text, TouchableOpacity, Image, StyleSheet, Picker, Dimensions, ImageBackground, TextInput, Alert, AsyncStorage
} from 'react-native';
import { MaterialIcons, Ionicons, EvilIcons, MaterialCommunityIcons, Octicons, Feather, Entypo, AntDesign, FontAwesome5, Zocial } from '@expo/vector-icons';
import { ScrollView } from 'react-native-gesture-handler';
const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
import Header from './Header'
import { URL } from '../constants/API'
import i18n from 'i18n-js'
import translations from './translations'

console.disableYellowBox = true
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview'
import ToggleSwitch from 'toggle-switch-react-native'


export default class Login extends Component {
  // const [started, setStarted] = useState(false)
  constructor(props) {
    super(props);
    this.state = {
      started: false,
      email: "",
      password: "",
      lang_done:false

    }
  
  }
  async componentDidMount() {
    
    AsyncStorage.getItem("lang").then(lang => {
      const language = JSON.parse(lang);
      console.log("--a--")
      console.log(language)
      console.log(language)
      console.log("--abbbbbbbbbbbbb--")
      if (language) {  
        console.log("----")
        console.log(language)
        console.log(language)
        console.log("----")
        i18n.translations = translations;
        i18n.locale = language;
        i18n.fallbacks = true;
      
      }
      this.setState({lang_done : true})
    });
  }
  submit = () => {
    let email = this.state.email
    let password = this.state.password

    if (email == "" && password == "") {
      Alert.alert(
        "Sorry",
        " E-mail and Password is required",
        [
          {
            text: "Cancel",
            style: "cancel"
          },
          { text: "OK" }
        ],
        { cancelable: false }
      );
    }

    if (email != "" && password == "") {
      Alert.alert(
        "Sorry",
        "Password is required",
        [
          {
            text: "Cancel",
            style: "cancel"
          },
          { text: "OK" }
        ],
        { cancelable: false }
      );
    }
    if (email == "" && password != "") {
      Alert.alert(
        "Sorry",
        "E-mail is required",
        [
          {
            text: "Cancel",
            style: "cancel"
          },
          { text: "OK" }
        ],
        { cancelable: false }
      );
    }
    if (email != "" && password != "") {

      console.log(email + "---" + password)
      fetch(URL + "login", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          "email": email,
          "password": password
        })
      })
        .then(res => res.json())
        .then(async response => {
          console.log(response);
          if (response.response == "success") {

            AsyncStorage.setItem("userID", JSON.stringify(response));
            Alert.alert(
              "Success",
              response.message,
              [
                {
                  text: "Cancel",
                  style: "cancel"
                },
                { text: "OK" }
              ],
              { cancelable: false }
            );
            // AsyncStorage.setItem("lang", JSON.stringify("en"));
            this.props.navigation.navigate("dashboard");

          }
          else {
            Alert.alert(
              "Sorry",
              response.message,
              [
                {
                  text: "Cancel",
                  style: "cancel"
                },
                { text: "OK" }
              ],
              { cancelable: false }
            );
          }


        })
        .catch(error => {
          console.log(error)
          alert("Please check internet connection");
        });

    }

  }

  render() {
    if(this.state.lang_done){
    return (

      <View style={{ width: screenWidth, height: screenHeight, alignItems: "center" }}>

        <Header title={i18n.t('login')} back={false} backtext={false} navigation={this.props.navigation} />
        <View style={{ width: "100%", marginTop: 40 }}>
          <Text style={{ fontWeight: "bold", fontSize: 20, color: "#044184", paddingLeft: 20 }}>
          {i18n.t('signin')}
          </Text>
        </View>

        <View style={{ width: "100%", marginTop: 40, paddingVertical: 5, alignContent: "center", alignItems: "center" }}>
          <View style={{ width: "90%", borderBottomWidth: 1, borderBottomColor: "#044184", paddingBottom: 5 }}>
            <TextInput
              style={{ color: '#044184', fontSize: 16, paddingHorizontal: 15 }}
              secureTextEntry={false}
              keyboardType="email-address"
              placeholder={i18n.t('email')}
              placeholderTextColor="#044184"
              onChangeText={(email) => this.setState({ email })}
              value={this.state.email}
            />
          </View>
        </View>
        <View style={{ width: "100%", marginTop: 25, paddingVertical: 5, alignContent: "center", alignItems: "center" }}>
          <View style={{ width: "90%", borderBottomWidth: 1, borderBottomColor: "#044184", paddingBottom: 5, flexDirection: "row" }}>
            <View style={{ width: "90%" }}>
              <TextInput
                style={{ color: '#044184', fontSize: 16, paddingLeft: 15 }}
                secureTextEntry={true}
                placeholder={i18n.t('password')}
                placeholderTextColor="#044184"
                onChangeText={(password) => this.setState({ password })}
                value={this.state.password}
              />
            </View>
            <View style={{ width: "10%", paddingTop: 3 }}>
              <MaterialIcons name="vpn-key" size={15} color="#044184" />
            </View>
      

          </View>
        </View>











        <View style={{ width: "100%", position: "absolute", alignItems: "center", bottom: 15 }} >


          <View style={{ width: "90%", justifyContent: "center", alignItems: "center", alignContent: "center" }}>
            <TouchableOpacity style={styles.SignUp_button} onPress={() => this.submit()} >

              {/* <EvilIcons style={{}} name="sc-facebook" size={24} color="#ffffff" /> */}
              <Text style={{ color: '#fff', alignItems: "center", marginTop: 2, fontSize: 18, fontWeight: "bold" }}>{i18n.t('signin')}</Text>

            </TouchableOpacity>
          </View>
          <TouchableOpacity style={{ paddingBottom: 10, paddingTop: 15 }} onPress={() => this.props.navigation.navigate('forgetpassword')}>
            <Text style={{ fontWeight: "bold", fontSize: 16, color: "#E7BF63", textAlign: "center", textDecorationLine: "underline" }}>
              {i18n.t('Forgot')}
    </Text>
          </TouchableOpacity>





        </View>



      </View>


);
}
else{
  return(
    <View>
      <Text>Wait</Text>
    </View>
  )
}
    
  }

}
const styles = StyleSheet.create({
  pic: {


    height: 250,
    width: "100%",


  },
  login_button: {
    width: "80%",
    marginLeft: 40,
    marginTop: 10,
    paddingVertical: 8,
    alignItems: "center",
    alignContent: "center",
    backgroundColor: "#808080",
    borderRadius: 10
  },
  getstarted_button: {
    width: "90%",
    // top: 80,
    marginHorizontal: 20,
    paddingVertical: 10,
    alignItems: "center",
    backgroundColor: "#fff",
    borderRadius: 10,

    bottom: 0


  },
  SignUp_button: {

    width: "90%",


    paddingVertical: 10,
    alignItems: "center",
    alignContent: "center",
    backgroundColor: "#E7BF63",
    borderRadius: 10,
    borderWidth: 1,
    borderColor: "#E7BF63"
  },
});