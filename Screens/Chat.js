import React, { Component } from 'react';
import {
  View, Text, TouchableOpacity, Image, StyleSheet, Picker, Dimensions, ImageBackground, TextInput, Alert, AsyncStorage, Keyboard
} from 'react-native';
import { URL } from '../constants/API'
import { Container, Content, ListItem, CheckBox, Body } from 'native-base';
console.disableYellowBox = true
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { MaterialIcons, MaterialCommunityIcons, Entypo, Feather, Ionicons } from '@expo/vector-icons';
const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
import Header from './Header'
import HTML from "react-native-render-html";
import i18n from 'i18n-js'
import translations from './translations'
import { ScrollView } from 'react-native-gesture-handler';
import Constants from 'expo-constants';
export default class Chat extends Component {
  // const [started, setStarted] = useState(false)
  constructor(props) {
    super(props);
    this.state = {
      started: false,
      user_id: "",
      keyboardHeight: 5,
      hideInput: false,
      get_data: [],
      message_text: ""
    }

  }
  componentDidMount() {
    AsyncStorage.getItem("userID").then(user_data => {
      const val = JSON.parse(user_data);
      if (val) {

        this.setState({ user_id: val.user.uuid })
      }
      this.submitmessage()
    });
    this.keyboardDidShowListener = Keyboard.addListener(
      "keyboardDidShow",
      this._keyboardDidShow
    );
    this.keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      this._keyboardDidHide,
    );
  }
  _keyboardDidShow = e => {
    var KeyboardHeight = e.endCoordinates.height;
    this.setState({
      keyboardHeight: KeyboardHeight,
    });
  };
  _keyboardDidHide = e => {
    this.setState({
      keyboardHeight: 0,
    });
  };
  submitmessage = () => {
    fetch(URL + "send-message", {
      method: "POST",
      body: JSON.stringify({
        "user_id": this.state.user_id,
        "message": this.state.message_text,
      }),
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(res => res.json())
      .then(async response => {
        console.log("submitmessage");
        console.log(response);
        if (response.response == "success") {
          this.setState({ get_data: response.chat })
          console.log("get_data")
          console.log(this.state.get_data)
          this.setState({ message_text: "" });
        }

        else {
          Alert.alert(
            "Sorry",
            response.message,
            [
              {
                text: "Cancel",
                style: "cancel"
              },
              { text: "OK" }
            ],
            { cancelable: false }
          );
        }


      })
      .catch(error => {
        alert("Please check internet connection");
      });


  }

  render() {
    return (

      <View style={{ width: screenWidth, height: screenHeight, alignItems: "center" }}>
        <Header title={i18n.t('ask')} back={true} backtext={false} navigation={this.props.navigation} />
        <View style={{ width: "100%", height: "90%", backgroundColor: "#E7BF63" }}>
          <ScrollView style={{ borderTopLeftRadius: 30, }} ref={ref => this.scrollView = ref} onContentSizeChange={(contentWidth, contentHeight) => {
            this.scrollView.scrollToEnd({ animated: true });
          }}
            showsVerticalScrollIndicator={false}
          >
            <View style={{ paddingBottom: this.state.keyboardHeight + 40, }}>
              {this.state.get_data.length > 0 ?
                this.state.get_data.map((item, index) => {

                  if (item.to != "admin") {
                    return (
                      <View key={index} style={{ flexDirection: "row", marginTop: 10, paddingHorizontal: 20 }}>


                        <View style={{ width: "auto", backgroundColor: "#fff", borderTopLeftRadius: 10, borderTopRightRadius: 10, borderBottomRightRadius: 10, borderBottomLeftRadius: 3 }}>
                          <View style={{ flexDirection: "row" }}>

                            <View style={{ width: "10%" }} >
                              <View style={{ width: "85%", paddingLeft: 5 }}>

                                <Image style={styles.box2} source={require('../assets/images/14.png')} />
                              </View>
                            </View>
                            <View style={{ paddingTop: 5 }}>
                              <Text style={{ color: "#000" }}>Elif Odamen</Text>
                            </View>



                          </View>
                          <View style={{ padding: 10 }}>
                            <HTML style={{ color: "#000" }} source={{ html: item.message }} contentWidth={"100%"} />
                            {/* <Text style={{ lineHeight: 26, color:"#E7BF63" ,fontSize:10 }}>{item.message}</Text> */}
                          </View>
                        </View>

                      </View>
                    )

                  }
                  else {
                    return (
                      <View key={index} >
                        <View style={{ flexDirection: "row-reverse", marginTop: 10, paddingHorizontal: 20 }}>
                          <View style={{ flexDirection: "row", width: "auto", backgroundColor: "#fff", borderTopLeftRadius: 10, borderTopRightRadius: 10, borderBottomLeftRadius: 10, borderBottomRightRadius: 3 }}>
                            <View style={{ padding: 8 }}>
                              <Text style={{ lineHeight: 26, paddingHorizontal: 10, color: "#000", fontSize: 10 }}>{item.message}</Text>
                            </View>
                          </View>
                          <View style={{ width: "40%" }}>
                          </View>
                        </View>

                      </View>
                    )
                  }




                })
                : null
              }
            </View>

          </ScrollView>

          <View style={{ width: "100%", alignItems: "center", alignContent: "center", bottom: this.state.keyboardHeight + 15, }}>
            <View style={{ width: "88%", backgroundColor: "#FFf", borderRadius: 50, flexDirection: "row", paddingVertical: 8 }}>
              <View style={{ width: "90%" }}>
                <TextInput
                  ref={ref => this.inputText = ref}
                  key={'input-def'}
                  multiline={true}
                  keyboardAppearance={"dark"}
                  style={{ color: 'black', fontSize: 15, paddingHorizontal: 15, paddingBottom: 5 }}
                  placeholder={i18n.t('chat')}
                  placeholderTextColor="#E7BF63"
                  onChangeText={(message_text) => this.setState({ message_text })}
                  value={this.state.message_text}
                />
              </View>


              <TouchableOpacity style={{ width: "10%" }} onPress={() => this.submitmessage()}>
                <View style={{ width: "70%", paddingTop: 4 }}>
                  <Ionicons name="ios-send-sharp" size={20} color="#E7BF63" />
                </View>
              </TouchableOpacity>

            </View>



          </View>


        </View>
      </View>


    );
  }

}
const styles = StyleSheet.create({
  pic: {


    height: 250,
    width: "100%",
    borderRadius: 50



  },
  login_button: {
    width: "100%",
    marginLeft: 10,
    marginTop: 10,
    paddingVertical: 8,
    alignItems: "center",
    alignContent: "center",
    backgroundColor: "#808080",
    borderRadius: 10
  },
  SignUp_button: {

    width: "90%",


    paddingVertical: 10,
    alignItems: "center",
    alignContent: "center",
    backgroundColor: "#E7BF63",
    borderRadius: 10,
    borderWidth: 1,
    borderColor: "#E7BF63"
  },
  box2: {

    width: "100%",
    height: 30,
    resizeMode: "contain",
    borderRadius: 80
    // , borderWidth: 1, borderColor: "#f7bb97"



  },



});