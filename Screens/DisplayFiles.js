import React, { Component } from 'react';
import {
  View, Text, TouchableOpacity, Image, StyleSheet, Picker, Dimensions, ImageBackground, TextInput, Alert, AsyncStorage,ActivityIndicator
} from 'react-native';
import { URL,IMAGE_URL } from '../constants/API'
import { Container, Content, ListItem, CheckBox, Body } from 'native-base';
console.disableYellowBox = true
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { MaterialIcons, MaterialCommunityIcons } from '@expo/vector-icons';
const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
import Header from './Header'
import PDFReader from 'rn-pdf-reader-js-no-loading'
import { WebView } from 'react-native-webview';
import { ScrollView } from 'react-native-gesture-handler'
import Pdf from 'react-native-pdf';
import { Modal } from 'react-native';
import ImageViewer from 'react-native-image-zoom-viewer';
import {ProgressView} from "@react-native-community/progress-view";
import i18n from 'i18n-js'
import translations from './translations'
export default class DisplayFiles extends Component {
  // const [started, setStarted] = useState(false)
  constructor(props) {
    super(props);
    this.data = this.props.route.params;
    console.log(">>>>>>>>>>>>>>this.data>>>>>>>>>>>>>>>>>")
    console.log(this.data)
  
    this.state = {
      started: false,
      loaded: true,
      otp: "",
      get_data: "",
      extension: this.data.other_data.extension,
      data: this.data.other_data,
      // file_id:this.data.other_data.policy_data[0].uuid,
      user_id:"",
      lang_done:false,
      save_loading: false,

    }
  }
  async componentDidMount() {
    AsyncStorage.getItem("userID").then(user_data => {
      const val = JSON.parse(user_data);
      if (val) {
       this.setState({user_id:val.user.uuid,file:val.file})
      
       console.log(val)
      }

      console.log(">>>>>>User_id>>>>>>>>>")
      console.log(this.state.user_id)
      console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
      console.log("Company_id")
      console.log(this.data.other_data.company_id)
      console.log(">>>>>>>>>>>>>>fille_type>>>>>>>>>>>>>>>>>")
      console.log(this.data.other_data.policy_data[0].file)

    });
    AsyncStorage.getItem("lang").then(lang => {
      const language = JSON.parse(lang);
      console.log("--a--")
      console.log(language)
      console.log(language)
      console.log("--a--")
      if (language) {  
        console.log("----")
        console.log(language)
        console.log(language)
        console.log("----")
        i18n.translations = translations;
        i18n.locale = language;
        i18n.fallbacks = true;
      
      }
      this.setState({lang_done : true})
    });
  }
  delete_Policy = () => {
   
    this.setState({ save_loading: true })
    fetch(URL +"delete-policy", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },

      body: JSON.stringify(
        {
          "user_id": this.state.user_id,
          "company_id":this.data.other_data.company_id,
          "file_id":this.data.other_data.policy_data[0].uuid
        }
      )
     
    })
      .then(res => res.json())
      .then(async response => {
        console.log("delete_Policy");
        console.log(response);
        if (response.response == "success") {
     
          Alert.alert(
            "Success",
            response.message,
            [
              {
                text: "Cancel",
                style: "cancel"
              },
              { text: "OK" }
            ],
            { cancelable: false }
            );
          this.props.navigation.replace('dashboard');
        
        }
    
        else {
          Alert.alert(
            "Sorry",
            response.message,
            [
              {
                text: "Cancel",
                style: "cancel"
              },
              { text: "OK" }
            ],
            { cancelable: false }
          );
        }
        this.setState({ save_loading: false })


      })
      .catch(error => {
        alert("Please check internet connection");
      });



  }
  render() {
    console.log("IMAGE_URL +this.data.policy_data")
    // console.log(IMAGE_URL+this.data.policy_data[0].file)
    if(this.state.lang_done){
    return (

      <View style={{ width: screenWidth, height: screenHeight, alignItems: "center" }}>
        <Header title={i18n.t('UploadedPolicy')} back={true} backtext={false} navigation={this.props.navigation} />
          {this.data.other_data.extension == "jpg" || this.data.other_data.extension == "png" ?
           (

            <View style={{ width: "100%", height: "90%" }}>
            <ImageViewer style={{}} imageUrls={[{url:IMAGE_URL +this.data.policy_data}]}/>
              
              <View style={{ width: "100%", position: "absolute", alignItems: "center", bottom:20 }} >
                <View style={{ width: "90%", justifyContent: "center", alignItems: "center", alignContent: "center" }}>
                {this.state.save_loading ? (
                  <View style={styles.SignUp_button}>
                                        <ActivityIndicator color={"#fff"} size={"small"} />
                                    </View>
                                    ) : (
                  <TouchableOpacity style={styles.SignUp_button} onPress={()=>this.delete_Policy()}  >
                    <Text style={{ color: '#fff', alignItems: "center", marginTop: 2, fontSize: 18, fontWeight: "bold" }}>{i18n.t('Delete')}</Text>
                  </TouchableOpacity>
                  )}
                </View>

              </View>
            </View>


          
        ) :
          this.data.other_data.extension == "pdf" ? (
            <View style={{ width: "100%", height: "90%" }}>
            {this.data.policy_data ? (
              <PDFReader noLoader={this.state.loaded} onLoad={()=> this.setState({loaded : false})} style={{ width: "100%", height: "100%" }} source={{uri:IMAGE_URL+this.data.policy_data}} />
            ) : null}
            
              
              <View style={{ width: "100%", position: "absolute", alignItems: "center", bottom: 10 }} >
                <View style={{ width: "90%", justifyContent: "center", alignItems: "center", alignContent: "center" }}>
                {this.state.save_loading ? (   
                  <View style={styles.SignUp_button}>
                                        <ActivityIndicator color={"#fff"} size={"small"} />
                                    </View>
                                    ) : (
                  <TouchableOpacity style={styles.SignUp_button} onPress={()=>this.delete_Policy()}  >
                    <Text style={{ color: '#fff', alignItems: "center", marginTop: 2, fontSize: 18, fontWeight: "bold" }}>{i18n.t('Delete')}</Text>
                  </TouchableOpacity>
                  )}
                </View>

              </View>
            </View>
          ) : 

            this.data.other_data.extension == "docx" ? (
              <View style={{ width: "100%", height: "10%" }}>
                <WebView source={{uri:IMAGE_URL +this.data.policy_data}} />
              <View style={{ width: "100%", alignItems: "center" }} >
                  <View style={{ width: "90%", justifyContent: "center", alignItems: "center", alignContent: "center" }}>
                    <TouchableOpacity   >
                      <Text style={{ color: '#000', alignItems: "center", marginTop: 2, fontSize: 18, fontWeight: "bold" }}>Your file is  downloading</Text>
                    </TouchableOpacity>
                  </View>

                </View>
            
                <View style={{ width: "100%", alignItems: "center",paddingTop:15 }} >
                  <View style={{ width: "90%", justifyContent: "center", alignItems: "center", alignContent: "center" }}>
                  {this.state.save_loading ? (  
                    <View style={styles.word_doc}>
                                        <ActivityIndicator color={"#fff"} size={"small"} />
                                    </View>
                                    ) : (
                    <TouchableOpacity style={styles.word_doc} onPress={()=>this.delete_Policy()} >
                      <Text style={{ color: '#000', alignItems: "center", marginTop: 2, fontSize: 18, fontWeight: "bold" }}>{i18n.t('Delete')}</Text>
                    </TouchableOpacity>
                    )}
                  </View>

                </View>
              </View>

            ) : null
      
       }







      </View>

);
}
else{
  return(
    <View>
      <Text>Wait</Text>
    </View>
  )
}
    
  }

}
const styles = StyleSheet.create({
  pic: {


    height: 250,
    width: "100%",
    borderRadius: 50



  },
  login_button: {
    width: "100%",
    marginLeft: 10,
    marginTop: 10,
    paddingVertical: 8,
    alignItems: "center",
    alignContent: "center",
    backgroundColor: "#808080",
    borderRadius: 10
  },
  SignUp_button: {

    width: "90%",


    paddingVertical: 10,
    alignItems: "center",
    alignContent: "center",
    backgroundColor: "#E7BF63",
    borderRadius: 10,
    borderWidth: 1,
    borderColor: "#E7BF63"
  },
  box1: {

    width: "40%",
    height: 25,
    resizeMode: "cover",
    borderRadius: 80,



  },
  SignUp_button: {

    width: "90%",


    paddingVertical: 10,
    alignItems: "center",
    alignContent: "center",
    backgroundColor: "#E7BF63",
    borderRadius: 10,
    borderWidth: 1,
    borderColor: "#E7BF63"
  }, 
  word_doc: {

    width: "40%",


    paddingVertical: 7,
    alignItems: "center",
    alignContent: "center",
    backgroundColor: "#E7BF63",
    borderRadius: 10,
    borderWidth: 1,
    borderColor: "#E7BF63"
  
   
  },
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginTop: 25,
},

});