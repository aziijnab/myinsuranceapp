import React, { Component } from 'react';
import {
  View, Text, TouchableOpacity, Image, StyleSheet, Picker, Dimensions, ImageBackground, TextInput, Alert, AsyncStorage
} from 'react-native';
import { Ionicons, EvilIcons, MaterialIcons } from '@expo/vector-icons';
import { ScrollView } from 'react-native-gesture-handler';
const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
import { URL } from '../constants/API'
console.disableYellowBox = true
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview'
import Footer from './Footer';
import Header from './Header'
import i18n from 'i18n-js'
import translations from './translations'

export default class ContractIndustries extends Component {
  // const [started, setStarted] = useState(false)
  constructor(props) {
    super(props);
    this.state = {
      started: false,
      lang_done:false
    }

  }
  async componentDidMount() {
   
    AsyncStorage.getItem("lang").then(lang => {
      const language = JSON.parse(lang);
      console.log("--a--")
      console.log(language)
      console.log(language)
      console.log("--a--")
      if (language) {  
        console.log("----")
        console.log(language)
        console.log(language)
        console.log("----")
        i18n.translations = translations;
        i18n.locale = language;
        i18n.fallbacks = true;
      
      }
      this.setState({lang_done : true})
    });
  }


  render()
  {
    if(this.state.lang_done){
      return (

        <View style={{ width: screenWidth, height: screenHeight, alignItems: "center" }}>
          <Header title={i18n.t('Industries')} back={true} backtext={false} navigation={this.props.navigation} />
          <ScrollView showsVerticalScrollIndicator={false} style={{ width: "97%" }}>
                <View style={{ width: "100%", paddingTop: 5, paddingHorizontal: 10 }}>
            <View style={{ flexDirection: 'row', flexWrap: 'wrap', justifyContent: "space-between",marginTop:20 }}>
  
            <View style={styles.popbox}>
                <TouchableOpacity style={{ width: "100%", padding: 5,height:"100%" }}
                onPress={() => this.props.navigation.navigate("Contractors")}
                >
                  <View style={{ width: "100%", alignItems: "center", justifyContent: "center", height:"85%" }}>
  
                    <Image style={{ width: "100%", height: "100%", resizeMode: "contain", borderRadius: 10 }} source={require('../assets/images/1.png')} />
  
  
                  </View>
                  <View style={{ alignItems: "center", justifyContent: "center" }}>
                    <Text style={{ fontWeight: "bold", fontSize: 12, color: "#044184",textAlign:"center" }}>
                      {i18n.t('health')} 
                          </Text>
                  </View>
  
                </TouchableOpacity>
              </View> 
               <View style={styles.popbox}>
                <TouchableOpacity style={{ width: "100%", padding: 5,height:"100%"  }}
                  onPress={() => this.props.navigation.navigate("Contractors")}
                >
                  <View style={{ width: "100%", alignItems: "center", justifyContent: "center", height:"85%" }}>
  
                    <Image style={{ width: "100%", height: "100%", resizeMode: "contain", borderRadius: 10 }} source={require('../assets/images/2.png')} />
  
  
                  </View>
                  <View style={{ alignItems: "center", justifyContent: "center" }}>
                    <Text style={{ fontWeight: "bold", fontSize: 12,  color: "#044184",textAlign:"center" }}>
                       {i18n.t('Motor')} {i18n.t('Vehicle')}
                          </Text>
                  </View>
  
                </TouchableOpacity>
              </View>  
              <View style={styles.popbox}>
                <TouchableOpacity style={{ width: "100%", padding: 5,height:"100%" }}
                  onPress={() => this.props.navigation.navigate("Contractors")}
                >
                  <View style={{ width: "100%", alignItems: "center", justifyContent: "center", height:"85%" }}>
  
                    <Image style={{ width: "100%", height: "100%", resizeMode: "contain", borderRadius: 10 }} source={require('../assets/images/3.png')} />
  
  
                  </View>
                  <View style={{ alignItems: "center", justifyContent: "center" }}>
                    <Text style={{ fontWeight: "bold", fontSize: 12,  color: "#044184",textAlign:"center" }}>
                      {i18n.t('Household')} 
                          </Text>
                  </View>
  
                </TouchableOpacity>
              </View>
              <View style={styles.popbox}>
                <TouchableOpacity style={{ width: "100%", padding: 5,height:"100%" }}
                  onPress={() => this.props.navigation.navigate("Contractors")}
                >
                  <View style={{ width: "100%", alignItems: "center", justifyContent: "center", height:"85%" }}>
  
                    <Image style={{ width: "100%", height: "100%", resizeMode: "contain", borderRadius: 10 }} source={require('../assets/images/4.png')} />
  
  
                  </View>
                  <View style={{ alignItems: "center", justifyContent: "center" }}>
                    <Text style={{ fontWeight: "bold", fontSize: 12,  color: "#044184",textAlign:"center" }}>
                    {i18n.t('Life')}  
                          </Text>
                  </View>
  
                </TouchableOpacity>
              </View> 
               <View style={styles.popbox}>
                <TouchableOpacity style={{ width: "100%", padding: 5,height:"100%" }}
                  onPress={() => this.props.navigation.navigate("Contractors")}
                >
                  <View style={{ width: "100%", alignItems: "center", justifyContent: "center", height:"85%" }}>
  
                    <Image style={{ width: "100%", height: "100%", resizeMode: "contain", borderRadius: 10 }} source={require('../assets/images/5.png')} />
  
  
                  </View>
                  <View style={{ alignItems: "center", justifyContent: "center" }}>
                    <Text style={{ fontWeight: "bold", fontSize: 12,  color: "#044184",textAlign:"center" }}>
                      {i18n.t('Legal')}  
                          </Text>
                  </View>
  
                </TouchableOpacity>
              </View>  
              <View style={styles.popbox}>
                <TouchableOpacity style={{ width: "100%", padding: 5,height:"100%" }}
                  onPress={() => this.props.navigation.navigate("Contractors")}
                >
                  <View style={{ width: "100%", alignItems: "center", justifyContent: "center", height:"85%" }}>
  
                    <Image style={{ width: "100%", height: "100%", resizeMode: "contain", borderRadius: 10 }} source={require('../assets/images/6.png')} />
  
  
                  </View>
                  <View style={{ alignItems: "center", justifyContent: "center" }}>
                    <Text style={{ fontWeight: "bold", fontSize: 12,  color: "#044184",textAlign:"center"}}>
                    {i18n.t('Travel')} 
                          </Text>
                  </View>
  
                </TouchableOpacity>
              </View>
              <View style={styles.popbox}>
                <TouchableOpacity style={{ width: "100%", padding: 5,height:"100%" }}
                  onPress={() => this.props.navigation.navigate("Contractors")}
                >
                  <View style={{ width: "100%", alignItems: "center", justifyContent: "center", height:"85%" }}>
  
                    <Image style={{ width: "100%", height: "100%", resizeMode: "contain", borderRadius: 10 }} source={require('../assets/images/7.png')} />
  
  
                  </View>
                  <View style={{ alignItems: "center", justifyContent: "center" }}>
                    <Text style={{ fontWeight: "bold", fontSize: 12,  color: "#044184",textAlign:"center" }}>
                    {i18n.t('Motorcycle')} 
                    
                          </Text>
                  </View>
  
                </TouchableOpacity>
              </View> 
               <View style={styles.popbox}>
                <TouchableOpacity style={{ width: "100%", padding: 5,height:"100%" }}
                  onPress={() => this.props.navigation.navigate("Contractors")}
                >
                  <View style={{ width: "100%", alignItems: "center", justifyContent: "center", height:"85%" }}>
  
                    <Image style={{ width: "100%", height: "100%", resizeMode: "contain", borderRadius: 10 }} source={require('../assets/images/8.png')} />
  
  
                  </View>
                  <View style={{ alignItems: "center", justifyContent: "center" }}>
                    <Text style={{ fontWeight: "bold", fontSize: 12,  color: "#044184",textAlign:"center" }}>
                      {i18n.t('Building')} 
                          </Text>
                  </View>
  
                </TouchableOpacity>
              </View>  
              <View style={styles.popbox}>
                <TouchableOpacity style={{ width: "100%", padding: 5,height:"100%" }}
                  onPress={() => this.props.navigation.navigate("Contractors")}
                >
                  <View style={{ width: "100%", alignItems: "center", justifyContent: "center", height:"85%" }}>
  
                    <Image style={{ width: "100%", height: "100%", resizeMode: "contain", borderRadius: 10 }} source={require('../assets/images/9.png')} />
  
  
                  </View>
                  <View style={{ alignItems: "center", justifyContent: "center" }}>
                    <Text style={{ fontWeight: "bold", fontSize: 12,  color: "#044184",textAlign:"center" }}>
                    {i18n.t('Personal')} 
                          </Text>
                  </View>
  
                </TouchableOpacity>
              </View>
              
              <View style={styles.popbox}>
                <TouchableOpacity style={{ width: "100%", padding: 5,height:"100%" }}
                  onPress={() => this.props.navigation.navigate("Contractors")}
                >
                  <View style={{ width: "100%", alignItems: "center", justifyContent: "center", height:"85%" }}>
  
                    <Image style={{ width: "100%", height: "100%", resizeMode: "contain", borderRadius: 10 }} source={require('../assets/images/10.png')} />
  
  
                  </View>
                  <View style={{ alignItems: "center", justifyContent: "center" }}>
                    <Text style={{ fontWeight: "bold", fontSize: 12,  color: "#044184",textAlign:"center" }}>
                     {i18n.t('Private')} {i18n.t('Accident')}
                          </Text>
                  </View>
  
                </TouchableOpacity>
              </View>
  
              <View style={styles.popbox}>
                <TouchableOpacity style={{ width: "100%", padding: 5,height:"100%" }}
                  onPress={() => this.props.navigation.navigate("Contractors")}
                >
                  <View style={{ width: "100%", alignItems: "center", justifyContent: "center", height:"85%" }}>
  
                    <Image style={{ width: "100%", height: "100%", resizeMode: "contain", borderRadius: 10 }} source={require('../assets/images/11.png')} />
  
  
                  </View>
                  <View style={{ alignItems: "center", justifyContent: "center" }}>
                    <Text style={{ fontWeight: "bold", fontSize: 12,  color: "#044184",textAlign:"center" }}>
                       {i18n.t('Rental')} {i18n.t('Deposit')}
                          </Text>
                  </View>
  
                </TouchableOpacity>
              </View> 
                <View style={styles.popbox}>
                <TouchableOpacity style={{ width: "100%", padding: 5,height:"100%" }}
                  onPress={() => this.props.navigation.navigate("Contractors")}
                >
                  <View style={{ width: "100%", alignItems: "center", justifyContent: "center", height:"85%" }}>
  
                    <Image style={{ width: "100%", height: "100%", resizeMode: "contain", borderRadius: 10 }} source={require('../assets/images/12.png')} />
  
  
                  </View>
                  <View style={{ alignItems: "center", justifyContent: "center" }}>
                    <Text style={{ fontWeight: "bold", fontSize: 12,  color: "#044184",textAlign:"center" }}>
                   {i18n.t('Motorboat')}
                          </Text>
                  </View>
  
                </TouchableOpacity>
              </View>  
               <View style={styles.popbox}>
                <TouchableOpacity style={{ width: "100%", padding: 5,height:"100%" }}
                  onPress={() => this.props.navigation.navigate("Contractors")}
                >
                  <View style={{ width: "100%", alignItems: "center", justifyContent: "center", height:"85%" }}>
  
                    <Image style={{ width: "100%", height: "100%", resizeMode: "contain", borderRadius: 10 }} source={require('../assets/images/13.png')} />
  
  
                  </View>
                  <View style={{ alignItems: "center", justifyContent: "center" }}>
                    <Text style={{ fontWeight: "bold", fontSize: 12,  color: "#044184",textAlign:"center" }}>
                     {i18n.t('Valuable')} 
                          </Text>
                  </View>
  
                </TouchableOpacity>
              </View>   
               <View style={styles.popbox}>
                <TouchableOpacity style={{ width: "100%", padding: 5,height:"100%" }}
                  onPress={() => this.props.navigation.navigate("Contractors")}
       >
                  <View style={{ width: "100%", alignItems: "center", justifyContent: "center", height:"85%" }}>
  
                    <Image style={{ width: "100%", height: "100%", resizeMode: "contain", borderRadius: 10 }} source={require('../assets/images/9.png')} />
  
  
                  </View>
                  <View style={{ alignItems: "center", justifyContent: "center" }}>
                    <Text style={{ fontWeight: "bold", fontSize: 12,  color: "#044184",textAlign:"center" }}>
                    {i18n.t('Mobile')}
                          </Text>
                  </View>
  
                </TouchableOpacity>
              </View>
              <View style={styles.popbox2}>
               
              </View>
            
            
            
            </View>
            </View>
            
            </ScrollView>
  
  
  
  
  
  
  
  
  
  
        </View>
  
  
      );
    }
    else{
      return(
        <View>
          <Text>Wait</Text>
        </View>
      )
    }
    }

}
const styles = StyleSheet.create({


  popbox: {
    width: '30%',height:screenHeight/4, alignContent: "center", alignItems: "center", backgroundColor: "#fff", borderRadius: 10,margin:5,
    shadowColor: "grey",
    shadowOffset: {
      width: 1,
      height: 1,
    },
    shadowOpacity: 2.60,
    shadowRadius: 2.62,
    elevation: 6,
  }, 
  popbox2: {
    width: '30%', alignContent: "center", alignItems: "center", borderRadius: 10,margin:5,
    
  },
  popbox1: {
    width: '46%', alignContent: "center", alignItems: "center", backgroundColor: "#fff",margin:8, borderRadius: 10, marginRight: 5,
    shadowColor: "grey",
    shadowOffset: {
      width: 1,
      height: 1,
    },
    shadowOpacity: 2.60,
    shadowRadius: 2.62,
    elevation: 6,
  }
});