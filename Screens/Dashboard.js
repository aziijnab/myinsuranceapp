import React, { Component } from 'react';
import {
  View, Text, TouchableOpacity, Image, StyleSheet, Picker, Dimensions, ImageBackground, TextInput, Alert, AsyncStorage
} from 'react-native';
import { Ionicons, EvilIcons, MaterialIcons } from '@expo/vector-icons';
import { ScrollView } from 'react-native-gesture-handler';
const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
import { URL } from '../constants/API'
console.disableYellowBox = true
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview'
import Footer from './Footer';
import Header from './Header'
import i18n from 'i18n-js'
import translations from './translations'
import * as Localization from 'expo-localization';
import Profile from './Profile'


export default class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      started: false,
       full_name:"",
       lang_done:false
  }

}
  async componentDidMount() {
    AsyncStorage.getItem("userID").then(user_data => {
      const val = JSON.parse(user_data);
      if (val) {
        this.setState({full_name:val.user.f_name})
 

       console.log(val)
      }

   

    });
    AsyncStorage.getItem("lang").then(lang => {
      const language = JSON.parse(lang);
      console.log("--a--")
      console.log(language)
      console.log(language)
      console.log("--a--")
      if (language) {  
        console.log("----")
        console.log(language)
        console.log(language)
        console.log("----")
        i18n.translations = translations;
        i18n.locale = language;
        i18n.fallbacks = true;
      
      }
      this.setState({lang_done : true})
    });
  }
  render() {
if(this.state.lang_done){
  return (

    <View style={{ width: screenWidth, height: screenHeight, alignItems: "center" }}>
      <Header title={i18n.t('Dashboard')} back={false} backtext={false} navigation={this.props.navigation} />
      <View style={{ width: "100%" ,marginTop:40}}>
        <Text style={{ fontWeight: "bold", fontSize: 20, color: "#044184",paddingLeft:20 }}>
            {i18n.t('help')} , {this.state.full_name}?
        </Text>
      </View>
      <View style={{width:"100%",alignItems:"center"}}>
      <View style={{ width: "97%" }}>
            {/* <View style={{ width: "100%", paddingTop: 5, paddingHorizontal: 10 }}> */}
        <View style={{ flexDirection: 'row', flexWrap: 'wrap', justifyContent: "space-between",marginTop:40 }}>

          <View style={styles.popbox}>
            <TouchableOpacity style={{ width: "100%",height:"100%", padding: 5 } }
            onPress={() => this.props.navigation.push("contractIndustries")}
            >
              <View style={{ width: "100%",height:"80%", alignItems: "center", justifyContent: "center" }}>

                <Image style={{ width: "100%", height:"100%", resizeMode: "contain", borderRadius: 10 }} source={require('../assets/images/My_Insurance.png')} />


              </View>
              <View style={{ width: "100%",height:"20%",alignItems: "center", justifyContent: "center" }}>
                <Text style={{ fontWeight: "bold", fontSize:15, paddingTop: 6, color: "#044184",textAlign:"center"  }}>
                      {i18n.t('Insurance_check')} 
                      </Text>
              </View>

            </TouchableOpacity>
          </View>


{/* ............................... */}

<View style={styles.popbox}>
            <TouchableOpacity style={{ width: "100%",height:"100%", padding: 5 } }
            onPress={() => this.props.navigation.push("industries")}
            >
              <View style={{ width: "100%",height:"80%", alignItems: "center", justifyContent: "center" }}>

                <Image style={{ width: "100%", height:"100%", resizeMode: "contain", borderRadius: 10 }} source={require('../assets/images/quotation.png')} />


              </View>
              <View style={{ width: "100%",height:"20%",alignItems: "center", justifyContent: "center" }}>
                <Text style={{ fontWeight: "bold", fontSize:15, paddingTop: 6, color: "#044184",textAlign:"center"  }}>
                      {i18n.t('Request_Quote')} 
                      </Text>
              </View>

            </TouchableOpacity>
          </View>
{/* ............................... */}

<View style={styles.popbox}>
            <TouchableOpacity style={{ width: "100%",height:"100%", padding: 5 } }
            onPress={() => this.props.navigation.push("Getfolder")}
            >
              <View style={{ width: "100%",height:"80%", alignItems: "center", justifyContent: "center" }}>

                <Image style={{ width: "100%", height:"100%", resizeMode: "contain", borderRadius: 10 }} source={require('../assets/images/my_safe.png')} />


              </View>
              <View style={{ width: "100%",height:"20%",alignItems: "center", justifyContent: "center" }}>
                <Text style={{ fontWeight: "bold", fontSize:15, paddingTop: 6, color: "#044184",textAlign:"center"  }}>
                      {i18n.t('Add_Contracts')} 
                      </Text>
              </View>

            </TouchableOpacity>
          </View>
{/* ............................... */}

<View style={styles.popbox}>
            <TouchableOpacity style={{ width: "100%",height:"100%", padding: 5 } }
            onPress={() => this.props.navigation.push("meetExpert")}
            >
              <View style={{ width: "100%",height:"80%", alignItems: "center", justifyContent: "center" }}>

                <Image style={{ width: "100%", height:"100%", resizeMode: "contain", borderRadius: 10 }} source={require('../assets/images/meet.png')} />


              </View>
              <View style={{ width: "100%",height:"20%",alignItems: "center", justifyContent: "center" }}>
                <Text style={{ fontWeight: "bold", fontSize:15, paddingTop: 6, color: "#044184",textAlign:"center"  }}>
                      {i18n.t('appointment')} 
                      </Text>
              </View>

            </TouchableOpacity>
          </View>
          {/* <View style={styles.popbox}>
            <TouchableOpacity style={{ width: "100%", padding: 5 }}
            onPress={() => this.props.navigation.push("industries")}
            >
              <View style={{ width: "100%", alignItems: "center", justifyContent: "center" }}>

                <Image style={{ width: "100%", height: 100, resizeMode: "stretch", borderRadius: 10 }} source={require('../assets/images/keyboard_typewriter.png')} />


              </View>
              <View style={{ alignItems: "center", justifyContent: "center" }}>
                <Text style={{ fontWeight: "bold", fontSize:15, paddingTop: 6, color: "#044184" }}>
                    {i18n.t('Request_Quote')} 
                      </Text>
              </View>

            </TouchableOpacity>
          </View>
          <View style={styles.popbox}>
            <TouchableOpacity style={{ width: "100%", padding: 5 }}
            onPress={() => this.props.navigation.push("Getfolder")}
            >
              <View style={{ width: "100%", alignItems: "center", justifyContent: "center" }}>

                <Image style={{ width: "100%", height: 100, resizeMode: "stretch", borderRadius: 10 }} source={require('../assets/images/safe.png')} />


              </View>
              <View style={{ alignItems: "center", justifyContent: "center" }}>
                <Text style={{ fontWeight: "bold", fontSize:15, paddingTop: 6, color: "#044184" }}>
                 {i18n.t('Add_Contracts')} 
                      </Text>
              </View>

            </TouchableOpacity>
          </View>

          <View style={styles.popbox}>
            <TouchableOpacity style={{ width: "100%", padding: 5 }}
            onPress={() => this.props.navigation.push("meetExpert")}
            >
              <View style={{ width: "100%", alignItems: "center", justifyContent: "center" }}>

                <Image style={{ width: "100%", height: 100, resizeMode: "stretch", borderRadius: 10 }} source={require('../assets/images/handshake.png')} />


              </View>
              <View style={{ alignItems: "center", justifyContent: "center" }}>
                <Text style={{ fontWeight: "bold", fontSize:15, paddingTop: 6, color: "#044184",textAlign:"center" }}>
                 {i18n.t('appointment')}
                      </Text>
              </View>

            </TouchableOpacity>
          </View>
       
        */}
        </View>

        {/* </View> */}
        </View>


      </View>
      







      <Footer title={"Home"} navigation={this.props.navigation} />
    </View>


  );
}
else{
  return(
    <View>
      <Text>Wait</Text>
    </View>
  )
}
    
  }

}
const styles = StyleSheet.create({

  popbox: {
    width: '48%', 
    height: screenHeight/6,
    alignContent: "center", alignItems: "center", backgroundColor: "#fff",
    marginTop:8,
     borderRadius: 10,
    shadowColor: "grey",
    shadowOffset: {
      width: 1,
      height: 1,
    },
    shadowOpacity: 2.60,
    shadowRadius: 2.62,
    elevation: 6,
  },
  // popbox1: {
  //   width: '46%', alignContent: "center", alignItems: "center", backgroundColor: "#fff",margin:8, borderRadius: 10, marginRight: 5,
  //   shadowColor: "grey",
  //   shadowOffset: {
  //     width: 1,
  //     height: 1,
  //   },
  //   shadowOpacity: 2.60,
  //   shadowRadius: 2.62,
  //   elevation: 6,
  // }
});