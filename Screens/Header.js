import React, { Component, useState } from 'react';
import { View, Text, TouchableOpacity, Image, StyleSheet, Picker, Dimensions, TextInput } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { MaterialIcons,Ionicons,AntDesign,Entypo  } from '@expo/vector-icons';

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
console.disableYellowBox=true


export default class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      page: "",
    }

  }
  render() {
    return (
      <View style={styles.popbox}>

<View style={{width: screenWidth, alignItems: 'center', backgroundColor: 'white',paddingVertical:5 }}>
      
        <View style={{ marginTop: 45, width: "100%" }}>
        <View style={{ width: "90%", flexDirection: "row"}}>
        <View style={{ width: "15%",paddingLeft:12 ,paddingTop:5}}>
            {this.props.back ? (
              <TouchableOpacity style={{ flexDirection: "row" }} onPress={() => this.props.navigation.goBack()}>
                <Ionicons name="ios-arrow-back" size={25} color="#E7BF63" />
                {this.props.backtext ? (

                  <Text style={{ textAlign: "left", color: "black", marginHorizontal: 5, fontSize:20,  }}>Back</Text>
                ) : null}
                
              </TouchableOpacity>
            ) : null}
            {this.props.backScreen ? (
              <TouchableOpacity style={{ flexDirection: "row" }} onPress={() => this.props.navigation.push('Contractors')}>
                <Ionicons name="ios-arrow-back" size={25} color="#E7BF63" />
                {this.props.backtext ? (

                  <Text style={{ textAlign: "left", color: "black", marginHorizontal: 5, fontSize:20,  }}>Back</Text>
                ) : null}
                
              </TouchableOpacity>
            ) : null}
          {this.props.cross ? (
              <TouchableOpacity style={{ flexDirection: "row" }} onPress={() =>this.props.navigation.goBack()}>
              <Entypo name="cross" size={24} color="black" />
              </TouchableOpacity>
            ) : null} 
             {this.props.cross ? (
              <TouchableOpacity style={{ flexDirection: "row" }} onPress={() =>this.props.navigation.goBack()}>
              <Entypo name="cross" size={24} color="black" />
              </TouchableOpacity>
            ) : null}  
            {this.props.animal ? (
              <TouchableOpacity style={{ flexDirection: "row" }} onPress={() => this.props.navigation.goBack()}>
              <Ionicons name="ios-arrow-back" size={30} color="#007AFF" />
              <Text style={{ textAlign: "left", color: "#007AFF", textDecorationLine: 'underline', marginHorizontal: 5, fontSize: 18, marginTop: 2 }}>My Animal</Text>
              </TouchableOpacity>
            ) : null}
          </View>

          <View style={{ width: "85%",alignItems:"center" }}>
            <Text style={{ fontWeight: 'bold', fontSize: 23,color:"#044184" }}>{this.props.title}</Text>
          </View>
          {/* <View style={{ width: "20%"}}>
            {this.props.location ? (
              <TouchableOpacity style={{ flexDirection: "row-reverse" }} onPress={() =>this.props.navigation.navigate('GetStarted')}>
                <MaterialIcons name="location-on" size={17} color="#757575" />
                <Text style={{ textAlign: "right", color: "#1294D0", textDecorationLine: 'underline' }}>{this.props.location_name}
           </Text>
              </TouchableOpacity>
            ) : null}
            <View style={{paddingLeft:35}}>

            {this.props.filter ? (
              <TouchableOpacity  >
               <AntDesign name="filter" size={17} color="black" />
              </TouchableOpacity>
            ) : null}

            </View>
            
            {this.props.done ? (
              <TouchableOpacity style={{  }} onPress={() => this.props.navigation.navigate('mail')}>
               <Text style={{ textAlign: "left", color: "#E7BF63", marginHorizontal: 5, fontSize: 22, marginTop: 2 }}>Done</Text>
              </TouchableOpacity>
            ) : null}
          </View> */}
          
        </View>
       
     
          <View>
            {this.props.more ? (
              <TouchableOpacity style={{ flexDirection: "row-reverse" }} onPress={() => this.props.navigation.navigate('SignUp')}>
                <Ionicons style={{ marginLeft: 3 }} name="ios-arrow-forward" size={21} color="#7A7A7A" />
                <Text style={{ textAlign: "right", color: "#888888", textDecorationLine: 'underline' }}>Not a member
            </Text>
              </TouchableOpacity>
            ) : null}
          </View>


        </View>

      </View>
</View>
   





     
  
  );
}
}const styles = StyleSheet.create({
  popbox: {
 
  shadowColor: "grey",
  shadowOffset: {
    width: 1,
    height: 1,
  },
  shadowOpacity:0.4,
  shadowRadius: 3.62,
  elevation: 2,
}
});
