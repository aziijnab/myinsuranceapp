import React, { Component } from 'react';
import {
  View, Text, TouchableOpacity, Image, StyleSheet, Picker, Dimensions, ImageBackground, TextInput, Alert, AsyncStorage, ScrollView
} from 'react-native';
import { MaterialIcons, Ionicons, EvilIcons, MaterialCommunityIcons, Octicons, Feather, Entypo, AntDesign, FontAwesome5, Zocial } from '@expo/vector-icons';
const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
import Header from './Header'
import { URL } from '../constants/API'
import RNPickerSelect from 'react-native-picker-select';
console.disableYellowBox = true
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview'
import i18n from 'i18n-js'
import translations from './translations'
export default class Registration extends Component {
  constructor(props) {
    super(props);
    this.state = {
      started: false,
      f_name: "",
      s_name: "",
      email: "",
      password: "",
      phone: "",
      address: "",
      zip_code: "",
      city: "",
      dob: "",
      lang_done:false,
      favColor: "",
    }

  }
  async componentDidMount() {
    AsyncStorage.getItem("userID").then(user_data => {
      const val = JSON.parse(user_data);
      if (val) {
        this.setState({full_name:val.user.f_name})
       console.log(">>>>>>>>>>>>>>>>>>full name>>>>>>>>>>")
       console.log(full_name)

       console.log(val)
      }

   

    });
    AsyncStorage.getItem("lang").then(lang => {
      const language = JSON.parse(lang);
      console.log("--a--")
      console.log(language)
      console.log(language)
      console.log("--a--")
      if (language) {  
        console.log("----")
        console.log(language)
        console.log(language)
        console.log("----")
        i18n.translations = translations;
        i18n.locale = language;
        i18n.fallbacks = true;
      
      }
      this.setState({lang_done : true})
    });
  }
  // sendRequest = () => {
  //   let f_name = this.state.f_name;
  //   let s_name = this.state.s_name;
  //   let email = this.state.email;
  //   let password = this.state.password;
  //   let phone = this.state.phone;
  //   let address = this.state.address;
  //   let zip_code = this.state.zip_code;
  //   let city = this.state.city;
  //   let dob = this.state.dob;

  //   fetch(URL + "register", {
  //     method: "POST",
  //     headers: {
  //       "Content-Type": "application/json",
  //     },
  //     body: JSON.stringify({
  //       "f_name": f_name,
  //       "sur_name": s_name,
  //       "email": email,
  //       "password": password,
  //       "phone_no": phone,
  //       "address": address,
  //       "zip_code": zip_code,
  //       "city": city,
  //       "access_code": dob,
  //     })
  //   })
  //     .then(res => res.json())
  //     .then(async response => {
  //       console.log(response);
  //       if (response.response == "success") {

  //         Alert.alert(
  //           "Success",
  //           response.message,
  //           [
  //             {
  //               text: "Cancel",
  //               style: "cancel"
  //             },
  //             { text: "OK" }
  //           ],
  //           { cancelable: false }
  //         );
  //         this.props.navigation.push("RegisteredInsurance", { register_data: this.state });

  //       }
  //       else {
  //         Alert.alert(
  //           "Error",
  //           response.message,
  //           [
  //             {
  //               text: "Cancel",
  //               style: "cancel"
  //             },
  //             { text: "OK" }
  //           ],
  //           { cancelable: false }
  //         );
  //       }

  //     }
  //     )
  //     .catch(error => {
  //       alert("Please check internet connection");

  //     });

  // }

  submit = () => {
    let f_name = this.state.f_name;
    let s_name = this.state.s_name;
    let email = this.state.email;
    let password = this.state.password;
    let phone = this.state.phone;
    let address = this.state.address;
    let zip_code = this.state.zip_code;
    let city = this.state.city;
    let dob = this.state.dob;
    let favColor = this.state.favColor;

    //  let email_regex = "^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$";
    var email_regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var phone_regex= /^([0-9\s\-\+\(\)]*)$/
    // var phone_regex= /^(1\s|1|)?((\(\d{3}\))|\d{3})(\-|\s)?(\d{3})(\-|\s)?(\d{4})$/;

    if (f_name == "" || s_name == "" || address == "" || email == "" || city == "" || zip_code == "" || phone == "" || dob == "" || password == ""|| favColor == "" )
     {
      Alert.alert(
        "My_Insurance ",
        "Please enter all fields ",
        [
          {
            text: "Cancel",
            style: "cancel"
          },
          { text: "OK" }
        ],
        { cancelable: false }
      );
    }
    else {
      if (email != "") {
        if (!email_regex.test(email)) {
          Alert.alert(
            "Sorry",
            "Email is not correct",
            [
              {
                text: "Cancel",
                style: "cancel"
              },
              { text: "OK" }
            ],
            { cancelable: false }
          );
        }
      }
     if(phone !="" ){
        if(!phone_regex.test(phone)){
          Alert.alert(
            "Sorry",
            "Phone Number is not correct",
            [
              {
                text: "Cancel",
                style: "cancel"
              },
              { text: "OK" }
            ],
            { cancelable: false }
          );

        }
      
 

      }
      
      this.props.navigation.push("RegisteredInsurance", { register_data: this.state })
    }
  }


  render() {
    if (this.state.lang_done) {
      const placeholder_country = {
        label: i18n.t('reg_gender'),
        value: null,
        color: "#044184",
        key: 0
    };
      return (

        <View style={{ width: screenWidth, height: screenHeight, alignItems: "center", backgroundColor: "" }}>

          <Header title={i18n.t('Registration')} back={true} backtext={false} navigation={this.props.navigation}/>
          <KeyboardAwareScrollView
            contentContainerStyle={{ width: screenWidth, height: screenHeight }}>
            <View style={{ width: "100%", paddingBottom: 300 }}>
              <View style={{ width: "100%", marginTop: 40 }}>
                <Text style={{ fontWeight: "bold", fontSize: 20, color: "#044184", paddingLeft: 20 }}>
                  {i18n.t('create')}
          </Text>
              </View>
              <View style={{ width: "100%", marginTop: 5, paddingVertical: 2, alignContent: "center", alignItems: "center" }}>
                <View style={{ width: "90%", borderBottomWidth: 1, borderBottomColor: "#044184",paddingLeft:5 }}>
              <RNPickerSelect
                  placeholder={placeholder_country}
                items={[
            {
                label: i18n.t('mr'),
                value: i18n.t('mr'),
            },
            {
                label: i18n.t('mrs'),
                value: i18n.t('mrs'),
                
            },
         
        ]}
                onValueChange={(value) => {
                    this.setState({
                        favColor: value,
                    });
                }}
            
                style={{ ...pickerSelectStyles }}
                value={this.state.favColor}
                
                // ref={(el) => {
                //     this.inputRefs.picker = el;
                // }}
            />
</View>
</View>
              <View style={{ width: "100%", marginTop: 15, paddingVertical: 5, alignContent: "center", alignItems: "center" }}>
                <View style={{ width: "90%", borderBottomWidth: 1, borderBottomColor: "#044184", paddingBottom: 5 }}>
                  <TextInput
                    style={{ color: '#044184', fontSize: 16, paddingHorizontal: 15 }}
                    secureTextEntry={false}
                    placeholder={i18n.t('FirstName')}
                    placeholderTextColor="#044184"

                    onChangeText={(f_name) => this.setState({ f_name })}
                    value={this.state.f_name}
                  />
                </View>
              </View>
              <View style={{ width: "100%", marginTop: 15, paddingVertical: 5, alignContent: "center", alignItems: "center" }}>
                <View style={{ width: "90%", borderBottomWidth: 1, borderBottomColor: "#044184", paddingBottom: 5 }}>
                  <TextInput
                    style={{ color: '#044184', fontSize: 16, paddingHorizontal: 15 }}
                    secureTextEntry={false}
                    placeholder={i18n.t('Surname')}
                    placeholderTextColor="#044184"
                    onChangeText={(s_name) => this.setState({ s_name })}
                    value={this.state.s_name}
                  />
                </View>
              </View>
              <View style={{ width: "100%", marginTop: 15, paddingVertical: 5, alignContent: "center", alignItems: "center" }}>
                <View style={{ width: "90%", borderBottomWidth: 1, borderBottomColor: "#044184", paddingBottom: 5 }}>
                  <TextInput
                    style={{ color: '#044184', fontSize: 16, paddingHorizontal: 15 }}
                    secureTextEntry={false}
                    keyboardType="email-address"
                    placeholder={i18n.t('email')}
                    placeholderTextColor="#044184"
                    onChangeText={(email) => this.setState({ email })}
                    value={this.state.email}
                  />
                </View>
              </View>
              <View style={{ width: "100%", marginTop: 15, paddingVertical: 5, alignContent: "center", alignItems: "center" }}>
                <View style={{ width: "90%", borderBottomWidth: 1, borderBottomColor: "#044184", paddingBottom: 5 }}>
                  <TextInput
                    style={{ color: '#044184', fontSize: 16, paddingHorizontal: 15 }}
                    secureTextEntry={true}
                    placeholder={i18n.t('password')}
                    placeholderTextColor="#044184"
                    onChangeText={(password) => this.setState({ password })}
                    value={this.state.password}
                  />
                </View>
              </View>
              <View style={{ width: "100%", marginTop: 15, paddingVertical: 5, alignContent: "center", alignItems: "center" }}>
                <View style={{ width: "90%", borderBottomWidth: 1, borderBottomColor: "#044184", paddingBottom: 5 }}>
                  <TextInput
                    style={{ color: '#044184', fontSize: 16, paddingHorizontal: 15 }}
                    keyboardType={'phone-pad'}
                    placeholder={i18n.t('phone')}
                    placeholderTextColor="#044184"
                    onChangeText={(phone) => this.setState({ phone })}
                    value={this.state.phone}
                  />
                </View>
              </View>
              <View style={{ width: "100%", marginTop: 15, paddingVertical: 5, alignContent: "center", alignItems: "center" }}>
                <View style={{ width: "90%", borderBottomWidth: 1, borderBottomColor: "#044184", paddingBottom: 5 }}>
                  <TextInput
                    style={{ color: '#044184', fontSize: 16, paddingHorizontal: 15 }}
                    multiline={true}
                    secureTextEntry={false}
                    placeholder={i18n.t('address')}
                    placeholderTextColor="#044184"
                    onChangeText={(address) => this.setState({ address })}
                    value={this.state.address}
                  />
                </View>
              </View>
              <View style={{ width: "100%", marginTop: 15, paddingVertical: 5, alignContent: "center", alignItems: "center" }}>
                <View style={{ width: "90%", borderBottomWidth: 1, borderBottomColor: "#044184", paddingBottom: 5 }}>
                  <TextInput
                    style={{ color: '#044184', fontSize: 16, paddingHorizontal: 15 }}
                    secureTextEntry={false}
                    placeholder={i18n.t('accessCode')}
                    placeholderTextColor="#044184"
                    onChangeText={(dob) => this.setState({ dob })}
                    value={this.state.dob}
                  />
                </View>
              </View>
              <View style={{ width: "100%", marginTop: 15, paddingVertical: 5, alignContent: "center", alignItems: "center" }}>
                <View style={{ width: "90%", borderBottomWidth: 1, borderBottomColor: "#044184", paddingBottom: 5 }}>
                  <TextInput
                    style={{ color: '#044184', fontSize: 16, paddingHorizontal: 15 }}
                    secureTextEntry={false}
                    placeholder={i18n.t('ZipCode')}
                    placeholderTextColor="#044184"
                    onChangeText={(zip_code) => this.setState({ zip_code })}
                    value={this.state.zip_code}
                  />
                </View>
              </View>
              <View style={{ width: "100%", marginTop: 15, paddingVertical: 5, alignContent: "center", alignItems: "center" }}>
                <View style={{ width: "90%", borderBottomWidth: 1, borderBottomColor: "#044184", paddingBottom: 5 }}>
                  <TextInput
                    style={{ color: '#044184', fontSize: 16, paddingHorizontal: 15 }}
                    secureTextEntry={false}
                    placeholder={i18n.t('City')}
                    placeholderTextColor="#044184"
                    onChangeText={(city) => this.setState({ city })}
                    value={this.state.city}
                  />
                </View>
              </View>


            </View>

          </KeyboardAwareScrollView>







          <View style={{ width: "100%", position: "absolute", alignItems: "center", bottom: 10 }} >


            <View style={{ width: "90%", justifyContent: "center", alignItems: "center", alignContent: "center" }}>
              <TouchableOpacity style={styles.SignUp_button} onPress={() => this.submit()} >
                <Text style={{ color: '#fff', alignItems: "center", marginTop: 2, fontSize: 18, fontWeight: "bold" }}>{i18n.t('continue')}</Text>
              </TouchableOpacity>
            </View>

          </View>



        </View>


      );
    }
    else {
      return (
        <View>
          <Text>Wait</Text>
        </View>
      )
    }
  }

}
const styles = StyleSheet.create({
  pic: {


    height: 250,
    width: "100%",


  },
  login_button: {
    width: "80%",
    marginLeft: 40,
    marginTop: 10,
    paddingVertical: 8,
    alignItems: "center",
    alignContent: "center",
    backgroundColor: "#808080",
    borderRadius: 10
  },
  getstarted_button: {
    width: "90%",
    // top: 80,
    marginHorizontal: 20,
    paddingVertical: 10,
    alignItems: "center",
    backgroundColor: "#fff",
    borderRadius: 10,

    bottom: 0


  },
  SignUp_button: {

    width: "90%",


    paddingVertical: 10,
    alignItems: "center",
    alignContent: "center",
    backgroundColor: "#E7BF63",
    borderRadius: 10,
    borderWidth: 1,
    borderColor: "#E7BF63"
  },
});
const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
      fontSize: 16,
      paddingTop: 13,
      paddingHorizontal: 10,
      paddingBottom: 12,
      borderWidth: 1,
      borderColor: '#044184',
      borderRadius: 4,
      // backgroundColor: '#044184',
      color: 'black',
  },
  inputAndroid: {
    fontSize: 16,
      paddingTop: 13,
     paddingLeft:20,
      paddingBottom: 10,
      borderWidth: 1,
      borderColor: '#044184',
      borderRadius: 4,
      // backgroundColor: '#044184',
      color: '#044184',
}
});