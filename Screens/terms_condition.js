//import React from 'react';
import React, { Component } from "react";
import {
  StyleSheet,
  StatusBar,
  Text,
  View,
  Dimensions,
  AsyncStorage,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ActivityIndicator,
  ScrollView,
  image
} from "react-native";
console.disableYellowBox = true;
import Footer from './Footer';
import { LinearGradient } from 'expo-linear-gradient';
import Icon from 'react-native-vector-icons/Ionicons';
import Constants from "expo-constants";
import { Container, Picker, Content, Button } from "native-base";

import * as ImagePicker from 'expo-image-picker';
import { MaterialIcons, Ionicons, EvilIcons, MaterialCommunityIcons, Octicons, Feather, Entypo, AntDesign } from '@expo/vector-icons';

import { Notifications } from "expo";
import * as Permissions from "expo-permissions";
import { TextInput } from "react-native-gesture-handler";
const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;
import i18n from 'i18n-js'
import translations from './translations'
import Header from './Header'
export default class terms_condition extends Component {
  constructor(props) {
    super(props);
    this.state = {
      lang_done:false
    };
  }
  componentDidMount() {
  
    AsyncStorage.getItem("lang").then(lang => {
      const language = JSON.parse(lang);
      console.log("--a--")
      console.log(language)
      console.log(language)
      console.log("--a--")
      if (language) {  
        console.log("----")
        console.log(language)
        console.log(language)
        console.log("----")
        i18n.translations = translations;
        i18n.locale = language;
        i18n.fallbacks = true;
      
      }
      this.setState({lang_done : true})
    });
  }
 

 
 
 
  render() {

    if(this.state.lang_done){
    return (


      <View style={{ width: screenWidth, height: screenHeight }} >
       <Header title={""} back={true} backtext={false} navigation={this.props.navigation} />
<ScrollView style={{width:"100%",height:"90%"}}
          showsVerticalScrollIndicator={false}

>
          <View style={{ width: "100%", alignItems: "center",marginTop:5 }}>
          <Image style={styles.pic} source={require('../assets/images/logo.png')} />
        </View>
        <View style={{ width: "100%", marginTop: 40,alignItems:"center",paddingBottom:50 }}>
        <View style={{width:"92%",alignItems:"center",backgroundColor:"#fff",borderRadius:15, padding:15}}>
                <View style={{width:"100%",paddingVertical:5}}>
                <Text style={{  fontSize: 15, color: "#044184",fontWeight:"bold",fontSize:18}}> 
                  {i18n.t('terms_condition_heading')}
          </Text>
                </View>
                <View style={{ width: "100%", marginTop: 10}}>
                <Text style={{  fontSize: 15, color: "#000",textAlign:"justify" }}>
                {i18n.t('terms_condition')}
          </Text>
                </View>   
                 <View style={{ width: "100%", marginTop: 10}}>
                <Text style={{  fontSize: 15, color: "#000"}}>
                {i18n.t('Broker_mandate')}
          </Text>
                </View> 
                 <View style={{ width: "100%", marginTop: 10}}>
                <Text style={{  fontSize: 15, color: "#000"}}>
                {i18n.t('In_between')}
          </Text>
                </View> 
                 <View style={{ width: "100%", marginTop: 10}}>
                <Text style={{  fontSize: 15, color: "#000"}}>
                {i18n.t('customer')}
          </Text>
                </View>
                <View style={{ width: "100%", marginTop: 10}}>
                <Text style={{  fontSize: 15, color: "#000"}}>
                {i18n.t('and_the')}
          </Text>
                </View>
                <View style={{ width: "100%", marginTop: 10}}>
                <Text style={{  fontSize: 15, color: "#000"}}>
                Kabu Consultation GmbH
          </Text>
                </View> 
                <View style={{ width: "100%", marginTop: 2}}>
                <Text style={{  fontSize: 15, color: "#000"}}>
                {i18n.t('Badenerstrasse')}
          </Text>
                </View> 
                <View style={{ width: "100%", marginTop: 2}}>
                <Text style={{  fontSize: 15, color: "#000"}}>
                {i18n.t('consultant')}
          </Text>
                </View> 
                <View style={{ width: "100%", marginTop: 10}}>
                <Text style={{  fontSize: 15, color: "#000"}}>
                {i18n.t('Beginning')}
          </Text>
                </View> 
                 <View style={{ width: "100%", marginTop: 10}}>
                <Text style={{  fontSize: 15, color: "#000"}}>
                {i18n.t('parties')}
          </Text>
                </View>
                <View style={{ width: "100%", marginTop: 2}}>
                <Text style={{  fontSize: 15, color: "#000"}}>
                {i18n.t('client_consultant')}
          </Text>
                </View> 
                 <View style={{ width: "100%", marginTop: 2}}>
                <Text style={{  fontSize: 15, color: "#000"}}>
                {i18n.t('policyh')}
          </Text>
                </View> 
                <View style={{ width: "100%", marginTop: 2}}>
                <Text style={{  fontSize: 15, color: "#000"}}>
                {i18n.t('grants')}
          </Text>
                </View>
                <View style={{ width: "100%", marginTop: 2}}>
                <Text style={{  fontSize: 15, color: "#000"}}>
                {i18n.t('particular_points')}
          </Text>
                </View>
                <View style={{ width: "100%", marginTop: 2}}>
                <Text style={{  fontSize: 15, color: "#000"}}>
                {i18n.t('review_insurance')}
          </Text>
                </View>
                <View style={{ width: "100%", marginTop: 2}}>
                <Text style={{  fontSize: 15, color: "#000"}}>
                {i18n.t('Obtaining_offers')}
          </Text>
                </View> 
                <View style={{ width: "100%", marginTop: 2}}>
                <Text style={{  fontSize: 15, color: "#000"}}>
                {i18n.t('renovations')}
          </Text>
                </View>
                 <View style={{ width: "100%", marginTop: 2}}>
                <Text style={{  fontSize: 15, color: "#000"}}>
                {i18n.t('bonus')}
          </Text>
                </View> 
                 <View style={{ width: "100%", marginTop: 2}}>
                <Text style={{  fontSize: 15, color: "#000"}}>
                {i18n.t('entire_insurance')}
          </Text>
                </View>   
                <View style={{ width: "100%", marginTop: 2}}>
                <Text style={{  fontSize: 15, color: "#000"}}>
                {i18n.t('expressly_requests')}
          </Text>
                </View>
               














              
              </View>
</View>
            
             




          {/* </View> */}
        
         

</ScrollView>
        




      </View>
  );
}
else{
  return(
    <View>
      <Text>Wait</Text>
    </View>
  )
}
    
  }

}
const styles = StyleSheet.create({
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: 'white',
  },
  search: {

    width: "100%",
    marginTop: 10,
    paddingVertical: 15,

    backgroundColor: "#F5F5F5",
    borderRadius: 50,
  },
  box: {

    width: "100%",
    height: 60,
    resizeMode: "cover",
    borderRadius: 80



  },
  login_button: {
    width: "50%",
    paddingVertical: 10,
    alignItems: "center",
    alignContent: "center",

    backgroundColor: "#fff",
    borderRadius: 10,
  },
  getstarted_button: {
    width: "90%",
    // top: 80,
    marginHorizontal: 20,
    paddingVertical: 10,
    alignItems: "center",
    backgroundColor: "#fff",
    borderRadius: 10,

    bottom: 10


  },
  pic: {


    height: 250,
    width: "100%",
    resizeMode:"contain",
    paddingTop:50


  },
})





















































{/* {this.state.isLoading ? (
                 <Button
                   bordered
                   style={{
                     width: "100%",
                     alignItems: "center",
                     justifyContent: "center",
                     borderColor: fontColor,
                     borderRadius: 5,
                     alignItems:"center"
                   }}
                 >
                   <ActivityIndicator color={"#fd7e14"} size={"small"} />
                 </Button>
               ) : (
               
               <TouchableOpacity onPress={()=>this.props.navigation.navigate('ChatDashboard')}>
                   <image source={require('../assets/images/enter.png')}/>
                   </TouchableOpacity>
               )} */}
