import React, { Component } from "react";
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, TouchableHighlight,AsyncStorage } from 'react-native';
import AppNavigator from './Navigation/AppNavigator';
import LoginNavigator from './Navigation/LoginNavigator';
import * as Font from 'expo-font';
import { Ionicons } from '@expo/vector-icons';
import AppLoading from 'expo-app-loading';
import i18n from 'i18n-js'
import translations from './Screens/translations'

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading:true,
      loggedIn:false,
    };
    
  }
  async componentDidMount() {
    
    // i18n.translations = translations;
    // i18n.locale = "en";
    // i18n.fallbacks = true;
    AsyncStorage.getItem("userID").then(user_data => {
      const val = JSON.parse(user_data);
      console.log(val);
      if(val){
        this.setState({loggedIn:true})
      }
     
    });

   

    await Promise.all([
      await Font.loadAsync({
        Roboto: require('native-base/Fonts/Roboto.ttf'),
        Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'),
        ...Ionicons.font,
       })
     ])
     this.setState({loading:false});
  }
  render(){
    if (this.state.loading) {
      return <AppLoading />;
    }
    if(this.state.loggedIn){
      return(
        <LoginNavigator/>
      )
    }else{
      return(
        <AppNavigator/>
      )
    }


  }

}
