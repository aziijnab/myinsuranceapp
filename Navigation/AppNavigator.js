import React, { Component } from "react";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Login from '../Screens/Login';
import Profile from '../Screens/Profile';
import ForgotPassword from '../Screens/ForgotPassword';
import Otp from '../Screens/Otp';
import Home from '../Screens/Home';
import Registration from '../Screens/Registration';
import Mandate from '../Screens/Mandate';
import Dashboard from '../Screens/Dashboard';
import InsuranceCompanies from '../Screens/InsuranceCompanies';
import Policy from '../Screens/Policy';
import MeetExpert from '../Screens/MeetExpert';
import Approval from '../Screens/Approval';
import Offer from '../Screens/Offer';
import UploadPolicy from '../Screens/UploadPolicy';
import Mail from '../Screens/Mail';
import checkInsurance from '../Screens/checkInsurance';
import Contractors from '../Screens/Contractors';
import Switchlanguage from '../Screens/Switchlanguage';
import Industries from '../Screens/Industries';
import ContractIndustries from '../Screens/ContractIndustries';
import ReviewMandate from '../Screens/ReviewMandate';
import Chat from '../Screens/Chat';
import DisplayFiles from '../Screens/DisplayFiles';
import Appointment_Mail from '../Screens/Appointment_Mail';
import Companies from '../Screens/Companies';
import RegisteredInsurance from '../Screens/RegisteredInsurance';
import Getfolder from '../Screens/Getfolder';
import uploadedSnap from '../Screens/uploadedSnap';
import ModeSelection from '../Screens/ModeSelection';
import terms_condition from '../Screens/terms_condition';




const Stack = createStackNavigator();

export default function AppNavigator() {
  return (
    <NavigationContainer>
    <Stack.Navigator initialRouteName="ModeSelection" headerMode={"none"}>
      <Stack.Screen name="login" component={Login} />
      <Stack.Screen name="profile" component={Profile} />
      <Stack.Screen name="forgetpassword" component={ForgotPassword} />
      <Stack.Screen name="otp" component={Otp} />
      <Stack.Screen name="home" component={Home} />
      <Stack.Screen name="registration" component={Registration} />
      <Stack.Screen name="mandate" component={Mandate} />
      <Stack.Screen name="dashboard" component={Dashboard} />
      <Stack.Screen name="insuranceCompanies" component={InsuranceCompanies} />
      <Stack.Screen name="policy" component={Policy} />
      <Stack.Screen name="meetExpert" component={MeetExpert} />
      <Stack.Screen name="approval" component={Approval} />
      <Stack.Screen name="offer" component={Offer} />
      <Stack.Screen name="uploadPolicy" component={UploadPolicy} />
      <Stack.Screen name="mail" component={Mail} />
      <Stack.Screen name="CheckInsurance" component={checkInsurance} />
      <Stack.Screen name="Contractors" component={Contractors} />
      <Stack.Screen name="switchlanguage" component={Switchlanguage} />
      <Stack.Screen name="industries" component={Industries} />
      <Stack.Screen name="contractIndustries" component={ContractIndustries} />
      <Stack.Screen name="reviewMandate" component={ReviewMandate} />
      <Stack.Screen name="chat" component={Chat} />
      <Stack.Screen name="DisplayFiles" component={DisplayFiles} />
      <Stack.Screen name="Appointment_Mail" component={Appointment_Mail} />
      <Stack.Screen name="Companies" component={Companies} />
      <Stack.Screen name="RegisteredInsurance" component={RegisteredInsurance} />
      <Stack.Screen name="Getfolder" component={Getfolder} />
      <Stack.Screen name="uploadedSnap" component={uploadedSnap} />
      <Stack.Screen name="ModeSelection" component={ModeSelection} />
      <Stack.Screen name="terms_condition" component={terms_condition} />


      </Stack.Navigator>
  </NavigationContainer> 
  );
}
